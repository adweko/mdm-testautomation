from selenium import webdriver

chromedriver_loc = r'D:\dev\workspace\mdm-testautomation\lib\chromedriver.exe'
driver = webdriver.Chrome(executable_path=chromedriver_loc)

driver.get("https://ebusiness-devl.helvetia.com/ch/zepasweb/?testlogin")
user_name = driver.find_element_by_name("isiwebuserid")
user_name.send_keys("chths03")
password = driver.find_element_by_name("isiwebpasswd")
password.send_keys("asdfg123")
field_continue = driver.find_element_by_name("continue")
field_continue.click()
# TODO: login failed check!

name_field = driver.find_element_by_id("formPersonensucheId:tabId:nameId")
name_field.send_keys("TestMann")
firstname_field =  driver.find_element_by_id("formPersonensucheId:tabId:j_idt298")
firstname_field.send_keys("Manni")
birthdate_field = driver.find_element_by_id("formPersonensucheId:tabId:j_idt306")
birthdate_field.send_keys("01.01.1980")
button = driver.find_element_by_id("formPersonensucheId:tabId:buttonSearch0Id")
button.click()
# TODO: what if already known?
neue_privatperson_button = driver.find_element_by_id("formPersonensucheId:j_idt592")
neue_privatperson_button.click()

anrede = driver.find_element_by_id("DialogPrivatpersonActionId:formId:j_idt628:wertebereichId_label")
anrede.click()
herr = driver.find_element_by_id("DialogPrivatpersonActionId:formId:j_idt628:wertebereichId_2")
herr.click()
sprache = driver.find_element_by_id("DialogPrivatpersonActionId:formId:j_idt633:wertebereichId_label")
sprache.click()
deutsch = driver.find_element_by_id("DialogPrivatpersonActionId:formId:j_idt633:wertebereichId_1")
deutsch.click()
strasse = driver.find_element_by_id("DialogPrivatpersonActionId:formId:strasseId")
strasse.send_keys("Hutgasse")
hausnummer = driver.find_element_by_id("DialogPrivatpersonActionId:formId:hausnummerId")
hausnummer.send_keys("1")
PLZ = driver.find_element_by_id("DialogPrivatpersonActionId:formId:privatpersonWohnadressePlzId")
PLZ.send_keys("4001")
ort = driver.find_element_by_id("DialogPrivatpersonActionId:formId:privatpersonWohnadresseOrtId")
ort.send_keys("Basel")
finish_button = driver.find_element_by_id("DialogPrivatpersonActionId:formId:j_idt735")
finish_button.click()

driver.close()

# driver.get("http://www.python.org")
# assert "Python" in driver.title
# elem = driver.find_element_by_name("q")
# elem.clear()
# elem.send_keys("pycon")
# elem.send_keys(Keys.RETURN)
# assert "No results found." not in driver.page_source
# driver.close()
