# create the truststore analogous to ch-mdm-syrius-consumer. Certificates are obtained from that application, too
cacrtfile="cluster-ca.crt"
syriuscrt="syrius.crt"
password="changeit"
truststore_jks="user-truststore.jks"

keytool -import -noprompt \
-keystore $truststore_jks \
-file $cacrtfile \
-storepass $password \
-alias kafka-cluster-ca-cert && \
keytool -import -noprompt \
-keystore $truststore_jks \
-file $syriuscrt \
-storepass $password \
-alias syriuscrt