/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class SiteEntry extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 5547379263947195657L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"SiteEntry\",\"namespace\":\"inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site\",\"fields\":[{\"name\":\"flat\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasChannel:flat\"},{\"name\":\"floor\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasChannel:floor\"},{\"name\":\"geoCoordinates\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasChannel:geoCoordinates\"},{\"name\":\"locationDescription\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasChannel:locationDescription\"},{\"name\":\"TECHExternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasChannel:TECHExternalPID\"},{\"name\":\"TECHInternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasChannel:TECHInternalPID\"},{\"name\":\"TECHSourceSystem\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasChannel:TECHSourceSystem\"},{\"name\":\"TECHUser\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasChannel:TECHUser\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<SiteEntry> ENCODER =
      new BinaryMessageEncoder<SiteEntry>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<SiteEntry> DECODER =
      new BinaryMessageDecoder<SiteEntry>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<SiteEntry> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<SiteEntry> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<SiteEntry>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this SiteEntry to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a SiteEntry from a ByteBuffer. */
  public static SiteEntry fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.lang.CharSequence flat;
  @Deprecated public java.lang.CharSequence floor;
  @Deprecated public java.lang.CharSequence geoCoordinates;
  @Deprecated public java.lang.CharSequence locationDescription;
  @Deprecated public java.lang.CharSequence TECHExternalPID;
  @Deprecated public java.lang.CharSequence TECHInternalPID;
  @Deprecated public java.lang.CharSequence TECHSourceSystem;
  @Deprecated public java.lang.CharSequence TECHUser;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public SiteEntry() {}

  /**
   * All-args constructor.
   * @param flat The new value for flat
   * @param floor The new value for floor
   * @param geoCoordinates The new value for geoCoordinates
   * @param locationDescription The new value for locationDescription
   * @param TECHExternalPID The new value for TECHExternalPID
   * @param TECHInternalPID The new value for TECHInternalPID
   * @param TECHSourceSystem The new value for TECHSourceSystem
   * @param TECHUser The new value for TECHUser
   */
  public SiteEntry(java.lang.CharSequence flat, java.lang.CharSequence floor, java.lang.CharSequence geoCoordinates, java.lang.CharSequence locationDescription, java.lang.CharSequence TECHExternalPID, java.lang.CharSequence TECHInternalPID, java.lang.CharSequence TECHSourceSystem, java.lang.CharSequence TECHUser) {
    this.flat = flat;
    this.floor = floor;
    this.geoCoordinates = geoCoordinates;
    this.locationDescription = locationDescription;
    this.TECHExternalPID = TECHExternalPID;
    this.TECHInternalPID = TECHInternalPID;
    this.TECHSourceSystem = TECHSourceSystem;
    this.TECHUser = TECHUser;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return flat;
    case 1: return floor;
    case 2: return geoCoordinates;
    case 3: return locationDescription;
    case 4: return TECHExternalPID;
    case 5: return TECHInternalPID;
    case 6: return TECHSourceSystem;
    case 7: return TECHUser;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: flat = (java.lang.CharSequence)value$; break;
    case 1: floor = (java.lang.CharSequence)value$; break;
    case 2: geoCoordinates = (java.lang.CharSequence)value$; break;
    case 3: locationDescription = (java.lang.CharSequence)value$; break;
    case 4: TECHExternalPID = (java.lang.CharSequence)value$; break;
    case 5: TECHInternalPID = (java.lang.CharSequence)value$; break;
    case 6: TECHSourceSystem = (java.lang.CharSequence)value$; break;
    case 7: TECHUser = (java.lang.CharSequence)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'flat' field.
   * @return The value of the 'flat' field.
   */
  public java.lang.CharSequence getFlat() {
    return flat;
  }

  /**
   * Sets the value of the 'flat' field.
   * @param value the value to set.
   */
  public void setFlat(java.lang.CharSequence value) {
    this.flat = value;
  }

  /**
   * Gets the value of the 'floor' field.
   * @return The value of the 'floor' field.
   */
  public java.lang.CharSequence getFloor() {
    return floor;
  }

  /**
   * Sets the value of the 'floor' field.
   * @param value the value to set.
   */
  public void setFloor(java.lang.CharSequence value) {
    this.floor = value;
  }

  /**
   * Gets the value of the 'geoCoordinates' field.
   * @return The value of the 'geoCoordinates' field.
   */
  public java.lang.CharSequence getGeoCoordinates() {
    return geoCoordinates;
  }

  /**
   * Sets the value of the 'geoCoordinates' field.
   * @param value the value to set.
   */
  public void setGeoCoordinates(java.lang.CharSequence value) {
    this.geoCoordinates = value;
  }

  /**
   * Gets the value of the 'locationDescription' field.
   * @return The value of the 'locationDescription' field.
   */
  public java.lang.CharSequence getLocationDescription() {
    return locationDescription;
  }

  /**
   * Sets the value of the 'locationDescription' field.
   * @param value the value to set.
   */
  public void setLocationDescription(java.lang.CharSequence value) {
    this.locationDescription = value;
  }

  /**
   * Gets the value of the 'TECHExternalPID' field.
   * @return The value of the 'TECHExternalPID' field.
   */
  public java.lang.CharSequence getTECHExternalPID() {
    return TECHExternalPID;
  }

  /**
   * Sets the value of the 'TECHExternalPID' field.
   * @param value the value to set.
   */
  public void setTECHExternalPID(java.lang.CharSequence value) {
    this.TECHExternalPID = value;
  }

  /**
   * Gets the value of the 'TECHInternalPID' field.
   * @return The value of the 'TECHInternalPID' field.
   */
  public java.lang.CharSequence getTECHInternalPID() {
    return TECHInternalPID;
  }

  /**
   * Sets the value of the 'TECHInternalPID' field.
   * @param value the value to set.
   */
  public void setTECHInternalPID(java.lang.CharSequence value) {
    this.TECHInternalPID = value;
  }

  /**
   * Gets the value of the 'TECHSourceSystem' field.
   * @return The value of the 'TECHSourceSystem' field.
   */
  public java.lang.CharSequence getTECHSourceSystem() {
    return TECHSourceSystem;
  }

  /**
   * Sets the value of the 'TECHSourceSystem' field.
   * @param value the value to set.
   */
  public void setTECHSourceSystem(java.lang.CharSequence value) {
    this.TECHSourceSystem = value;
  }

  /**
   * Gets the value of the 'TECHUser' field.
   * @return The value of the 'TECHUser' field.
   */
  public java.lang.CharSequence getTECHUser() {
    return TECHUser;
  }

  /**
   * Sets the value of the 'TECHUser' field.
   * @param value the value to set.
   */
  public void setTECHUser(java.lang.CharSequence value) {
    this.TECHUser = value;
  }

  /**
   * Creates a new SiteEntry RecordBuilder.
   * @return A new SiteEntry RecordBuilder
   */
  public static inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder newBuilder() {
    return new inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder();
  }

  /**
   * Creates a new SiteEntry RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new SiteEntry RecordBuilder
   */
  public static inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder newBuilder(inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder other) {
    return new inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder(other);
  }

  /**
   * Creates a new SiteEntry RecordBuilder by copying an existing SiteEntry instance.
   * @param other The existing instance to copy.
   * @return A new SiteEntry RecordBuilder
   */
  public static inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder newBuilder(inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry other) {
    return new inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder(other);
  }

  /**
   * RecordBuilder for SiteEntry instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<SiteEntry>
    implements org.apache.avro.data.RecordBuilder<SiteEntry> {

    private java.lang.CharSequence flat;
    private java.lang.CharSequence floor;
    private java.lang.CharSequence geoCoordinates;
    private java.lang.CharSequence locationDescription;
    private java.lang.CharSequence TECHExternalPID;
    private java.lang.CharSequence TECHInternalPID;
    private java.lang.CharSequence TECHSourceSystem;
    private java.lang.CharSequence TECHUser;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.flat)) {
        this.flat = data().deepCopy(fields()[0].schema(), other.flat);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.floor)) {
        this.floor = data().deepCopy(fields()[1].schema(), other.floor);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.geoCoordinates)) {
        this.geoCoordinates = data().deepCopy(fields()[2].schema(), other.geoCoordinates);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.locationDescription)) {
        this.locationDescription = data().deepCopy(fields()[3].schema(), other.locationDescription);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.TECHExternalPID)) {
        this.TECHExternalPID = data().deepCopy(fields()[4].schema(), other.TECHExternalPID);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.TECHInternalPID)) {
        this.TECHInternalPID = data().deepCopy(fields()[5].schema(), other.TECHInternalPID);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.TECHSourceSystem)) {
        this.TECHSourceSystem = data().deepCopy(fields()[6].schema(), other.TECHSourceSystem);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.TECHUser)) {
        this.TECHUser = data().deepCopy(fields()[7].schema(), other.TECHUser);
        fieldSetFlags()[7] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing SiteEntry instance
     * @param other The existing instance to copy.
     */
    private Builder(inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.flat)) {
        this.flat = data().deepCopy(fields()[0].schema(), other.flat);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.floor)) {
        this.floor = data().deepCopy(fields()[1].schema(), other.floor);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.geoCoordinates)) {
        this.geoCoordinates = data().deepCopy(fields()[2].schema(), other.geoCoordinates);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.locationDescription)) {
        this.locationDescription = data().deepCopy(fields()[3].schema(), other.locationDescription);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.TECHExternalPID)) {
        this.TECHExternalPID = data().deepCopy(fields()[4].schema(), other.TECHExternalPID);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.TECHInternalPID)) {
        this.TECHInternalPID = data().deepCopy(fields()[5].schema(), other.TECHInternalPID);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.TECHSourceSystem)) {
        this.TECHSourceSystem = data().deepCopy(fields()[6].schema(), other.TECHSourceSystem);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.TECHUser)) {
        this.TECHUser = data().deepCopy(fields()[7].schema(), other.TECHUser);
        fieldSetFlags()[7] = true;
      }
    }

    /**
      * Gets the value of the 'flat' field.
      * @return The value.
      */
    public java.lang.CharSequence getFlat() {
      return flat;
    }

    /**
      * Sets the value of the 'flat' field.
      * @param value The value of 'flat'.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder setFlat(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.flat = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'flat' field has been set.
      * @return True if the 'flat' field has been set, false otherwise.
      */
    public boolean hasFlat() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'flat' field.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder clearFlat() {
      flat = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'floor' field.
      * @return The value.
      */
    public java.lang.CharSequence getFloor() {
      return floor;
    }

    /**
      * Sets the value of the 'floor' field.
      * @param value The value of 'floor'.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder setFloor(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.floor = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'floor' field has been set.
      * @return True if the 'floor' field has been set, false otherwise.
      */
    public boolean hasFloor() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'floor' field.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder clearFloor() {
      floor = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'geoCoordinates' field.
      * @return The value.
      */
    public java.lang.CharSequence getGeoCoordinates() {
      return geoCoordinates;
    }

    /**
      * Sets the value of the 'geoCoordinates' field.
      * @param value The value of 'geoCoordinates'.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder setGeoCoordinates(java.lang.CharSequence value) {
      validate(fields()[2], value);
      this.geoCoordinates = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'geoCoordinates' field has been set.
      * @return True if the 'geoCoordinates' field has been set, false otherwise.
      */
    public boolean hasGeoCoordinates() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'geoCoordinates' field.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder clearGeoCoordinates() {
      geoCoordinates = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'locationDescription' field.
      * @return The value.
      */
    public java.lang.CharSequence getLocationDescription() {
      return locationDescription;
    }

    /**
      * Sets the value of the 'locationDescription' field.
      * @param value The value of 'locationDescription'.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder setLocationDescription(java.lang.CharSequence value) {
      validate(fields()[3], value);
      this.locationDescription = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'locationDescription' field has been set.
      * @return True if the 'locationDescription' field has been set, false otherwise.
      */
    public boolean hasLocationDescription() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'locationDescription' field.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder clearLocationDescription() {
      locationDescription = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /**
      * Gets the value of the 'TECHExternalPID' field.
      * @return The value.
      */
    public java.lang.CharSequence getTECHExternalPID() {
      return TECHExternalPID;
    }

    /**
      * Sets the value of the 'TECHExternalPID' field.
      * @param value The value of 'TECHExternalPID'.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder setTECHExternalPID(java.lang.CharSequence value) {
      validate(fields()[4], value);
      this.TECHExternalPID = value;
      fieldSetFlags()[4] = true;
      return this;
    }

    /**
      * Checks whether the 'TECHExternalPID' field has been set.
      * @return True if the 'TECHExternalPID' field has been set, false otherwise.
      */
    public boolean hasTECHExternalPID() {
      return fieldSetFlags()[4];
    }


    /**
      * Clears the value of the 'TECHExternalPID' field.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder clearTECHExternalPID() {
      TECHExternalPID = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    /**
      * Gets the value of the 'TECHInternalPID' field.
      * @return The value.
      */
    public java.lang.CharSequence getTECHInternalPID() {
      return TECHInternalPID;
    }

    /**
      * Sets the value of the 'TECHInternalPID' field.
      * @param value The value of 'TECHInternalPID'.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder setTECHInternalPID(java.lang.CharSequence value) {
      validate(fields()[5], value);
      this.TECHInternalPID = value;
      fieldSetFlags()[5] = true;
      return this;
    }

    /**
      * Checks whether the 'TECHInternalPID' field has been set.
      * @return True if the 'TECHInternalPID' field has been set, false otherwise.
      */
    public boolean hasTECHInternalPID() {
      return fieldSetFlags()[5];
    }


    /**
      * Clears the value of the 'TECHInternalPID' field.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder clearTECHInternalPID() {
      TECHInternalPID = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    /**
      * Gets the value of the 'TECHSourceSystem' field.
      * @return The value.
      */
    public java.lang.CharSequence getTECHSourceSystem() {
      return TECHSourceSystem;
    }

    /**
      * Sets the value of the 'TECHSourceSystem' field.
      * @param value The value of 'TECHSourceSystem'.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder setTECHSourceSystem(java.lang.CharSequence value) {
      validate(fields()[6], value);
      this.TECHSourceSystem = value;
      fieldSetFlags()[6] = true;
      return this;
    }

    /**
      * Checks whether the 'TECHSourceSystem' field has been set.
      * @return True if the 'TECHSourceSystem' field has been set, false otherwise.
      */
    public boolean hasTECHSourceSystem() {
      return fieldSetFlags()[6];
    }


    /**
      * Clears the value of the 'TECHSourceSystem' field.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder clearTECHSourceSystem() {
      TECHSourceSystem = null;
      fieldSetFlags()[6] = false;
      return this;
    }

    /**
      * Gets the value of the 'TECHUser' field.
      * @return The value.
      */
    public java.lang.CharSequence getTECHUser() {
      return TECHUser;
    }

    /**
      * Sets the value of the 'TECHUser' field.
      * @param value The value of 'TECHUser'.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder setTECHUser(java.lang.CharSequence value) {
      validate(fields()[7], value);
      this.TECHUser = value;
      fieldSetFlags()[7] = true;
      return this;
    }

    /**
      * Checks whether the 'TECHUser' field has been set.
      * @return True if the 'TECHUser' field has been set, false otherwise.
      */
    public boolean hasTECHUser() {
      return fieldSetFlags()[7];
    }


    /**
      * Clears the value of the 'TECHUser' field.
      * @return This builder.
      */
    public inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasSite.hasSiteSchema.Site.SiteEntry.Builder clearTECHUser() {
      TECHUser = null;
      fieldSetFlags()[7] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public SiteEntry build() {
      try {
        SiteEntry record = new SiteEntry();
        record.flat = fieldSetFlags()[0] ? this.flat : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.floor = fieldSetFlags()[1] ? this.floor : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.geoCoordinates = fieldSetFlags()[2] ? this.geoCoordinates : (java.lang.CharSequence) defaultValue(fields()[2]);
        record.locationDescription = fieldSetFlags()[3] ? this.locationDescription : (java.lang.CharSequence) defaultValue(fields()[3]);
        record.TECHExternalPID = fieldSetFlags()[4] ? this.TECHExternalPID : (java.lang.CharSequence) defaultValue(fields()[4]);
        record.TECHInternalPID = fieldSetFlags()[5] ? this.TECHInternalPID : (java.lang.CharSequence) defaultValue(fields()[5]);
        record.TECHSourceSystem = fieldSetFlags()[6] ? this.TECHSourceSystem : (java.lang.CharSequence) defaultValue(fields()[6]);
        record.TECHUser = fieldSetFlags()[7] ? this.TECHUser : (java.lang.CharSequence) defaultValue(fields()[7]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<SiteEntry>
    WRITER$ = (org.apache.avro.io.DatumWriter<SiteEntry>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<SiteEntry>
    READER$ = (org.apache.avro.io.DatumReader<SiteEntry>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
