
package com.adcubum.syrius.api.common.faults.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Fehlertyp, welcher zurückgeliefert wird, wenn eine Validierung fehlschlägt.
 * 
 * <p>Java class for ValidationFaultInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidationFaultInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:faults:v1}ApiFaultInfoType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidationFaultInfoType")
public class ValidationFaultInfoType
    extends ApiFaultInfoType
{


}
