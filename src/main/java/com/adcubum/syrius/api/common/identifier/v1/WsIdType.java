
package com.adcubum.syrius.api.common.identifier.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAdresseAenderungIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAdresseDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAdresseIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAdressrolleDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAdressrolleIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAkteIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAuslaenderstatusIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsBankIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsBenutzerIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsBeziehungDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsBeziehungIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsCodeIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsDokumenttypIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsDokumentversandIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsEmailDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsEmailIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsFaktDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsFaktIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsFalldossierIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsGbereichIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsIntermediateEventDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsKommVerbVerwendungIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsKontaktIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsLandIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsLeistungserbringerIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsMessageTaskDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsMutationsgrundIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsOrganisationseinheitIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerAenderungIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartneridentifikatorDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartneridentifikatorIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerrolleAttrDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerrolleDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerrolleIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerschutzDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsProcessDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsProcessIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsRechnungsstellerTypIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsRegionTypIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsSepaMandatIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsTelefonDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsTelefonIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsTextModulIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsUserTaskDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsUserTaskIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsVertragIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsWaehrungIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsWebDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsWebIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsZahlungsverbindungIdType;


/**
 * Abstrakter Basistyp für in Webservices vorkommende Ids.
 * 
 * <p>Java class for WsIdType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsIdType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsIdType", propOrder = {
    "id"
})
@XmlSeeAlso({
    WsAdresseIdType.class,
    WsAdressrolleIdType.class,
    WsAkteIdType.class,
    WsAuslaenderstatusIdType.class,
    WsBenutzerIdType.class,
    WsBeziehungIdType.class,
    WsEmailIdType.class,
    WsFaktIdType.class,
    WsKommVerbVerwendungIdType.class,
    WsKontaktIdType.class,
    WsLeistungserbringerIdType.class,
    WsOrganisationseinheitIdType.class,
    WsPartnerIdType.class,
    WsPartneridentifikatorIdType.class,
    WsPartnerrolleIdType.class,
    WsSepaMandatIdType.class,
    WsTelefonIdType.class,
    WsTextModulIdType.class,
    WsWebIdType.class,
    WsZahlungsverbindungIdType.class,
    WsAdresseDefIdType.class,
    WsAdressrolleDefIdType.class,
    WsBeziehungDefIdType.class,
    WsEmailDefIdType.class,
    WsFaktDefIdType.class,
    WsPartneridentifikatorDefIdType.class,
    WsPartnerrolleDefIdType.class,
    WsPartnerrolleAttrDefIdType.class,
    WsPartnerschutzDefIdType.class,
    WsTelefonDefIdType.class,
    WsWebDefIdType.class,
    WsMutationsgrundIdType.class,
    WsBankIdType.class,
    WsLandIdType.class,
    WsVertragIdType.class,
    WsWaehrungIdType.class,
    WsAdresseAenderungIdType.class,
    WsRechnungsstellerTypIdType.class,
    WsPartnerAenderungIdType.class,
    WsGbereichIdType.class,
    WsDokumentversandIdType.class,
    WsDokumenttypIdType.class,
    WsFalldossierIdType.class,
    WsRegionTypIdType.class,
    WsProcessIdType.class,
    WsProcessDefIdType.class,
    WsUserTaskIdType.class,
    WsUserTaskDefIdType.class,
    WsIntermediateEventDefIdType.class,
    WsMessageTaskDefIdType.class,
    WsCodeIdType.class
})
public abstract class WsIdType {

    @XmlElement(required = true)
    protected String id;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
