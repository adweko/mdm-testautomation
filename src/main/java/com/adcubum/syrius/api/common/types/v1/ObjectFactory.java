
package com.adcubum.syrius.api.common.types.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.adcubum.syrius.api.common.types.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.adcubum.syrius.api.common.types.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WsLocalizedTextType }
     * 
     */
    public WsLocalizedTextType createWsLocalizedTextType() {
        return new WsLocalizedTextType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeType }
     * 
     */
    public WsCustomAttributeType createWsCustomAttributeType() {
        return new WsCustomAttributeType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeStringType }
     * 
     */
    public WsCustomAttributeStringType createWsCustomAttributeStringType() {
        return new WsCustomAttributeStringType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeIntegerType }
     * 
     */
    public WsCustomAttributeIntegerType createWsCustomAttributeIntegerType() {
        return new WsCustomAttributeIntegerType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeLongType }
     * 
     */
    public WsCustomAttributeLongType createWsCustomAttributeLongType() {
        return new WsCustomAttributeLongType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeDecimalType }
     * 
     */
    public WsCustomAttributeDecimalType createWsCustomAttributeDecimalType() {
        return new WsCustomAttributeDecimalType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeBooleanType }
     * 
     */
    public WsCustomAttributeBooleanType createWsCustomAttributeBooleanType() {
        return new WsCustomAttributeBooleanType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeDateType }
     * 
     */
    public WsCustomAttributeDateType createWsCustomAttributeDateType() {
        return new WsCustomAttributeDateType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeDateTimeType }
     * 
     */
    public WsCustomAttributeDateTimeType createWsCustomAttributeDateTimeType() {
        return new WsCustomAttributeDateTimeType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeTimestampType }
     * 
     */
    public WsCustomAttributeTimestampType createWsCustomAttributeTimestampType() {
        return new WsCustomAttributeTimestampType();
    }

    /**
     * Create an instance of {@link WsCustomAttributeCodeType }
     * 
     */
    public WsCustomAttributeCodeType createWsCustomAttributeCodeType() {
        return new WsCustomAttributeCodeType();
    }

    /**
     * Create an instance of {@link WsHistObjectType }
     * 
     */
    public WsHistObjectType createWsHistObjectType() {
        return new WsHistObjectType();
    }

    /**
     * Create an instance of {@link WsLifecycleObjectType }
     * 
     */
    public WsLifecycleObjectType createWsLifecycleObjectType() {
        return new WsLifecycleObjectType();
    }

    /**
     * Create an instance of {@link WsDataObjectType }
     * 
     */
    public WsDataObjectType createWsDataObjectType() {
        return new WsDataObjectType();
    }

}
