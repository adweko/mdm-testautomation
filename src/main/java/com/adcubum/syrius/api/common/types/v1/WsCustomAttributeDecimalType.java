
package com.adcubum.syrius.api.common.types.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 Objekt zur Darstellung beliebiger Decimal-Attribute.
 *             
 * 
 * <p>Java class for WsCustomAttributeDecimalType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCustomAttributeDecimalType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:types:v1}WsCustomAttributeType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="attrValue" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}DecimalType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCustomAttributeDecimalType", propOrder = {
    "attrValue"
})
public class WsCustomAttributeDecimalType
    extends WsCustomAttributeType
{

    protected BigDecimal attrValue;

    /**
     * Gets the value of the attrValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAttrValue() {
        return attrValue;
    }

    /**
     * Sets the value of the attrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAttrValue(BigDecimal value) {
        this.attrValue = value;
    }

}
