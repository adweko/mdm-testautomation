
package com.adcubum.syrius.api.common.types.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 Objekt zur Darstellung beliebiger Attribute.
 *             
 * 
 * <p>Java class for WsCustomAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCustomAttributeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="attrId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCustomAttributeType", propOrder = {
    "attrId"
})
@XmlSeeAlso({
    WsCustomAttributeStringType.class,
    WsCustomAttributeIntegerType.class,
    WsCustomAttributeLongType.class,
    WsCustomAttributeDecimalType.class,
    WsCustomAttributeBooleanType.class,
    WsCustomAttributeDateType.class,
    WsCustomAttributeDateTimeType.class,
    WsCustomAttributeTimestampType.class,
    WsCustomAttributeCodeType.class
})
public class WsCustomAttributeType {

    @XmlElement(required = true)
    protected String attrId;

    /**
     * Gets the value of the attrId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrId() {
        return attrId;
    }

    /**
     * Sets the value of the attrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrId(String value) {
        this.attrId = value;
    }

}
