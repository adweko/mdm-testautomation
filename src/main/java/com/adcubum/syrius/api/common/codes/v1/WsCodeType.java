
package com.adcubum.syrius.api.common.codes.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsAdrAendBegruendungCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsAdrAendStatusCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsAktivCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsArbeitslosCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsAufenthaltsbewCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsBerufsgruppeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsBrancheCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsDublBeziehungCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsEinzugsermaechtigungCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsEntschaedigungCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsErlaubnisCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsErlaubnisartCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsFaktStatusCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsGemeindeKantonCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsGeschlechtCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsInAusbildungCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKantonCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKommVerbTypCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKontaktAnredeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKontaktschutzCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKonzernpartnerCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsLeistungspflichtCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsLerbAnredeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsLstAbrMoeglichCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsMandatArtCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsMehrfachverwendungCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsNutzungszweckCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsOrganisationTypCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsPartAendBegruendungCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsPartAendStatusCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsPartnerTypCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsPatientenstatusCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsPflegestufeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsPostfachOhneNrCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsRechtsformCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsRegionCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsSozialhilfeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsStilleKoGuCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsStrasseNrReihenfolgeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsSystemCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsTitelCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsUnterstPflichtCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsVerbandCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsVipCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsZivilstandCodeType;


/**
 * Code
 * 
 * <p>Java class for WsCodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codeId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCodeType", propOrder = {
    "codeId"
})
@XmlSeeAlso({
    WsAktivCodeType.class,
    WsArbeitslosCodeType.class,
    WsAufenthaltsbewCodeType.class,
    WsBerufsgruppeCodeType.class,
    WsBrancheCodeType.class,
    WsEinzugsermaechtigungCodeType.class,
    WsEntschaedigungCodeType.class,
    WsFaktStatusCodeType.class,
    WsGeschlechtCodeType.class,
    WsInAusbildungCodeType.class,
    WsKontaktAnredeCodeType.class,
    WsKontaktschutzCodeType.class,
    WsKonzernpartnerCodeType.class,
    WsLeistungspflichtCodeType.class,
    WsLerbAnredeCodeType.class,
    WsLstAbrMoeglichCodeType.class,
    WsOrganisationTypCodeType.class,
    WsRechtsformCodeType.class,
    WsSozialhilfeCodeType.class,
    WsStilleKoGuCodeType.class,
    WsStrasseNrReihenfolgeCodeType.class,
    WsPostfachOhneNrCodeType.class,
    WsSystemCodeType.class,
    WsTitelCodeType.class,
    WsUnterstPflichtCodeType.class,
    WsVerbandCodeType.class,
    WsVipCodeType.class,
    WsZivilstandCodeType.class,
    WsKantonCodeType.class,
    WsGemeindeKantonCodeType.class,
    WsMehrfachverwendungCodeType.class,
    WsRegionCodeType.class,
    WsPflegestufeCodeType.class,
    WsPatientenstatusCodeType.class,
    WsAdrAendStatusCodeType.class,
    WsAdrAendBegruendungCodeType.class,
    WsPartAendStatusCodeType.class,
    WsPartAendBegruendungCodeType.class,
    WsMandatArtCodeType.class,
    WsErlaubnisCodeType.class,
    WsErlaubnisartCodeType.class,
    WsKommVerbTypCodeType.class,
    WsDublBeziehungCodeType.class,
    WsPartnerTypCodeType.class,
    WsNutzungszweckCodeType.class
})
public class WsCodeType {

    @XmlElement(required = true)
    protected String codeId;

    /**
     * Gets the value of the codeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeId() {
        return codeId;
    }

    /**
     * Sets the value of the codeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeId(String value) {
        this.codeId = value;
    }

}
