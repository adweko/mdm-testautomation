
package com.adcubum.syrius.api.common.types.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *                 Basistyp eines Datenobjekts
 *             
 * 
 * <p>Java class for WsDataObjectType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsDataObjectType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aenderungAm" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}TimestampType"/&gt;
 *         &lt;element name="aenderungDurch" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="customAttribute" type="{urn:com:adcubum:syrius:api:common:types:v1}WsCustomAttributeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsDataObjectType", propOrder = {
    "aenderungAm",
    "aenderungDurch",
    "customAttribute"
})
@XmlSeeAlso({
    WsLifecycleObjectType.class
})
public class WsDataObjectType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aenderungAm;
    @XmlElement(required = true)
    protected String aenderungDurch;
    protected List<WsCustomAttributeType> customAttribute;

    /**
     * Gets the value of the aenderungAm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAenderungAm() {
        return aenderungAm;
    }

    /**
     * Sets the value of the aenderungAm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAenderungAm(XMLGregorianCalendar value) {
        this.aenderungAm = value;
    }

    /**
     * Gets the value of the aenderungDurch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAenderungDurch() {
        return aenderungDurch;
    }

    /**
     * Sets the value of the aenderungDurch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAenderungDurch(String value) {
        this.aenderungDurch = value;
    }

    /**
     * Gets the value of the customAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsCustomAttributeType }
     * 
     * 
     */
    public List<WsCustomAttributeType> getCustomAttribute() {
        if (customAttribute == null) {
            customAttribute = new ArrayList<WsCustomAttributeType>();
        }
        return this.customAttribute;
    }

}
