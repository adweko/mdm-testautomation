
package com.adcubum.syrius.api.common.types.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.common.codes.v1.WsCodeType;


/**
 * 
 *                 Objekt zur Darstellung beliebiger Code-Attribute.
 *             
 * 
 * <p>Java class for WsCustomAttributeCodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCustomAttributeCodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:types:v1}WsCustomAttributeType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="attrValue" type="{urn:com:adcubum:syrius:api:common:codes:v1}WsCodeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCustomAttributeCodeType", propOrder = {
    "attrValue"
})
public class WsCustomAttributeCodeType
    extends WsCustomAttributeType
{

    protected WsCodeType attrValue;

    /**
     * Gets the value of the attrValue property.
     * 
     * @return
     *     possible object is
     *     {@link WsCodeType }
     *     
     */
    public WsCodeType getAttrValue() {
        return attrValue;
    }

    /**
     * Sets the value of the attrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsCodeType }
     *     
     */
    public void setAttrValue(WsCodeType value) {
        this.attrValue = value;
    }

}
