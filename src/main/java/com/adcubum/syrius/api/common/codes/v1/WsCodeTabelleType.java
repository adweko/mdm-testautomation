
package com.adcubum.syrius.api.common.codes.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * CodeTabelle
 * 
 * <p>Java class for WsCodeTabelleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCodeTabelleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codeEntry" type="{urn:com:adcubum:syrius:api:common:codes:v1}WsCodeEntryType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCodeTabelleType", propOrder = {
    "codeEntry"
})
public class WsCodeTabelleType {

    protected List<WsCodeEntryType> codeEntry;

    /**
     * Gets the value of the codeEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codeEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodeEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsCodeEntryType }
     * 
     * 
     */
    public List<WsCodeEntryType> getCodeEntry() {
        if (codeEntry == null) {
            codeEntry = new ArrayList<WsCodeEntryType>();
        }
        return this.codeEntry;
    }

}
