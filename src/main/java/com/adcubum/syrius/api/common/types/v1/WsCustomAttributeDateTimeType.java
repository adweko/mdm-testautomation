
package com.adcubum.syrius.api.common.types.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *                 Objekt zur Darstellung beliebiger DateTime-Attribute.
 *             
 * 
 * <p>Java class for WsCustomAttributeDateTimeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCustomAttributeDateTimeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:types:v1}WsCustomAttributeType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="attrValue" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}DateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCustomAttributeDateTimeType", propOrder = {
    "attrValue"
})
public class WsCustomAttributeDateTimeType
    extends WsCustomAttributeType
{

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar attrValue;

    /**
     * Gets the value of the attrValue property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAttrValue() {
        return attrValue;
    }

    /**
     * Sets the value of the attrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAttrValue(XMLGregorianCalendar value) {
        this.attrValue = value;
    }

}
