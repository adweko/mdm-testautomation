
package com.adcubum.syrius.api.common.types.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 Objekt zur Darstellung beliebiger Boolean-Attribute.
 *             
 * 
 * <p>Java class for WsCustomAttributeBooleanType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCustomAttributeBooleanType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:types:v1}WsCustomAttributeType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="attrValue" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}BooleanType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCustomAttributeBooleanType", propOrder = {
    "attrValue"
})
public class WsCustomAttributeBooleanType
    extends WsCustomAttributeType
{

    protected Integer attrValue;

    /**
     * Gets the value of the attrValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAttrValue() {
        return attrValue;
    }

    /**
     * Sets the value of the attrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAttrValue(Integer value) {
        this.attrValue = value;
    }

}
