
package com.adcubum.syrius.api.common.types.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *                 Objekt zur Darstellung beliebiger Date-Attribute.
 *             
 * 
 * <p>Java class for WsCustomAttributeDateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCustomAttributeDateType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:types:v1}WsCustomAttributeType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="attrValue" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}DateType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCustomAttributeDateType", propOrder = {
    "attrValue"
})
public class WsCustomAttributeDateType
    extends WsCustomAttributeType
{

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar attrValue;

    /**
     * Gets the value of the attrValue property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAttrValue() {
        return attrValue;
    }

    /**
     * Sets the value of the attrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAttrValue(XMLGregorianCalendar value) {
        this.attrValue = value;
    }

}
