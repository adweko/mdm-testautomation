
package com.adcubum.syrius.api.common.faults.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Fehlertyp, welcher zurückgeliefert wird, wenn ein Konflikt auftritt.
 * 
 * <p>Java class for ConflictFaultInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConflictFaultInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:faults:v1}ApiFaultInfoType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="conflict" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConflictFaultInfoType", propOrder = {
    "conflict"
})
public class ConflictFaultInfoType
    extends ApiFaultInfoType
{

    protected String conflict;

    /**
     * Gets the value of the conflict property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConflict() {
        return conflict;
    }

    /**
     * Sets the value of the conflict property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConflict(String value) {
        this.conflict = value;
    }

}
