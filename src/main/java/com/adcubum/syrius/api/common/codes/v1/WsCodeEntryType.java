
package com.adcubum.syrius.api.common.codes.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * CodeEntry
 * 
 * <p>Java class for WsCodeEntryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsCodeEntryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="codeId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" /&gt;
 *       &lt;attribute name="visible" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}BooleanType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsCodeEntryType")
public class WsCodeEntryType {

    @XmlAttribute(name = "codeId")
    protected String codeId;
    @XmlAttribute(name = "visible")
    protected Integer visible;

    /**
     * Gets the value of the codeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeId() {
        return codeId;
    }

    /**
     * Sets the value of the codeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeId(String value) {
        this.codeId = value;
    }

    /**
     * Gets the value of the visible property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVisible() {
        return visible;
    }

    /**
     * Sets the value of the visible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVisible(Integer value) {
        this.visible = value;
    }

}
