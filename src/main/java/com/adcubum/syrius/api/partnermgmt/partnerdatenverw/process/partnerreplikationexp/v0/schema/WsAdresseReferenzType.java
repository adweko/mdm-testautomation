
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAdresseIdType;


/**
 * Referenz auf eine Adresse mit einer techn. und/oder einer externen Id.
 * 
 * <p>Java class for WsAdresseReferenzType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAdresseReferenzType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="externeReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsExterneReferenzType" minOccurs="0"/&gt;
 *         &lt;element name="adresseId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsAdresseIdType" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAdresseReferenzType", propOrder = {

})
public class WsAdresseReferenzType {

    protected WsExterneReferenzType externeReferenz;
    protected WsAdresseIdType adresseId;

    /**
     * Gets the value of the externeReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public WsExterneReferenzType getExterneReferenz() {
        return externeReferenz;
    }

    /**
     * Sets the value of the externeReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public void setExterneReferenz(WsExterneReferenzType value) {
        this.externeReferenz = value;
    }

    /**
     * Gets the value of the adresseId property.
     * 
     * @return
     *     possible object is
     *     {@link WsAdresseIdType }
     *     
     */
    public WsAdresseIdType getAdresseId() {
        return adresseId;
    }

    /**
     * Sets the value of the adresseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAdresseIdType }
     *     
     */
    public void setAdresseId(WsAdresseIdType value) {
        this.adresseId = value;
    }

}
