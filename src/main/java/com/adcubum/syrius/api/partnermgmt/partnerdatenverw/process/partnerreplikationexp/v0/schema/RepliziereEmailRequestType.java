
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Request für das Replizieren einer Email-Adresse.
 * 
 * <p>Java class for RepliziereEmailRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliziereEmailRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="emailReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsEmailReferenzType"/&gt;
 *         &lt;element name="partnerReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerReferenzType"/&gt;
 *         &lt;element name="messageId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="emailStruktur" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsEmailStrukturType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliziereEmailRequestType", propOrder = {
    "emailReferenz",
    "partnerReferenz",
    "messageId",
    "emailStruktur"
})
public class RepliziereEmailRequestType {

    @XmlElement(required = true)
    protected WsEmailReferenzType emailReferenz;
    @XmlElement(required = true)
    protected WsPartnerReferenzType partnerReferenz;
    @XmlElement(required = true)
    protected String messageId;
    @XmlElement(required = true)
    protected WsEmailStrukturType emailStruktur;

    /**
     * Gets the value of the emailReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsEmailReferenzType }
     *     
     */
    public WsEmailReferenzType getEmailReferenz() {
        return emailReferenz;
    }

    /**
     * Sets the value of the emailReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsEmailReferenzType }
     *     
     */
    public void setEmailReferenz(WsEmailReferenzType value) {
        this.emailReferenz = value;
    }

    /**
     * Gets the value of the partnerReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public WsPartnerReferenzType getPartnerReferenz() {
        return partnerReferenz;
    }

    /**
     * Sets the value of the partnerReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public void setPartnerReferenz(WsPartnerReferenzType value) {
        this.partnerReferenz = value;
    }

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the emailStruktur property.
     * 
     * @return
     *     possible object is
     *     {@link WsEmailStrukturType }
     *     
     */
    public WsEmailStrukturType getEmailStruktur() {
        return emailStruktur;
    }

    /**
     * Sets the value of the emailStruktur property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsEmailStrukturType }
     *     
     */
    public void setEmailStruktur(WsEmailStrukturType value) {
        this.emailStruktur = value;
    }

}
