
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAdressrolleDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAdressrolleIdType;


/**
 * Adressrolle einer Adresse.
 * 
 * <p>Java class for WsAdressrolleStrukturType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAdressrolleStrukturType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="externeReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsExterneReferenzType" minOccurs="0"/&gt;
 *         &lt;element name="id" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsAdressrolleIdType" minOccurs="0"/&gt;
 *         &lt;element name="gueltAb" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="gueltBis" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="adressrolleDefId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsAdressrolleDefIdType"/&gt;
 *         &lt;element name="adressrolleStates" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsAdressrolleType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAdressrolleStrukturType", propOrder = {
    "externeReferenz",
    "id",
    "gueltAb",
    "gueltBis",
    "adressrolleDefId",
    "adressrolleStates"
})
public class WsAdressrolleStrukturType {

    protected WsExterneReferenzType externeReferenz;
    protected WsAdressrolleIdType id;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltAb;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltBis;
    @XmlElement(required = true)
    protected WsAdressrolleDefIdType adressrolleDefId;
    protected List<WsAdressrolleType> adressrolleStates;

    /**
     * Gets the value of the externeReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public WsExterneReferenzType getExterneReferenz() {
        return externeReferenz;
    }

    /**
     * Sets the value of the externeReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public void setExterneReferenz(WsExterneReferenzType value) {
        this.externeReferenz = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link WsAdressrolleIdType }
     *     
     */
    public WsAdressrolleIdType getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAdressrolleIdType }
     *     
     */
    public void setId(WsAdressrolleIdType value) {
        this.id = value;
    }

    /**
     * Gets the value of the gueltAb property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltAb() {
        return gueltAb;
    }

    /**
     * Sets the value of the gueltAb property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltAb(XMLGregorianCalendar value) {
        this.gueltAb = value;
    }

    /**
     * Gets the value of the gueltBis property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltBis() {
        return gueltBis;
    }

    /**
     * Sets the value of the gueltBis property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltBis(XMLGregorianCalendar value) {
        this.gueltBis = value;
    }

    /**
     * Gets the value of the adressrolleDefId property.
     * 
     * @return
     *     possible object is
     *     {@link WsAdressrolleDefIdType }
     *     
     */
    public WsAdressrolleDefIdType getAdressrolleDefId() {
        return adressrolleDefId;
    }

    /**
     * Sets the value of the adressrolleDefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAdressrolleDefIdType }
     *     
     */
    public void setAdressrolleDefId(WsAdressrolleDefIdType value) {
        this.adressrolleDefId = value;
    }

    /**
     * Gets the value of the adressrolleStates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adressrolleStates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdressrolleStates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAdressrolleType }
     * 
     * 
     */
    public List<WsAdressrolleType> getAdressrolleStates() {
        if (adressrolleStates == null) {
            adressrolleStates = new ArrayList<WsAdressrolleType>();
        }
        return this.adressrolleStates;
    }

}
