
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsTelefonIdType;


/**
 * Referenz auf eine Telefon-Nummer mit einer techn. und/oder einer externen Id.
 * 
 * <p>Java class for WsTelefonReferenzType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsTelefonReferenzType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="externeReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsExterneReferenzType" minOccurs="0"/&gt;
 *         &lt;element name="telefonId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsTelefonIdType" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsTelefonReferenzType", propOrder = {

})
public class WsTelefonReferenzType {

    protected WsExterneReferenzType externeReferenz;
    protected WsTelefonIdType telefonId;

    /**
     * Gets the value of the externeReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public WsExterneReferenzType getExterneReferenz() {
        return externeReferenz;
    }

    /**
     * Sets the value of the externeReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public void setExterneReferenz(WsExterneReferenzType value) {
        this.externeReferenz = value;
    }

    /**
     * Gets the value of the telefonId property.
     * 
     * @return
     *     possible object is
     *     {@link WsTelefonIdType }
     *     
     */
    public WsTelefonIdType getTelefonId() {
        return telefonId;
    }

    /**
     * Sets the value of the telefonId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsTelefonIdType }
     *     
     */
    public void setTelefonId(WsTelefonIdType value) {
        this.telefonId = value;
    }

}
