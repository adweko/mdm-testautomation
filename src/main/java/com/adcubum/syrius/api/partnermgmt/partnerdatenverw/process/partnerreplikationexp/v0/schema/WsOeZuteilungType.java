
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsGbereichIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsOrganisationseinheitIdType;
import com.adcubum.syrius.api.partnermgmt.common.types.v1.WsOrganisationseinheitArtType;


/**
 * Enthält die Daten einer OeZuteilung.
 * 
 * <p>Java class for WsOeZuteilungType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsOeZuteilungType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="stateFrom" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType"/&gt;
 *         &lt;element name="stateUpto" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="gbereichId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsGbereichIdType"/&gt;
 *         &lt;element name="organisationseinheitArt" type="{urn:com:adcubum:syrius:api:partnermgmt:common:types:v1}WsOrganisationseinheitArtType"/&gt;
 *         &lt;element name="organisationseinheitId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsOrganisationseinheitIdType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungDurch" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungAm" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}OffsetDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsOeZuteilungType", propOrder = {
    "stateFrom",
    "stateUpto",
    "gbereichId",
    "organisationseinheitArt",
    "organisationseinheitId",
    "aenderungDurch",
    "aenderungAm"
})
public class WsOeZuteilungType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stateFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stateUpto;
    @XmlElement(required = true)
    protected WsGbereichIdType gbereichId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected WsOrganisationseinheitArtType organisationseinheitArt;
    protected WsOrganisationseinheitIdType organisationseinheitId;
    protected String aenderungDurch;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aenderungAm;

    /**
     * Gets the value of the stateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStateFrom() {
        return stateFrom;
    }

    /**
     * Sets the value of the stateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStateFrom(XMLGregorianCalendar value) {
        this.stateFrom = value;
    }

    /**
     * Gets the value of the stateUpto property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStateUpto() {
        return stateUpto;
    }

    /**
     * Sets the value of the stateUpto property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStateUpto(XMLGregorianCalendar value) {
        this.stateUpto = value;
    }

    /**
     * Gets the value of the gbereichId property.
     * 
     * @return
     *     possible object is
     *     {@link WsGbereichIdType }
     *     
     */
    public WsGbereichIdType getGbereichId() {
        return gbereichId;
    }

    /**
     * Sets the value of the gbereichId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsGbereichIdType }
     *     
     */
    public void setGbereichId(WsGbereichIdType value) {
        this.gbereichId = value;
    }

    /**
     * Gets the value of the organisationseinheitArt property.
     * 
     * @return
     *     possible object is
     *     {@link WsOrganisationseinheitArtType }
     *     
     */
    public WsOrganisationseinheitArtType getOrganisationseinheitArt() {
        return organisationseinheitArt;
    }

    /**
     * Sets the value of the organisationseinheitArt property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsOrganisationseinheitArtType }
     *     
     */
    public void setOrganisationseinheitArt(WsOrganisationseinheitArtType value) {
        this.organisationseinheitArt = value;
    }

    /**
     * Gets the value of the organisationseinheitId property.
     * 
     * @return
     *     possible object is
     *     {@link WsOrganisationseinheitIdType }
     *     
     */
    public WsOrganisationseinheitIdType getOrganisationseinheitId() {
        return organisationseinheitId;
    }

    /**
     * Sets the value of the organisationseinheitId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsOrganisationseinheitIdType }
     *     
     */
    public void setOrganisationseinheitId(WsOrganisationseinheitIdType value) {
        this.organisationseinheitId = value;
    }

    /**
     * Gets the value of the aenderungDurch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAenderungDurch() {
        return aenderungDurch;
    }

    /**
     * Sets the value of the aenderungDurch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAenderungDurch(String value) {
        this.aenderungDurch = value;
    }

    /**
     * Gets the value of the aenderungAm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAenderungAm() {
        return aenderungAm;
    }

    /**
     * Sets the value of the aenderungAm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAenderungAm(XMLGregorianCalendar value) {
        this.aenderungAm = value;
    }

}
