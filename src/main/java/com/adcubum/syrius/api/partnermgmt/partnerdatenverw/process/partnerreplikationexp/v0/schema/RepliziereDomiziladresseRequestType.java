
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Request für das Replizieren einer Domiziladresse.
 * 
 * <p>Java class for RepliziereDomiziladresseRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliziereDomiziladresseRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="adresseReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsAdresseReferenzType"/&gt;
 *         &lt;element name="partnerReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerReferenzType"/&gt;
 *         &lt;element name="messageId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="domiziladresseStruktur" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsDomiziladresseStrukturType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliziereDomiziladresseRequestType", propOrder = {
    "adresseReferenz",
    "partnerReferenz",
    "messageId",
    "domiziladresseStruktur"
})
public class RepliziereDomiziladresseRequestType {

    @XmlElement(required = true)
    protected WsAdresseReferenzType adresseReferenz;
    @XmlElement(required = true)
    protected WsPartnerReferenzType partnerReferenz;
    @XmlElement(required = true)
    protected String messageId;
    @XmlElement(required = true)
    protected WsDomiziladresseStrukturType domiziladresseStruktur;

    /**
     * Gets the value of the adresseReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsAdresseReferenzType }
     *     
     */
    public WsAdresseReferenzType getAdresseReferenz() {
        return adresseReferenz;
    }

    /**
     * Sets the value of the adresseReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAdresseReferenzType }
     *     
     */
    public void setAdresseReferenz(WsAdresseReferenzType value) {
        this.adresseReferenz = value;
    }

    /**
     * Gets the value of the partnerReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public WsPartnerReferenzType getPartnerReferenz() {
        return partnerReferenz;
    }

    /**
     * Sets the value of the partnerReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public void setPartnerReferenz(WsPartnerReferenzType value) {
        this.partnerReferenz = value;
    }

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the domiziladresseStruktur property.
     * 
     * @return
     *     possible object is
     *     {@link WsDomiziladresseStrukturType }
     *     
     */
    public WsDomiziladresseStrukturType getDomiziladresseStruktur() {
        return domiziladresseStruktur;
    }

    /**
     * Sets the value of the domiziladresseStruktur property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsDomiziladresseStrukturType }
     *     
     */
    public void setDomiziladresseStruktur(WsDomiziladresseStrukturType value) {
        this.domiziladresseStruktur = value;
    }

}
