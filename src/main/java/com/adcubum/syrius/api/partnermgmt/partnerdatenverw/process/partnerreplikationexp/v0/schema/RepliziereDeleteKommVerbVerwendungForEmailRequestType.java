
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Request für das Löschen einer Kommunikationsverbindungsverwendung.
 * 
 * <p>Java class for RepliziereDeleteKommVerbVerwendungForEmailRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliziereDeleteKommVerbVerwendungForEmailRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partnerReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerReferenzType"/&gt;
 *         &lt;element name="emailReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsEmailReferenzType"/&gt;
 *         &lt;element name="messageId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="externeVersion" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LongType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliziereDeleteKommVerbVerwendungForEmailRequestType", propOrder = {
    "partnerReferenz",
    "emailReferenz",
    "messageId",
    "externeVersion"
})
public class RepliziereDeleteKommVerbVerwendungForEmailRequestType {

    @XmlElement(required = true)
    protected WsPartnerReferenzType partnerReferenz;
    @XmlElement(required = true)
    protected WsEmailReferenzType emailReferenz;
    @XmlElement(required = true)
    protected String messageId;
    protected Long externeVersion;

    /**
     * Gets the value of the partnerReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public WsPartnerReferenzType getPartnerReferenz() {
        return partnerReferenz;
    }

    /**
     * Sets the value of the partnerReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public void setPartnerReferenz(WsPartnerReferenzType value) {
        this.partnerReferenz = value;
    }

    /**
     * Gets the value of the emailReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsEmailReferenzType }
     *     
     */
    public WsEmailReferenzType getEmailReferenz() {
        return emailReferenz;
    }

    /**
     * Sets the value of the emailReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsEmailReferenzType }
     *     
     */
    public void setEmailReferenz(WsEmailReferenzType value) {
        this.emailReferenz = value;
    }

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the externeVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExterneVersion() {
        return externeVersion;
    }

    /**
     * Sets the value of the externeVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExterneVersion(Long value) {
        this.externeVersion = value;
    }

}
