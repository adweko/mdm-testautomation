
package com.adcubum.syrius.api.partnermgmt.common.codes.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.adcubum.syrius.api.partnermgmt.common.codes.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.adcubum.syrius.api.partnermgmt.common.codes.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WsAktivCodeType }
     * 
     */
    public WsAktivCodeType createWsAktivCodeType() {
        return new WsAktivCodeType();
    }

    /**
     * Create an instance of {@link WsArbeitslosCodeType }
     * 
     */
    public WsArbeitslosCodeType createWsArbeitslosCodeType() {
        return new WsArbeitslosCodeType();
    }

    /**
     * Create an instance of {@link WsAufenthaltsbewCodeType }
     * 
     */
    public WsAufenthaltsbewCodeType createWsAufenthaltsbewCodeType() {
        return new WsAufenthaltsbewCodeType();
    }

    /**
     * Create an instance of {@link WsBerufsgruppeCodeType }
     * 
     */
    public WsBerufsgruppeCodeType createWsBerufsgruppeCodeType() {
        return new WsBerufsgruppeCodeType();
    }

    /**
     * Create an instance of {@link WsBrancheCodeType }
     * 
     */
    public WsBrancheCodeType createWsBrancheCodeType() {
        return new WsBrancheCodeType();
    }

    /**
     * Create an instance of {@link WsEinzugsermaechtigungCodeType }
     * 
     */
    public WsEinzugsermaechtigungCodeType createWsEinzugsermaechtigungCodeType() {
        return new WsEinzugsermaechtigungCodeType();
    }

    /**
     * Create an instance of {@link WsEntschaedigungCodeType }
     * 
     */
    public WsEntschaedigungCodeType createWsEntschaedigungCodeType() {
        return new WsEntschaedigungCodeType();
    }

    /**
     * Create an instance of {@link WsFaktStatusCodeType }
     * 
     */
    public WsFaktStatusCodeType createWsFaktStatusCodeType() {
        return new WsFaktStatusCodeType();
    }

    /**
     * Create an instance of {@link WsGeschlechtCodeType }
     * 
     */
    public WsGeschlechtCodeType createWsGeschlechtCodeType() {
        return new WsGeschlechtCodeType();
    }

    /**
     * Create an instance of {@link WsInAusbildungCodeType }
     * 
     */
    public WsInAusbildungCodeType createWsInAusbildungCodeType() {
        return new WsInAusbildungCodeType();
    }

    /**
     * Create an instance of {@link WsKontaktAnredeCodeType }
     * 
     */
    public WsKontaktAnredeCodeType createWsKontaktAnredeCodeType() {
        return new WsKontaktAnredeCodeType();
    }

    /**
     * Create an instance of {@link WsKontaktschutzCodeType }
     * 
     */
    public WsKontaktschutzCodeType createWsKontaktschutzCodeType() {
        return new WsKontaktschutzCodeType();
    }

    /**
     * Create an instance of {@link WsKonzernpartnerCodeType }
     * 
     */
    public WsKonzernpartnerCodeType createWsKonzernpartnerCodeType() {
        return new WsKonzernpartnerCodeType();
    }

    /**
     * Create an instance of {@link WsLeistungspflichtCodeType }
     * 
     */
    public WsLeistungspflichtCodeType createWsLeistungspflichtCodeType() {
        return new WsLeistungspflichtCodeType();
    }

    /**
     * Create an instance of {@link WsLerbAnredeCodeType }
     * 
     */
    public WsLerbAnredeCodeType createWsLerbAnredeCodeType() {
        return new WsLerbAnredeCodeType();
    }

    /**
     * Create an instance of {@link WsLstAbrMoeglichCodeType }
     * 
     */
    public WsLstAbrMoeglichCodeType createWsLstAbrMoeglichCodeType() {
        return new WsLstAbrMoeglichCodeType();
    }

    /**
     * Create an instance of {@link WsOrganisationTypCodeType }
     * 
     */
    public WsOrganisationTypCodeType createWsOrganisationTypCodeType() {
        return new WsOrganisationTypCodeType();
    }

    /**
     * Create an instance of {@link WsRechtsformCodeType }
     * 
     */
    public WsRechtsformCodeType createWsRechtsformCodeType() {
        return new WsRechtsformCodeType();
    }

    /**
     * Create an instance of {@link WsSozialhilfeCodeType }
     * 
     */
    public WsSozialhilfeCodeType createWsSozialhilfeCodeType() {
        return new WsSozialhilfeCodeType();
    }

    /**
     * Create an instance of {@link WsStilleKoGuCodeType }
     * 
     */
    public WsStilleKoGuCodeType createWsStilleKoGuCodeType() {
        return new WsStilleKoGuCodeType();
    }

    /**
     * Create an instance of {@link WsStrasseNrReihenfolgeCodeType }
     * 
     */
    public WsStrasseNrReihenfolgeCodeType createWsStrasseNrReihenfolgeCodeType() {
        return new WsStrasseNrReihenfolgeCodeType();
    }

    /**
     * Create an instance of {@link WsPostfachOhneNrCodeType }
     * 
     */
    public WsPostfachOhneNrCodeType createWsPostfachOhneNrCodeType() {
        return new WsPostfachOhneNrCodeType();
    }

    /**
     * Create an instance of {@link WsSystemCodeType }
     * 
     */
    public WsSystemCodeType createWsSystemCodeType() {
        return new WsSystemCodeType();
    }

    /**
     * Create an instance of {@link WsTitelCodeType }
     * 
     */
    public WsTitelCodeType createWsTitelCodeType() {
        return new WsTitelCodeType();
    }

    /**
     * Create an instance of {@link WsUnterstPflichtCodeType }
     * 
     */
    public WsUnterstPflichtCodeType createWsUnterstPflichtCodeType() {
        return new WsUnterstPflichtCodeType();
    }

    /**
     * Create an instance of {@link WsVerbandCodeType }
     * 
     */
    public WsVerbandCodeType createWsVerbandCodeType() {
        return new WsVerbandCodeType();
    }

    /**
     * Create an instance of {@link WsVipCodeType }
     * 
     */
    public WsVipCodeType createWsVipCodeType() {
        return new WsVipCodeType();
    }

    /**
     * Create an instance of {@link WsZivilstandCodeType }
     * 
     */
    public WsZivilstandCodeType createWsZivilstandCodeType() {
        return new WsZivilstandCodeType();
    }

    /**
     * Create an instance of {@link WsKantonCodeType }
     * 
     */
    public WsKantonCodeType createWsKantonCodeType() {
        return new WsKantonCodeType();
    }

    /**
     * Create an instance of {@link WsGemeindeKantonCodeType }
     * 
     */
    public WsGemeindeKantonCodeType createWsGemeindeKantonCodeType() {
        return new WsGemeindeKantonCodeType();
    }

    /**
     * Create an instance of {@link WsMehrfachverwendungCodeType }
     * 
     */
    public WsMehrfachverwendungCodeType createWsMehrfachverwendungCodeType() {
        return new WsMehrfachverwendungCodeType();
    }

    /**
     * Create an instance of {@link WsRegionCodeType }
     * 
     */
    public WsRegionCodeType createWsRegionCodeType() {
        return new WsRegionCodeType();
    }

    /**
     * Create an instance of {@link WsPflegestufeCodeType }
     * 
     */
    public WsPflegestufeCodeType createWsPflegestufeCodeType() {
        return new WsPflegestufeCodeType();
    }

    /**
     * Create an instance of {@link WsPatientenstatusCodeType }
     * 
     */
    public WsPatientenstatusCodeType createWsPatientenstatusCodeType() {
        return new WsPatientenstatusCodeType();
    }

    /**
     * Create an instance of {@link WsAdrAendStatusCodeType }
     * 
     */
    public WsAdrAendStatusCodeType createWsAdrAendStatusCodeType() {
        return new WsAdrAendStatusCodeType();
    }

    /**
     * Create an instance of {@link WsAdrAendBegruendungCodeType }
     * 
     */
    public WsAdrAendBegruendungCodeType createWsAdrAendBegruendungCodeType() {
        return new WsAdrAendBegruendungCodeType();
    }

    /**
     * Create an instance of {@link WsPartAendStatusCodeType }
     * 
     */
    public WsPartAendStatusCodeType createWsPartAendStatusCodeType() {
        return new WsPartAendStatusCodeType();
    }

    /**
     * Create an instance of {@link WsPartAendBegruendungCodeType }
     * 
     */
    public WsPartAendBegruendungCodeType createWsPartAendBegruendungCodeType() {
        return new WsPartAendBegruendungCodeType();
    }

    /**
     * Create an instance of {@link WsMandatArtCodeType }
     * 
     */
    public WsMandatArtCodeType createWsMandatArtCodeType() {
        return new WsMandatArtCodeType();
    }

    /**
     * Create an instance of {@link WsErlaubnisCodeType }
     * 
     */
    public WsErlaubnisCodeType createWsErlaubnisCodeType() {
        return new WsErlaubnisCodeType();
    }

    /**
     * Create an instance of {@link WsErlaubnisartCodeType }
     * 
     */
    public WsErlaubnisartCodeType createWsErlaubnisartCodeType() {
        return new WsErlaubnisartCodeType();
    }

    /**
     * Create an instance of {@link WsKommVerbTypCodeType }
     * 
     */
    public WsKommVerbTypCodeType createWsKommVerbTypCodeType() {
        return new WsKommVerbTypCodeType();
    }

    /**
     * Create an instance of {@link WsDublBeziehungCodeType }
     * 
     */
    public WsDublBeziehungCodeType createWsDublBeziehungCodeType() {
        return new WsDublBeziehungCodeType();
    }

    /**
     * Create an instance of {@link WsPartnerTypCodeType }
     * 
     */
    public WsPartnerTypCodeType createWsPartnerTypCodeType() {
        return new WsPartnerTypCodeType();
    }

    /**
     * Create an instance of {@link WsNutzungszweckCodeType }
     * 
     */
    public WsNutzungszweckCodeType createWsNutzungszweckCodeType() {
        return new WsNutzungszweckCodeType();
    }

}
