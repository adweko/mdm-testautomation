
package com.adcubum.syrius.api.partnermgmt.common.types.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsAttributTypType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WsAttributTypType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="text"/&gt;
 *     &lt;enumeration value="datum"/&gt;
 *     &lt;enumeration value="code"/&gt;
 *     &lt;enumeration value="boolean"/&gt;
 *     &lt;enumeration value="ganzzahl"/&gt;
 *     &lt;enumeration value="decimalzahl"/&gt;
 *     &lt;enumeration value="referenz"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "WsAttributTypType")
@XmlEnum
public enum WsAttributTypType {

    @XmlEnumValue("text")
    TEXT("text"),
    @XmlEnumValue("datum")
    DATUM("datum"),
    @XmlEnumValue("code")
    CODE("code"),
    @XmlEnumValue("boolean")
    BOOLEAN("boolean"),
    @XmlEnumValue("ganzzahl")
    GANZZAHL("ganzzahl"),
    @XmlEnumValue("decimalzahl")
    DECIMALZAHL("decimalzahl"),
    @XmlEnumValue("referenz")
    REFERENZ("referenz");
    private final String value;

    WsAttributTypType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WsAttributTypType fromValue(String v) {
        for (WsAttributTypType c: WsAttributTypType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
