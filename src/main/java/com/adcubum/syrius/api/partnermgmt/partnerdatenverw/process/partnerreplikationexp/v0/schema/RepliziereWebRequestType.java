
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Request für das Replizieren einer Web-Adresse.
 * 
 * <p>Java class for RepliziereWebRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliziereWebRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="webReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsWebReferenzType"/&gt;
 *         &lt;element name="partnerReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerReferenzType"/&gt;
 *         &lt;element name="messageId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="webStruktur" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsWebStrukturType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliziereWebRequestType", propOrder = {
    "webReferenz",
    "partnerReferenz",
    "messageId",
    "webStruktur"
})
public class RepliziereWebRequestType {

    @XmlElement(required = true)
    protected WsWebReferenzType webReferenz;
    @XmlElement(required = true)
    protected WsPartnerReferenzType partnerReferenz;
    @XmlElement(required = true)
    protected String messageId;
    @XmlElement(required = true)
    protected WsWebStrukturType webStruktur;

    /**
     * Gets the value of the webReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsWebReferenzType }
     *     
     */
    public WsWebReferenzType getWebReferenz() {
        return webReferenz;
    }

    /**
     * Sets the value of the webReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsWebReferenzType }
     *     
     */
    public void setWebReferenz(WsWebReferenzType value) {
        this.webReferenz = value;
    }

    /**
     * Gets the value of the partnerReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public WsPartnerReferenzType getPartnerReferenz() {
        return partnerReferenz;
    }

    /**
     * Sets the value of the partnerReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public void setPartnerReferenz(WsPartnerReferenzType value) {
        this.partnerReferenz = value;
    }

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the webStruktur property.
     * 
     * @return
     *     possible object is
     *     {@link WsWebStrukturType }
     *     
     */
    public WsWebStrukturType getWebStruktur() {
        return webStruktur;
    }

    /**
     * Sets the value of the webStruktur property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsWebStrukturType }
     *     
     */
    public void setWebStruktur(WsWebStrukturType value) {
        this.webStruktur = value;
    }

}
