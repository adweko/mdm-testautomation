
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.schema;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.schema package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPartnerrolleRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema", "getPartnerrolleRequest");
    private final static QName _GetPartnerrolleResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema", "getPartnerrolleResponse");
    private final static QName _GetPartnerPartnerrolleRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema", "getPartnerPartnerrolleRequest");
    private final static QName _GetPartnerPartnerrolleResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema", "getPartnerPartnerrolleResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.schema
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPartnerrolleRequestType }
     * 
     */
    public GetPartnerrolleRequestType createGetPartnerrolleRequestType() {
        return new GetPartnerrolleRequestType();
    }

    /**
     * Create an instance of {@link GetPartnerrolleResponseType }
     * 
     */
    public GetPartnerrolleResponseType createGetPartnerrolleResponseType() {
        return new GetPartnerrolleResponseType();
    }

    /**
     * Create an instance of {@link GetPartnerPartnerrolleRequestType }
     * 
     */
    public GetPartnerPartnerrolleRequestType createGetPartnerPartnerrolleRequestType() {
        return new GetPartnerPartnerrolleRequestType();
    }

    /**
     * Create an instance of {@link GetPartnerPartnerrolleResponseType }
     * 
     */
    public GetPartnerPartnerrolleResponseType createGetPartnerPartnerrolleResponseType() {
        return new GetPartnerPartnerrolleResponseType();
    }

    /**
     * Create an instance of {@link WsPartnerPartnerrolleType }
     * 
     */
    public WsPartnerPartnerrolleType createWsPartnerPartnerrolleType() {
        return new WsPartnerPartnerrolleType();
    }

    /**
     * Create an instance of {@link WsPartnerrolleType }
     * 
     */
    public WsPartnerrolleType createWsPartnerrolleType() {
        return new WsPartnerrolleType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartnerrolleRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetPartnerrolleRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema", name = "getPartnerrolleRequest")
    public JAXBElement<GetPartnerrolleRequestType> createGetPartnerrolleRequest(GetPartnerrolleRequestType value) {
        return new JAXBElement<GetPartnerrolleRequestType>(_GetPartnerrolleRequest_QNAME, GetPartnerrolleRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartnerrolleResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetPartnerrolleResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema", name = "getPartnerrolleResponse")
    public JAXBElement<GetPartnerrolleResponseType> createGetPartnerrolleResponse(GetPartnerrolleResponseType value) {
        return new JAXBElement<GetPartnerrolleResponseType>(_GetPartnerrolleResponse_QNAME, GetPartnerrolleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartnerPartnerrolleRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetPartnerPartnerrolleRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema", name = "getPartnerPartnerrolleRequest")
    public JAXBElement<GetPartnerPartnerrolleRequestType> createGetPartnerPartnerrolleRequest(GetPartnerPartnerrolleRequestType value) {
        return new JAXBElement<GetPartnerPartnerrolleRequestType>(_GetPartnerPartnerrolleRequest_QNAME, GetPartnerPartnerrolleRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartnerPartnerrolleResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetPartnerPartnerrolleResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema", name = "getPartnerPartnerrolleResponse")
    public JAXBElement<GetPartnerPartnerrolleResponseType> createGetPartnerPartnerrolleResponse(GetPartnerPartnerrolleResponseType value) {
        return new JAXBElement<GetPartnerPartnerrolleResponseType>(_GetPartnerPartnerrolleResponse_QNAME, GetPartnerPartnerrolleResponseType.class, null, value);
    }

}
