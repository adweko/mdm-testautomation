
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsWebIdType;


/**
 * Referenz auf eine Web-Adresse mit einer techn. und/oder einer externen Id.
 * 
 * <p>Java class for WsWebReferenzType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsWebReferenzType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="externeReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsExterneReferenzType" minOccurs="0"/&gt;
 *         &lt;element name="webId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsWebIdType" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsWebReferenzType", propOrder = {

})
public class WsWebReferenzType {

    protected WsExterneReferenzType externeReferenz;
    protected WsWebIdType webId;

    /**
     * Gets the value of the externeReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public WsExterneReferenzType getExterneReferenz() {
        return externeReferenz;
    }

    /**
     * Sets the value of the externeReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public void setExterneReferenz(WsExterneReferenzType value) {
        this.externeReferenz = value;
    }

    /**
     * Gets the value of the webId property.
     * 
     * @return
     *     possible object is
     *     {@link WsWebIdType }
     *     
     */
    public WsWebIdType getWebId() {
        return webId;
    }

    /**
     * Sets the value of the webId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsWebIdType }
     *     
     */
    public void setWebId(WsWebIdType value) {
        this.webId = value;
    }

}
