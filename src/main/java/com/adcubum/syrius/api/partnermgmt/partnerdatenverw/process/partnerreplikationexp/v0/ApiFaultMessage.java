
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.3.6
 * 2020-08-07T10:41:17.636+02:00
 * Generated source version: 3.3.6
 */

@WebFault(name = "apiFaultInfo", targetNamespace = "urn:com:adcubum:syrius:api:common:faults:v1")
public class ApiFaultMessage extends Exception {

    private com.adcubum.syrius.api.common.faults.v1.ApiFaultInfoType apiFaultInfo;

    public ApiFaultMessage() {
        super();
    }

    public ApiFaultMessage(String message) {
        super(message);
    }

    public ApiFaultMessage(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public ApiFaultMessage(String message, com.adcubum.syrius.api.common.faults.v1.ApiFaultInfoType apiFaultInfo) {
        super(message);
        this.apiFaultInfo = apiFaultInfo;
    }

    public ApiFaultMessage(String message, com.adcubum.syrius.api.common.faults.v1.ApiFaultInfoType apiFaultInfo, java.lang.Throwable cause) {
        super(message, cause);
        this.apiFaultInfo = apiFaultInfo;
    }

    public com.adcubum.syrius.api.common.faults.v1.ApiFaultInfoType getFaultInfo() {
        return this.apiFaultInfo;
    }
}
