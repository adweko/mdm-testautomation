
package com.adcubum.syrius.api.partnermgmt.common.identifier.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.common.identifier.v1.WsIdType;


/**
 * Technischer Schlüssel einer Adressrollen-Definition.
 * 
 * <p>Java class for WsAdressrolleDefIdType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAdressrolleDefIdType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:identifier:v1}WsIdType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAdressrolleDefIdType")
public class WsAdressrolleDefIdType
    extends WsIdType
{


}
