
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Request für das Replizieren einer Zusatzadresse.
 * 
 * <p>Java class for RepliziereZusatzadresseRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliziereZusatzadresseRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="adresseReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsAdresseReferenzType"/&gt;
 *         &lt;element name="partnerReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerReferenzType"/&gt;
 *         &lt;element name="messageId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="zusatzadresseStruktur" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsZusatzadresseStrukturType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliziereZusatzadresseRequestType", propOrder = {
    "adresseReferenz",
    "partnerReferenz",
    "messageId",
    "zusatzadresseStruktur"
})
public class RepliziereZusatzadresseRequestType {

    @XmlElement(required = true)
    protected WsAdresseReferenzType adresseReferenz;
    @XmlElement(required = true)
    protected WsPartnerReferenzType partnerReferenz;
    @XmlElement(required = true)
    protected String messageId;
    @XmlElement(required = true)
    protected WsZusatzadresseStrukturType zusatzadresseStruktur;

    /**
     * Gets the value of the adresseReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsAdresseReferenzType }
     *     
     */
    public WsAdresseReferenzType getAdresseReferenz() {
        return adresseReferenz;
    }

    /**
     * Sets the value of the adresseReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAdresseReferenzType }
     *     
     */
    public void setAdresseReferenz(WsAdresseReferenzType value) {
        this.adresseReferenz = value;
    }

    /**
     * Gets the value of the partnerReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public WsPartnerReferenzType getPartnerReferenz() {
        return partnerReferenz;
    }

    /**
     * Sets the value of the partnerReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public void setPartnerReferenz(WsPartnerReferenzType value) {
        this.partnerReferenz = value;
    }

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the zusatzadresseStruktur property.
     * 
     * @return
     *     possible object is
     *     {@link WsZusatzadresseStrukturType }
     *     
     */
    public WsZusatzadresseStrukturType getZusatzadresseStruktur() {
        return zusatzadresseStruktur;
    }

    /**
     * Sets the value of the zusatzadresseStruktur property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsZusatzadresseStrukturType }
     *     
     */
    public void setZusatzadresseStruktur(WsZusatzadresseStrukturType value) {
        this.zusatzadresseStruktur = value;
    }

}
