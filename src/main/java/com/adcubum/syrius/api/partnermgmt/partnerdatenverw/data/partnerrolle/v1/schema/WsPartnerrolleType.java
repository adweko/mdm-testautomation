
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.common.types.v1.WsHistObjectType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsMutationsgrundIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerrolleDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerrolleIdType;


/**
 * Enthält die Daten einer Partnerrolle.
 * 
 * <p>Java class for WsPartnerrolleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPartnerrolleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:types:v1}WsHistObjectType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsPartnerrolleIdType"/&gt;
 *         &lt;element name="partnerrolleDefId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsPartnerrolleDefIdType"/&gt;
 *         &lt;element name="mutationsgrundId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsMutationsgrundIdType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPartnerrolleType", propOrder = {
    "id",
    "partnerrolleDefId",
    "mutationsgrundId"
})
public class WsPartnerrolleType
    extends WsHistObjectType
{

    @XmlElement(required = true)
    protected WsPartnerrolleIdType id;
    @XmlElement(required = true)
    protected WsPartnerrolleDefIdType partnerrolleDefId;
    protected WsMutationsgrundIdType mutationsgrundId;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerrolleIdType }
     *     
     */
    public WsPartnerrolleIdType getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerrolleIdType }
     *     
     */
    public void setId(WsPartnerrolleIdType value) {
        this.id = value;
    }

    /**
     * Gets the value of the partnerrolleDefId property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerrolleDefIdType }
     *     
     */
    public WsPartnerrolleDefIdType getPartnerrolleDefId() {
        return partnerrolleDefId;
    }

    /**
     * Sets the value of the partnerrolleDefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerrolleDefIdType }
     *     
     */
    public void setPartnerrolleDefId(WsPartnerrolleDefIdType value) {
        this.partnerrolleDefId = value;
    }

    /**
     * Gets the value of the mutationsgrundId property.
     * 
     * @return
     *     possible object is
     *     {@link WsMutationsgrundIdType }
     *     
     */
    public WsMutationsgrundIdType getMutationsgrundId() {
        return mutationsgrundId;
    }

    /**
     * Sets the value of the mutationsgrundId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsMutationsgrundIdType }
     *     
     */
    public void setMutationsgrundId(WsMutationsgrundIdType value) {
        this.mutationsgrundId = value;
    }

}
