
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Response, in dem die geladenen Partnerrollen der angegebenen Partner enthalten sind.
 * 
 * <p>Java class for GetPartnerPartnerrolleResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPartnerPartnerrolleResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partnerPartnerrolle" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema}WsPartnerPartnerrolleType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPartnerPartnerrolleResponseType", propOrder = {
    "partnerPartnerrolle"
})
public class GetPartnerPartnerrolleResponseType {

    @XmlElement(required = true)
    protected List<WsPartnerPartnerrolleType> partnerPartnerrolle;

    /**
     * Gets the value of the partnerPartnerrolle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partnerPartnerrolle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartnerPartnerrolle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsPartnerPartnerrolleType }
     * 
     * 
     */
    public List<WsPartnerPartnerrolleType> getPartnerPartnerrolle() {
        if (partnerPartnerrolle == null) {
            partnerPartnerrolle = new ArrayList<WsPartnerPartnerrolleType>();
        }
        return this.partnerPartnerrolle;
    }

}
