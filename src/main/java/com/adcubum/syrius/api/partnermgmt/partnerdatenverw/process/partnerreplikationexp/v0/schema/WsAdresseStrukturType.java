
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Alle Fachlichen States einer Adresse inkl. Adressrollen und Verwendungen.
 * 
 * <p>Java class for WsAdresseStrukturType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAdresseStrukturType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="gueltAb" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="gueltBis" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="externeVersion" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LongType" minOccurs="0"/&gt;
 *         &lt;element name="adresseStates" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsAdresseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="adressrollen" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsAdressrolleStrukturType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="kommVerbVerwendung" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsKommVerbVerwendungType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAdresseStrukturType", propOrder = {
    "gueltAb",
    "gueltBis",
    "externeVersion",
    "adresseStates",
    "adressrollen",
    "kommVerbVerwendung"
})
@XmlSeeAlso({
    WsDomiziladresseStrukturType.class,
    WsZusatzadresseStrukturType.class
})
public abstract class WsAdresseStrukturType {

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltAb;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltBis;
    protected Long externeVersion;
    protected List<WsAdresseType> adresseStates;
    protected List<WsAdressrolleStrukturType> adressrollen;
    protected WsKommVerbVerwendungType kommVerbVerwendung;

    /**
     * Gets the value of the gueltAb property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltAb() {
        return gueltAb;
    }

    /**
     * Sets the value of the gueltAb property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltAb(XMLGregorianCalendar value) {
        this.gueltAb = value;
    }

    /**
     * Gets the value of the gueltBis property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltBis() {
        return gueltBis;
    }

    /**
     * Sets the value of the gueltBis property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltBis(XMLGregorianCalendar value) {
        this.gueltBis = value;
    }

    /**
     * Gets the value of the externeVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExterneVersion() {
        return externeVersion;
    }

    /**
     * Sets the value of the externeVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExterneVersion(Long value) {
        this.externeVersion = value;
    }

    /**
     * Gets the value of the adresseStates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adresseStates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdresseStates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAdresseType }
     * 
     * 
     */
    public List<WsAdresseType> getAdresseStates() {
        if (adresseStates == null) {
            adresseStates = new ArrayList<WsAdresseType>();
        }
        return this.adresseStates;
    }

    /**
     * Gets the value of the adressrollen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adressrollen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdressrollen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsAdressrolleStrukturType }
     * 
     * 
     */
    public List<WsAdressrolleStrukturType> getAdressrollen() {
        if (adressrollen == null) {
            adressrollen = new ArrayList<WsAdressrolleStrukturType>();
        }
        return this.adressrollen;
    }

    /**
     * Gets the value of the kommVerbVerwendung property.
     * 
     * @return
     *     possible object is
     *     {@link WsKommVerbVerwendungType }
     *     
     */
    public WsKommVerbVerwendungType getKommVerbVerwendung() {
        return kommVerbVerwendung;
    }

    /**
     * Sets the value of the kommVerbVerwendung property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsKommVerbVerwendungType }
     *     
     */
    public void setKommVerbVerwendung(WsKommVerbVerwendungType value) {
        this.kommVerbVerwendung = value;
    }

}
