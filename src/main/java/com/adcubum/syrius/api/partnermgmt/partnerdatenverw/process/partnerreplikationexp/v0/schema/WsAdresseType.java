
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsPostfachOhneNrCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsStrasseNrReihenfolgeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsMutationsgrundIdType;


/**
 * Abstrakte Adresse für die gemeinsamen Eigenschaften von Domizil- oder Zusatzadressen.
 * 
 * <p>Java class for WsAdresseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsAdresseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="stateFrom" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType"/&gt;
 *         &lt;element name="stateUpto" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="bemerkungen" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="hausId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="hausNrZusatz" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="hausnummer" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="land" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="ort" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="plz" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="plzId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="plzZusatz" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="postfach" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="postfachOhneNr" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsPostfachOhneNrCodeType" minOccurs="0"/&gt;
 *         &lt;element name="zusatz1VorStr" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="zusatz2VorStr" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="zusatz3VorStr" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="zusatz4VorStr" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="strasse" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="strasseId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="zusatz1NachStr" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="zusatz2NachStr" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="gemeindenummer" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}IntegerType" minOccurs="0"/&gt;
 *         &lt;element name="gemeindename" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="provinz" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="gemeindeProvinz" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="strasseNrReihenfolge" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsStrasseNrReihenfolgeCodeType" minOccurs="0"/&gt;
 *         &lt;element name="mutationsgrundId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsMutationsgrundIdType"/&gt;
 *         &lt;element name="abhaengigVon" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsAdresseReferenzType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungDurch" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungAm" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}OffsetDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsAdresseType", propOrder = {
    "stateFrom",
    "stateUpto",
    "bemerkungen",
    "hausId",
    "hausNrZusatz",
    "hausnummer",
    "land",
    "ort",
    "plz",
    "plzId",
    "plzZusatz",
    "postfach",
    "postfachOhneNr",
    "zusatz1VorStr",
    "zusatz2VorStr",
    "zusatz3VorStr",
    "zusatz4VorStr",
    "strasse",
    "strasseId",
    "zusatz1NachStr",
    "zusatz2NachStr",
    "gemeindenummer",
    "gemeindename",
    "provinz",
    "gemeindeProvinz",
    "strasseNrReihenfolge",
    "mutationsgrundId",
    "abhaengigVon",
    "aenderungDurch",
    "aenderungAm"
})
public class WsAdresseType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stateFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stateUpto;
    protected String bemerkungen;
    protected String hausId;
    protected String hausNrZusatz;
    protected String hausnummer;
    @XmlElement(required = true)
    protected String land;
    @XmlElement(required = true)
    protected String ort;
    protected String plz;
    protected String plzId;
    protected String plzZusatz;
    protected String postfach;
    protected WsPostfachOhneNrCodeType postfachOhneNr;
    protected String zusatz1VorStr;
    protected String zusatz2VorStr;
    protected String zusatz3VorStr;
    protected String zusatz4VorStr;
    protected String strasse;
    protected String strasseId;
    protected String zusatz1NachStr;
    protected String zusatz2NachStr;
    protected Integer gemeindenummer;
    protected String gemeindename;
    protected String provinz;
    protected String gemeindeProvinz;
    protected WsStrasseNrReihenfolgeCodeType strasseNrReihenfolge;
    @XmlElement(required = true)
    protected WsMutationsgrundIdType mutationsgrundId;
    protected WsAdresseReferenzType abhaengigVon;
    protected String aenderungDurch;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aenderungAm;

    /**
     * Gets the value of the stateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStateFrom() {
        return stateFrom;
    }

    /**
     * Sets the value of the stateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStateFrom(XMLGregorianCalendar value) {
        this.stateFrom = value;
    }

    /**
     * Gets the value of the stateUpto property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStateUpto() {
        return stateUpto;
    }

    /**
     * Sets the value of the stateUpto property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStateUpto(XMLGregorianCalendar value) {
        this.stateUpto = value;
    }

    /**
     * Gets the value of the bemerkungen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBemerkungen() {
        return bemerkungen;
    }

    /**
     * Sets the value of the bemerkungen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBemerkungen(String value) {
        this.bemerkungen = value;
    }

    /**
     * Gets the value of the hausId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHausId() {
        return hausId;
    }

    /**
     * Sets the value of the hausId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHausId(String value) {
        this.hausId = value;
    }

    /**
     * Gets the value of the hausNrZusatz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHausNrZusatz() {
        return hausNrZusatz;
    }

    /**
     * Sets the value of the hausNrZusatz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHausNrZusatz(String value) {
        this.hausNrZusatz = value;
    }

    /**
     * Gets the value of the hausnummer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHausnummer() {
        return hausnummer;
    }

    /**
     * Sets the value of the hausnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHausnummer(String value) {
        this.hausnummer = value;
    }

    /**
     * Gets the value of the land property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLand() {
        return land;
    }

    /**
     * Sets the value of the land property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLand(String value) {
        this.land = value;
    }

    /**
     * Gets the value of the ort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrt() {
        return ort;
    }

    /**
     * Sets the value of the ort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrt(String value) {
        this.ort = value;
    }

    /**
     * Gets the value of the plz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlz() {
        return plz;
    }

    /**
     * Sets the value of the plz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlz(String value) {
        this.plz = value;
    }

    /**
     * Gets the value of the plzId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlzId() {
        return plzId;
    }

    /**
     * Sets the value of the plzId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlzId(String value) {
        this.plzId = value;
    }

    /**
     * Gets the value of the plzZusatz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlzZusatz() {
        return plzZusatz;
    }

    /**
     * Sets the value of the plzZusatz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlzZusatz(String value) {
        this.plzZusatz = value;
    }

    /**
     * Gets the value of the postfach property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostfach() {
        return postfach;
    }

    /**
     * Sets the value of the postfach property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostfach(String value) {
        this.postfach = value;
    }

    /**
     * Gets the value of the postfachOhneNr property.
     * 
     * @return
     *     possible object is
     *     {@link WsPostfachOhneNrCodeType }
     *     
     */
    public WsPostfachOhneNrCodeType getPostfachOhneNr() {
        return postfachOhneNr;
    }

    /**
     * Sets the value of the postfachOhneNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPostfachOhneNrCodeType }
     *     
     */
    public void setPostfachOhneNr(WsPostfachOhneNrCodeType value) {
        this.postfachOhneNr = value;
    }

    /**
     * Gets the value of the zusatz1VorStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz1VorStr() {
        return zusatz1VorStr;
    }

    /**
     * Sets the value of the zusatz1VorStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz1VorStr(String value) {
        this.zusatz1VorStr = value;
    }

    /**
     * Gets the value of the zusatz2VorStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz2VorStr() {
        return zusatz2VorStr;
    }

    /**
     * Sets the value of the zusatz2VorStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz2VorStr(String value) {
        this.zusatz2VorStr = value;
    }

    /**
     * Gets the value of the zusatz3VorStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz3VorStr() {
        return zusatz3VorStr;
    }

    /**
     * Sets the value of the zusatz3VorStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz3VorStr(String value) {
        this.zusatz3VorStr = value;
    }

    /**
     * Gets the value of the zusatz4VorStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz4VorStr() {
        return zusatz4VorStr;
    }

    /**
     * Sets the value of the zusatz4VorStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz4VorStr(String value) {
        this.zusatz4VorStr = value;
    }

    /**
     * Gets the value of the strasse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * Sets the value of the strasse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrasse(String value) {
        this.strasse = value;
    }

    /**
     * Gets the value of the strasseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrasseId() {
        return strasseId;
    }

    /**
     * Sets the value of the strasseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrasseId(String value) {
        this.strasseId = value;
    }

    /**
     * Gets the value of the zusatz1NachStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz1NachStr() {
        return zusatz1NachStr;
    }

    /**
     * Sets the value of the zusatz1NachStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz1NachStr(String value) {
        this.zusatz1NachStr = value;
    }

    /**
     * Gets the value of the zusatz2NachStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz2NachStr() {
        return zusatz2NachStr;
    }

    /**
     * Sets the value of the zusatz2NachStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz2NachStr(String value) {
        this.zusatz2NachStr = value;
    }

    /**
     * Gets the value of the gemeindenummer property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGemeindenummer() {
        return gemeindenummer;
    }

    /**
     * Sets the value of the gemeindenummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGemeindenummer(Integer value) {
        this.gemeindenummer = value;
    }

    /**
     * Gets the value of the gemeindename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGemeindename() {
        return gemeindename;
    }

    /**
     * Sets the value of the gemeindename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGemeindename(String value) {
        this.gemeindename = value;
    }

    /**
     * Gets the value of the provinz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvinz() {
        return provinz;
    }

    /**
     * Sets the value of the provinz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvinz(String value) {
        this.provinz = value;
    }

    /**
     * Gets the value of the gemeindeProvinz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGemeindeProvinz() {
        return gemeindeProvinz;
    }

    /**
     * Sets the value of the gemeindeProvinz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGemeindeProvinz(String value) {
        this.gemeindeProvinz = value;
    }

    /**
     * Gets the value of the strasseNrReihenfolge property.
     * 
     * @return
     *     possible object is
     *     {@link WsStrasseNrReihenfolgeCodeType }
     *     
     */
    public WsStrasseNrReihenfolgeCodeType getStrasseNrReihenfolge() {
        return strasseNrReihenfolge;
    }

    /**
     * Sets the value of the strasseNrReihenfolge property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsStrasseNrReihenfolgeCodeType }
     *     
     */
    public void setStrasseNrReihenfolge(WsStrasseNrReihenfolgeCodeType value) {
        this.strasseNrReihenfolge = value;
    }

    /**
     * Gets the value of the mutationsgrundId property.
     * 
     * @return
     *     possible object is
     *     {@link WsMutationsgrundIdType }
     *     
     */
    public WsMutationsgrundIdType getMutationsgrundId() {
        return mutationsgrundId;
    }

    /**
     * Sets the value of the mutationsgrundId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsMutationsgrundIdType }
     *     
     */
    public void setMutationsgrundId(WsMutationsgrundIdType value) {
        this.mutationsgrundId = value;
    }

    /**
     * Gets the value of the abhaengigVon property.
     * 
     * @return
     *     possible object is
     *     {@link WsAdresseReferenzType }
     *     
     */
    public WsAdresseReferenzType getAbhaengigVon() {
        return abhaengigVon;
    }

    /**
     * Sets the value of the abhaengigVon property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAdresseReferenzType }
     *     
     */
    public void setAbhaengigVon(WsAdresseReferenzType value) {
        this.abhaengigVon = value;
    }

    /**
     * Gets the value of the aenderungDurch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAenderungDurch() {
        return aenderungDurch;
    }

    /**
     * Sets the value of the aenderungDurch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAenderungDurch(String value) {
        this.aenderungDurch = value;
    }

    /**
     * Gets the value of the aenderungAm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAenderungAm() {
        return aenderungAm;
    }

    /**
     * Sets the value of the aenderungAm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAenderungAm(XMLGregorianCalendar value) {
        this.aenderungAm = value;
    }

}
