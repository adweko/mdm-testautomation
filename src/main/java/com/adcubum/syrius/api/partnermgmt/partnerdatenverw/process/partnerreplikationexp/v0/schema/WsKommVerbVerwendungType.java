
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsErlaubnisCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsErlaubnisartCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKommVerbTypCodeType;


/**
 * Kommunikationsverbindungsverwendung eines Partners.
 * 
 * <p>Java class for WsKommVerbVerwendungType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsKommVerbVerwendungType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="erlaubnis" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsErlaubnisCodeType"/&gt;
 *         &lt;element name="erlaubnisart" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsErlaubnisartCodeType"/&gt;
 *         &lt;element name="verbindungstyp" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsKommVerbTypCodeType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungDurch" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungAm" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}OffsetDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsKommVerbVerwendungType", propOrder = {
    "erlaubnis",
    "erlaubnisart",
    "verbindungstyp",
    "aenderungDurch",
    "aenderungAm"
})
public class WsKommVerbVerwendungType {

    @XmlElement(required = true)
    protected WsErlaubnisCodeType erlaubnis;
    @XmlElement(required = true)
    protected WsErlaubnisartCodeType erlaubnisart;
    protected WsKommVerbTypCodeType verbindungstyp;
    protected String aenderungDurch;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aenderungAm;

    /**
     * Gets the value of the erlaubnis property.
     * 
     * @return
     *     possible object is
     *     {@link WsErlaubnisCodeType }
     *     
     */
    public WsErlaubnisCodeType getErlaubnis() {
        return erlaubnis;
    }

    /**
     * Sets the value of the erlaubnis property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsErlaubnisCodeType }
     *     
     */
    public void setErlaubnis(WsErlaubnisCodeType value) {
        this.erlaubnis = value;
    }

    /**
     * Gets the value of the erlaubnisart property.
     * 
     * @return
     *     possible object is
     *     {@link WsErlaubnisartCodeType }
     *     
     */
    public WsErlaubnisartCodeType getErlaubnisart() {
        return erlaubnisart;
    }

    /**
     * Sets the value of the erlaubnisart property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsErlaubnisartCodeType }
     *     
     */
    public void setErlaubnisart(WsErlaubnisartCodeType value) {
        this.erlaubnisart = value;
    }

    /**
     * Gets the value of the verbindungstyp property.
     * 
     * @return
     *     possible object is
     *     {@link WsKommVerbTypCodeType }
     *     
     */
    public WsKommVerbTypCodeType getVerbindungstyp() {
        return verbindungstyp;
    }

    /**
     * Sets the value of the verbindungstyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsKommVerbTypCodeType }
     *     
     */
    public void setVerbindungstyp(WsKommVerbTypCodeType value) {
        this.verbindungstyp = value;
    }

    /**
     * Gets the value of the aenderungDurch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAenderungDurch() {
        return aenderungDurch;
    }

    /**
     * Sets the value of the aenderungDurch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAenderungDurch(String value) {
        this.aenderungDurch = value;
    }

    /**
     * Gets the value of the aenderungAm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAenderungAm() {
        return aenderungAm;
    }

    /**
     * Sets the value of the aenderungAm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAenderungAm(XMLGregorianCalendar value) {
        this.aenderungAm = value;
    }

}
