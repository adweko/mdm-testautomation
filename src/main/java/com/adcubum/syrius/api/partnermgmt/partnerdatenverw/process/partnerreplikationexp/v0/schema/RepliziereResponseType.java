
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Response für das Replizieren eines Objekts.
 * 
 * <p>Java class for RepliziereResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliziereResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="replikationErfolgreich" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}BooleanType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliziereResponseType", propOrder = {
    "replikationErfolgreich"
})
public class RepliziereResponseType {

    protected int replikationErfolgreich;

    /**
     * Gets the value of the replikationErfolgreich property.
     * 
     */
    public int getReplikationErfolgreich() {
        return replikationErfolgreich;
    }

    /**
     * Sets the value of the replikationErfolgreich property.
     * 
     */
    public void setReplikationErfolgreich(int value) {
        this.replikationErfolgreich = value;
    }

}
