
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKommVerbTypCodeType;


/**
 * Request für das Löschen einer Kommunikationsverbindungsverwendung.
 * 
 * <p>Java class for RepliziereDeleteKommVerbVerwendungRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliziereDeleteKommVerbVerwendungRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partnerReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerReferenzType"/&gt;
 *         &lt;element name="kommVerbVerwendungTyp" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsKommVerbTypCodeType"/&gt;
 *         &lt;element name="messageId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="externeVersion" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LongType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliziereDeleteKommVerbVerwendungRequestType", propOrder = {
    "partnerReferenz",
    "kommVerbVerwendungTyp",
    "messageId",
    "externeVersion"
})
public class RepliziereDeleteKommVerbVerwendungRequestType {

    @XmlElement(required = true)
    protected WsPartnerReferenzType partnerReferenz;
    @XmlElement(required = true)
    protected WsKommVerbTypCodeType kommVerbVerwendungTyp;
    @XmlElement(required = true)
    protected String messageId;
    protected Long externeVersion;

    /**
     * Gets the value of the partnerReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public WsPartnerReferenzType getPartnerReferenz() {
        return partnerReferenz;
    }

    /**
     * Sets the value of the partnerReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public void setPartnerReferenz(WsPartnerReferenzType value) {
        this.partnerReferenz = value;
    }

    /**
     * Gets the value of the kommVerbVerwendungTyp property.
     * 
     * @return
     *     possible object is
     *     {@link WsKommVerbTypCodeType }
     *     
     */
    public WsKommVerbTypCodeType getKommVerbVerwendungTyp() {
        return kommVerbVerwendungTyp;
    }

    /**
     * Sets the value of the kommVerbVerwendungTyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsKommVerbTypCodeType }
     *     
     */
    public void setKommVerbVerwendungTyp(WsKommVerbTypCodeType value) {
        this.kommVerbVerwendungTyp = value;
    }

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the externeVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExterneVersion() {
        return externeVersion;
    }

    /**
     * Sets the value of the externeVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExterneVersion(Long value) {
        this.externeVersion = value;
    }

}
