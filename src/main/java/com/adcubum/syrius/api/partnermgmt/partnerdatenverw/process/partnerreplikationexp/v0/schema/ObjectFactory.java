
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RepliziereNatPersonResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereNatPersonResponse");
    private final static QName _RepliziereNatPersonRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereNatPersonRequest");
    private final static QName _RepliziereJurPersonResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereJurPersonResponse");
    private final static QName _RepliziereJurPersonRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereJurPersonRequest");
    private final static QName _RepliziereDomiziladresseResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDomiziladresseResponse");
    private final static QName _RepliziereDomiziladresseRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDomiziladresseRequest");
    private final static QName _RepliziereZusatzadresseResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereZusatzadresseResponse");
    private final static QName _RepliziereZusatzadresseRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereZusatzadresseRequest");
    private final static QName _ReplizierePartnerrolleResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "replizierePartnerrolleResponse");
    private final static QName _ReplizierePartnerrolleRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "replizierePartnerrolleRequest");
    private final static QName _RepliziereEmailResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereEmailResponse");
    private final static QName _RepliziereEmailRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereEmailRequest");
    private final static QName _RepliziereEmailForAdresseResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereEmailForAdresseResponse");
    private final static QName _RepliziereEmailForAdresseRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereEmailForAdresseRequest");
    private final static QName _RepliziereTelefonResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereTelefonResponse");
    private final static QName _RepliziereTelefonRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereTelefonRequest");
    private final static QName _RepliziereTelefonForAdresseResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereTelefonForAdresseResponse");
    private final static QName _RepliziereTelefonForAdresseRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereTelefonForAdresseRequest");
    private final static QName _RepliziereWebResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereWebResponse");
    private final static QName _RepliziereWebRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereWebRequest");
    private final static QName _RepliziereWebForAdresseResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereWebForAdresseResponse");
    private final static QName _RepliziereWebForAdresseRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereWebForAdresseRequest");
    private final static QName _RepliziereDublettenResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDublettenResponse");
    private final static QName _RepliziereDublettenRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDublettenRequest");
    private final static QName _RepliziereDeleteNatPersonResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteNatPersonResponse");
    private final static QName _RepliziereDeleteNatPersonRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteNatPersonRequest");
    private final static QName _RepliziereDeleteJurPersonResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteJurPersonResponse");
    private final static QName _RepliziereDeleteJurPersonRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteJurPersonRequest");
    private final static QName _RepliziereDeletePartnerrolleResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeletePartnerrolleResponse");
    private final static QName _RepliziereDeletePartnerrolleRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeletePartnerrolleRequest");
    private final static QName _RepliziereDeleteDomiziladresseResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteDomiziladresseResponse");
    private final static QName _RepliziereDeleteDomiziladresseRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteDomiziladresseRequest");
    private final static QName _RepliziereDeleteZusatzadresseResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteZusatzadresseResponse");
    private final static QName _RepliziereDeleteZusatzadresseRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteZusatzadresseRequest");
    private final static QName _RepliziereDeleteEmailResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteEmailResponse");
    private final static QName _RepliziereDeleteEmailRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteEmailRequest");
    private final static QName _RepliziereDeleteTelefonResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteTelefonResponse");
    private final static QName _RepliziereDeleteTelefonRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteTelefonRequest");
    private final static QName _RepliziereDeleteWebResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteWebResponse");
    private final static QName _RepliziereDeleteWebRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteWebRequest");
    private final static QName _RepliziereDeleteKommVerbVerwendungResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungResponse");
    private final static QName _RepliziereDeleteKommVerbVerwendungRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungRequest");
    private final static QName _RepliziereDeleteKommVerbVerwendungForAdresseResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungForAdresseResponse");
    private final static QName _RepliziereDeleteKommVerbVerwendungForAdresseRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungForAdresseRequest");
    private final static QName _RepliziereDeleteKommVerbVerwendungForEmailResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungForEmailResponse");
    private final static QName _RepliziereDeleteKommVerbVerwendungForEmailRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungForEmailRequest");
    private final static QName _RepliziereDeleteKommVerbVerwendungForTelefonResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungForTelefonResponse");
    private final static QName _RepliziereDeleteKommVerbVerwendungForTelefonRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungForTelefonRequest");
    private final static QName _RepliziereDeleteKommVerbVerwendungForWebResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungForWebResponse");
    private final static QName _RepliziereDeleteKommVerbVerwendungForWebRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeleteKommVerbVerwendungForWebRequest");
    private final static QName _RepliziereDeletePartneridentifikatorResponse_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeletePartneridentifikatorResponse");
    private final static QName _RepliziereDeletePartneridentifikatorRequest_QNAME = new QName("urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", "repliziereDeletePartneridentifikatorRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RepliziereResponseType }
     * 
     */
    public RepliziereResponseType createRepliziereResponseType() {
        return new RepliziereResponseType();
    }

    /**
     * Create an instance of {@link RepliziereNatPersonRequestType }
     * 
     */
    public RepliziereNatPersonRequestType createRepliziereNatPersonRequestType() {
        return new RepliziereNatPersonRequestType();
    }

    /**
     * Create an instance of {@link RepliziereJurPersonRequestType }
     * 
     */
    public RepliziereJurPersonRequestType createRepliziereJurPersonRequestType() {
        return new RepliziereJurPersonRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDomiziladresseRequestType }
     * 
     */
    public RepliziereDomiziladresseRequestType createRepliziereDomiziladresseRequestType() {
        return new RepliziereDomiziladresseRequestType();
    }

    /**
     * Create an instance of {@link RepliziereZusatzadresseRequestType }
     * 
     */
    public RepliziereZusatzadresseRequestType createRepliziereZusatzadresseRequestType() {
        return new RepliziereZusatzadresseRequestType();
    }

    /**
     * Create an instance of {@link ReplizierePartnerrolleRequestType }
     * 
     */
    public ReplizierePartnerrolleRequestType createReplizierePartnerrolleRequestType() {
        return new ReplizierePartnerrolleRequestType();
    }

    /**
     * Create an instance of {@link RepliziereEmailRequestType }
     * 
     */
    public RepliziereEmailRequestType createRepliziereEmailRequestType() {
        return new RepliziereEmailRequestType();
    }

    /**
     * Create an instance of {@link RepliziereEmailForAdresseRequestType }
     * 
     */
    public RepliziereEmailForAdresseRequestType createRepliziereEmailForAdresseRequestType() {
        return new RepliziereEmailForAdresseRequestType();
    }

    /**
     * Create an instance of {@link RepliziereTelefonRequestType }
     * 
     */
    public RepliziereTelefonRequestType createRepliziereTelefonRequestType() {
        return new RepliziereTelefonRequestType();
    }

    /**
     * Create an instance of {@link RepliziereTelefonForAdresseRequestType }
     * 
     */
    public RepliziereTelefonForAdresseRequestType createRepliziereTelefonForAdresseRequestType() {
        return new RepliziereTelefonForAdresseRequestType();
    }

    /**
     * Create an instance of {@link RepliziereWebRequestType }
     * 
     */
    public RepliziereWebRequestType createRepliziereWebRequestType() {
        return new RepliziereWebRequestType();
    }

    /**
     * Create an instance of {@link RepliziereWebForAdresseRequestType }
     * 
     */
    public RepliziereWebForAdresseRequestType createRepliziereWebForAdresseRequestType() {
        return new RepliziereWebForAdresseRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDublettenRequestType }
     * 
     */
    public RepliziereDublettenRequestType createRepliziereDublettenRequestType() {
        return new RepliziereDublettenRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteNatPersonRequestType }
     * 
     */
    public RepliziereDeleteNatPersonRequestType createRepliziereDeleteNatPersonRequestType() {
        return new RepliziereDeleteNatPersonRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteJurPersonRequestType }
     * 
     */
    public RepliziereDeleteJurPersonRequestType createRepliziereDeleteJurPersonRequestType() {
        return new RepliziereDeleteJurPersonRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeletePartnerrolleRequestType }
     * 
     */
    public RepliziereDeletePartnerrolleRequestType createRepliziereDeletePartnerrolleRequestType() {
        return new RepliziereDeletePartnerrolleRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteDomiziladresseRequestType }
     * 
     */
    public RepliziereDeleteDomiziladresseRequestType createRepliziereDeleteDomiziladresseRequestType() {
        return new RepliziereDeleteDomiziladresseRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteZusatzadresseRequestType }
     * 
     */
    public RepliziereDeleteZusatzadresseRequestType createRepliziereDeleteZusatzadresseRequestType() {
        return new RepliziereDeleteZusatzadresseRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteEmailRequestType }
     * 
     */
    public RepliziereDeleteEmailRequestType createRepliziereDeleteEmailRequestType() {
        return new RepliziereDeleteEmailRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteTelefonRequestType }
     * 
     */
    public RepliziereDeleteTelefonRequestType createRepliziereDeleteTelefonRequestType() {
        return new RepliziereDeleteTelefonRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteWebRequestType }
     * 
     */
    public RepliziereDeleteWebRequestType createRepliziereDeleteWebRequestType() {
        return new RepliziereDeleteWebRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteKommVerbVerwendungRequestType }
     * 
     */
    public RepliziereDeleteKommVerbVerwendungRequestType createRepliziereDeleteKommVerbVerwendungRequestType() {
        return new RepliziereDeleteKommVerbVerwendungRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteKommVerbVerwendungForAdresseRequestType }
     * 
     */
    public RepliziereDeleteKommVerbVerwendungForAdresseRequestType createRepliziereDeleteKommVerbVerwendungForAdresseRequestType() {
        return new RepliziereDeleteKommVerbVerwendungForAdresseRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteKommVerbVerwendungForEmailRequestType }
     * 
     */
    public RepliziereDeleteKommVerbVerwendungForEmailRequestType createRepliziereDeleteKommVerbVerwendungForEmailRequestType() {
        return new RepliziereDeleteKommVerbVerwendungForEmailRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteKommVerbVerwendungForTelefonRequestType }
     * 
     */
    public RepliziereDeleteKommVerbVerwendungForTelefonRequestType createRepliziereDeleteKommVerbVerwendungForTelefonRequestType() {
        return new RepliziereDeleteKommVerbVerwendungForTelefonRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeleteKommVerbVerwendungForWebRequestType }
     * 
     */
    public RepliziereDeleteKommVerbVerwendungForWebRequestType createRepliziereDeleteKommVerbVerwendungForWebRequestType() {
        return new RepliziereDeleteKommVerbVerwendungForWebRequestType();
    }

    /**
     * Create an instance of {@link RepliziereDeletePartneridentifikatorRequestType }
     * 
     */
    public RepliziereDeletePartneridentifikatorRequestType createRepliziereDeletePartneridentifikatorRequestType() {
        return new RepliziereDeletePartneridentifikatorRequestType();
    }

    /**
     * Create an instance of {@link WsAdresseReferenzType }
     * 
     */
    public WsAdresseReferenzType createWsAdresseReferenzType() {
        return new WsAdresseReferenzType();
    }

    /**
     * Create an instance of {@link WsPartnerReferenzType }
     * 
     */
    public WsPartnerReferenzType createWsPartnerReferenzType() {
        return new WsPartnerReferenzType();
    }

    /**
     * Create an instance of {@link WsEmailReferenzType }
     * 
     */
    public WsEmailReferenzType createWsEmailReferenzType() {
        return new WsEmailReferenzType();
    }

    /**
     * Create an instance of {@link WsTelefonReferenzType }
     * 
     */
    public WsTelefonReferenzType createWsTelefonReferenzType() {
        return new WsTelefonReferenzType();
    }

    /**
     * Create an instance of {@link WsWebReferenzType }
     * 
     */
    public WsWebReferenzType createWsWebReferenzType() {
        return new WsWebReferenzType();
    }

    /**
     * Create an instance of {@link WsPartnerrolleReferenzType }
     * 
     */
    public WsPartnerrolleReferenzType createWsPartnerrolleReferenzType() {
        return new WsPartnerrolleReferenzType();
    }

    /**
     * Create an instance of {@link WsPartneridentifikatorReferenzType }
     * 
     */
    public WsPartneridentifikatorReferenzType createWsPartneridentifikatorReferenzType() {
        return new WsPartneridentifikatorReferenzType();
    }

    /**
     * Create an instance of {@link WsNatPersonType }
     * 
     */
    public WsNatPersonType createWsNatPersonType() {
        return new WsNatPersonType();
    }

    /**
     * Create an instance of {@link WsJurPersonType }
     * 
     */
    public WsJurPersonType createWsJurPersonType() {
        return new WsJurPersonType();
    }

    /**
     * Create an instance of {@link WsPartneridentifikatorType }
     * 
     */
    public WsPartneridentifikatorType createWsPartneridentifikatorType() {
        return new WsPartneridentifikatorType();
    }

    /**
     * Create an instance of {@link WsExterneReferenzType }
     * 
     */
    public WsExterneReferenzType createWsExterneReferenzType() {
        return new WsExterneReferenzType();
    }

    /**
     * Create an instance of {@link WsNatPersonStrukturType }
     * 
     */
    public WsNatPersonStrukturType createWsNatPersonStrukturType() {
        return new WsNatPersonStrukturType();
    }

    /**
     * Create an instance of {@link WsJurPersonStrukturType }
     * 
     */
    public WsJurPersonStrukturType createWsJurPersonStrukturType() {
        return new WsJurPersonStrukturType();
    }

    /**
     * Create an instance of {@link WsOeZuteilungStrukturType }
     * 
     */
    public WsOeZuteilungStrukturType createWsOeZuteilungStrukturType() {
        return new WsOeZuteilungStrukturType();
    }

    /**
     * Create an instance of {@link WsOeZuteilungType }
     * 
     */
    public WsOeZuteilungType createWsOeZuteilungType() {
        return new WsOeZuteilungType();
    }

    /**
     * Create an instance of {@link WsAdresseType }
     * 
     */
    public WsAdresseType createWsAdresseType() {
        return new WsAdresseType();
    }

    /**
     * Create an instance of {@link WsEmailType }
     * 
     */
    public WsEmailType createWsEmailType() {
        return new WsEmailType();
    }

    /**
     * Create an instance of {@link WsTelefonType }
     * 
     */
    public WsTelefonType createWsTelefonType() {
        return new WsTelefonType();
    }

    /**
     * Create an instance of {@link WsWebType }
     * 
     */
    public WsWebType createWsWebType() {
        return new WsWebType();
    }

    /**
     * Create an instance of {@link WsDomiziladresseStrukturType }
     * 
     */
    public WsDomiziladresseStrukturType createWsDomiziladresseStrukturType() {
        return new WsDomiziladresseStrukturType();
    }

    /**
     * Create an instance of {@link WsZusatzadresseStrukturType }
     * 
     */
    public WsZusatzadresseStrukturType createWsZusatzadresseStrukturType() {
        return new WsZusatzadresseStrukturType();
    }

    /**
     * Create an instance of {@link WsAdressrolleStrukturType }
     * 
     */
    public WsAdressrolleStrukturType createWsAdressrolleStrukturType() {
        return new WsAdressrolleStrukturType();
    }

    /**
     * Create an instance of {@link WsAdressrolleType }
     * 
     */
    public WsAdressrolleType createWsAdressrolleType() {
        return new WsAdressrolleType();
    }

    /**
     * Create an instance of {@link WsPartnerrolleStrukturType }
     * 
     */
    public WsPartnerrolleStrukturType createWsPartnerrolleStrukturType() {
        return new WsPartnerrolleStrukturType();
    }

    /**
     * Create an instance of {@link WsPartnerrolleType }
     * 
     */
    public WsPartnerrolleType createWsPartnerrolleType() {
        return new WsPartnerrolleType();
    }

    /**
     * Create an instance of {@link WsKommVerbVerwendungType }
     * 
     */
    public WsKommVerbVerwendungType createWsKommVerbVerwendungType() {
        return new WsKommVerbVerwendungType();
    }

    /**
     * Create an instance of {@link WsEmailStrukturType }
     * 
     */
    public WsEmailStrukturType createWsEmailStrukturType() {
        return new WsEmailStrukturType();
    }

    /**
     * Create an instance of {@link WsTelefonStrukturType }
     * 
     */
    public WsTelefonStrukturType createWsTelefonStrukturType() {
        return new WsTelefonStrukturType();
    }

    /**
     * Create an instance of {@link WsWebStrukturType }
     * 
     */
    public WsWebStrukturType createWsWebStrukturType() {
        return new WsWebStrukturType();
    }

    /**
     * Create an instance of {@link WsDubletteType }
     * 
     */
    public WsDubletteType createWsDubletteType() {
        return new WsDubletteType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereNatPersonResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereNatPersonResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereNatPersonResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereNatPersonRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereNatPersonRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereNatPersonRequest")
    public JAXBElement<RepliziereNatPersonRequestType> createRepliziereNatPersonRequest(RepliziereNatPersonRequestType value) {
        return new JAXBElement<RepliziereNatPersonRequestType>(_RepliziereNatPersonRequest_QNAME, RepliziereNatPersonRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereJurPersonResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereJurPersonResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereJurPersonResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereJurPersonRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereJurPersonRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereJurPersonRequest")
    public JAXBElement<RepliziereJurPersonRequestType> createRepliziereJurPersonRequest(RepliziereJurPersonRequestType value) {
        return new JAXBElement<RepliziereJurPersonRequestType>(_RepliziereJurPersonRequest_QNAME, RepliziereJurPersonRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDomiziladresseResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDomiziladresseResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDomiziladresseResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDomiziladresseRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDomiziladresseRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDomiziladresseRequest")
    public JAXBElement<RepliziereDomiziladresseRequestType> createRepliziereDomiziladresseRequest(RepliziereDomiziladresseRequestType value) {
        return new JAXBElement<RepliziereDomiziladresseRequestType>(_RepliziereDomiziladresseRequest_QNAME, RepliziereDomiziladresseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereZusatzadresseResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereZusatzadresseResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereZusatzadresseResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereZusatzadresseRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereZusatzadresseRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereZusatzadresseRequest")
    public JAXBElement<RepliziereZusatzadresseRequestType> createRepliziereZusatzadresseRequest(RepliziereZusatzadresseRequestType value) {
        return new JAXBElement<RepliziereZusatzadresseRequestType>(_RepliziereZusatzadresseRequest_QNAME, RepliziereZusatzadresseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "replizierePartnerrolleResponse")
    public JAXBElement<RepliziereResponseType> createReplizierePartnerrolleResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_ReplizierePartnerrolleResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReplizierePartnerrolleRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ReplizierePartnerrolleRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "replizierePartnerrolleRequest")
    public JAXBElement<ReplizierePartnerrolleRequestType> createReplizierePartnerrolleRequest(ReplizierePartnerrolleRequestType value) {
        return new JAXBElement<ReplizierePartnerrolleRequestType>(_ReplizierePartnerrolleRequest_QNAME, ReplizierePartnerrolleRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereEmailResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereEmailResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereEmailResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereEmailRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereEmailRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereEmailRequest")
    public JAXBElement<RepliziereEmailRequestType> createRepliziereEmailRequest(RepliziereEmailRequestType value) {
        return new JAXBElement<RepliziereEmailRequestType>(_RepliziereEmailRequest_QNAME, RepliziereEmailRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereEmailForAdresseResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereEmailForAdresseResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereEmailForAdresseResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereEmailForAdresseRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereEmailForAdresseRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereEmailForAdresseRequest")
    public JAXBElement<RepliziereEmailForAdresseRequestType> createRepliziereEmailForAdresseRequest(RepliziereEmailForAdresseRequestType value) {
        return new JAXBElement<RepliziereEmailForAdresseRequestType>(_RepliziereEmailForAdresseRequest_QNAME, RepliziereEmailForAdresseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereTelefonResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereTelefonResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereTelefonResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereTelefonRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereTelefonRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereTelefonRequest")
    public JAXBElement<RepliziereTelefonRequestType> createRepliziereTelefonRequest(RepliziereTelefonRequestType value) {
        return new JAXBElement<RepliziereTelefonRequestType>(_RepliziereTelefonRequest_QNAME, RepliziereTelefonRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereTelefonForAdresseResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereTelefonForAdresseResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereTelefonForAdresseResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereTelefonForAdresseRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereTelefonForAdresseRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereTelefonForAdresseRequest")
    public JAXBElement<RepliziereTelefonForAdresseRequestType> createRepliziereTelefonForAdresseRequest(RepliziereTelefonForAdresseRequestType value) {
        return new JAXBElement<RepliziereTelefonForAdresseRequestType>(_RepliziereTelefonForAdresseRequest_QNAME, RepliziereTelefonForAdresseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereWebResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereWebResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereWebResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereWebRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereWebRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereWebRequest")
    public JAXBElement<RepliziereWebRequestType> createRepliziereWebRequest(RepliziereWebRequestType value) {
        return new JAXBElement<RepliziereWebRequestType>(_RepliziereWebRequest_QNAME, RepliziereWebRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereWebForAdresseResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereWebForAdresseResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereWebForAdresseResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereWebForAdresseRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereWebForAdresseRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereWebForAdresseRequest")
    public JAXBElement<RepliziereWebForAdresseRequestType> createRepliziereWebForAdresseRequest(RepliziereWebForAdresseRequestType value) {
        return new JAXBElement<RepliziereWebForAdresseRequestType>(_RepliziereWebForAdresseRequest_QNAME, RepliziereWebForAdresseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDublettenResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDublettenResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDublettenResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDublettenRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDublettenRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDublettenRequest")
    public JAXBElement<RepliziereDublettenRequestType> createRepliziereDublettenRequest(RepliziereDublettenRequestType value) {
        return new JAXBElement<RepliziereDublettenRequestType>(_RepliziereDublettenRequest_QNAME, RepliziereDublettenRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteNatPersonResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteNatPersonResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteNatPersonResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteNatPersonRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteNatPersonRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteNatPersonRequest")
    public JAXBElement<RepliziereDeleteNatPersonRequestType> createRepliziereDeleteNatPersonRequest(RepliziereDeleteNatPersonRequestType value) {
        return new JAXBElement<RepliziereDeleteNatPersonRequestType>(_RepliziereDeleteNatPersonRequest_QNAME, RepliziereDeleteNatPersonRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteJurPersonResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteJurPersonResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteJurPersonResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteJurPersonRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteJurPersonRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteJurPersonRequest")
    public JAXBElement<RepliziereDeleteJurPersonRequestType> createRepliziereDeleteJurPersonRequest(RepliziereDeleteJurPersonRequestType value) {
        return new JAXBElement<RepliziereDeleteJurPersonRequestType>(_RepliziereDeleteJurPersonRequest_QNAME, RepliziereDeleteJurPersonRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeletePartnerrolleResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeletePartnerrolleResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeletePartnerrolleResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeletePartnerrolleRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeletePartnerrolleRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeletePartnerrolleRequest")
    public JAXBElement<RepliziereDeletePartnerrolleRequestType> createRepliziereDeletePartnerrolleRequest(RepliziereDeletePartnerrolleRequestType value) {
        return new JAXBElement<RepliziereDeletePartnerrolleRequestType>(_RepliziereDeletePartnerrolleRequest_QNAME, RepliziereDeletePartnerrolleRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteDomiziladresseResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteDomiziladresseResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteDomiziladresseResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteDomiziladresseRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteDomiziladresseRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteDomiziladresseRequest")
    public JAXBElement<RepliziereDeleteDomiziladresseRequestType> createRepliziereDeleteDomiziladresseRequest(RepliziereDeleteDomiziladresseRequestType value) {
        return new JAXBElement<RepliziereDeleteDomiziladresseRequestType>(_RepliziereDeleteDomiziladresseRequest_QNAME, RepliziereDeleteDomiziladresseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteZusatzadresseResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteZusatzadresseResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteZusatzadresseResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteZusatzadresseRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteZusatzadresseRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteZusatzadresseRequest")
    public JAXBElement<RepliziereDeleteZusatzadresseRequestType> createRepliziereDeleteZusatzadresseRequest(RepliziereDeleteZusatzadresseRequestType value) {
        return new JAXBElement<RepliziereDeleteZusatzadresseRequestType>(_RepliziereDeleteZusatzadresseRequest_QNAME, RepliziereDeleteZusatzadresseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteEmailResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteEmailResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteEmailResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteEmailRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteEmailRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteEmailRequest")
    public JAXBElement<RepliziereDeleteEmailRequestType> createRepliziereDeleteEmailRequest(RepliziereDeleteEmailRequestType value) {
        return new JAXBElement<RepliziereDeleteEmailRequestType>(_RepliziereDeleteEmailRequest_QNAME, RepliziereDeleteEmailRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteTelefonResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteTelefonResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteTelefonResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteTelefonRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteTelefonRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteTelefonRequest")
    public JAXBElement<RepliziereDeleteTelefonRequestType> createRepliziereDeleteTelefonRequest(RepliziereDeleteTelefonRequestType value) {
        return new JAXBElement<RepliziereDeleteTelefonRequestType>(_RepliziereDeleteTelefonRequest_QNAME, RepliziereDeleteTelefonRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteWebResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteWebResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteWebResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteWebRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteWebRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteWebRequest")
    public JAXBElement<RepliziereDeleteWebRequestType> createRepliziereDeleteWebRequest(RepliziereDeleteWebRequestType value) {
        return new JAXBElement<RepliziereDeleteWebRequestType>(_RepliziereDeleteWebRequest_QNAME, RepliziereDeleteWebRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteKommVerbVerwendungResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteKommVerbVerwendungResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungRequest")
    public JAXBElement<RepliziereDeleteKommVerbVerwendungRequestType> createRepliziereDeleteKommVerbVerwendungRequest(RepliziereDeleteKommVerbVerwendungRequestType value) {
        return new JAXBElement<RepliziereDeleteKommVerbVerwendungRequestType>(_RepliziereDeleteKommVerbVerwendungRequest_QNAME, RepliziereDeleteKommVerbVerwendungRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungForAdresseResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteKommVerbVerwendungForAdresseResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteKommVerbVerwendungForAdresseResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungForAdresseRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungForAdresseRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungForAdresseRequest")
    public JAXBElement<RepliziereDeleteKommVerbVerwendungForAdresseRequestType> createRepliziereDeleteKommVerbVerwendungForAdresseRequest(RepliziereDeleteKommVerbVerwendungForAdresseRequestType value) {
        return new JAXBElement<RepliziereDeleteKommVerbVerwendungForAdresseRequestType>(_RepliziereDeleteKommVerbVerwendungForAdresseRequest_QNAME, RepliziereDeleteKommVerbVerwendungForAdresseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungForEmailResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteKommVerbVerwendungForEmailResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteKommVerbVerwendungForEmailResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungForEmailRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungForEmailRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungForEmailRequest")
    public JAXBElement<RepliziereDeleteKommVerbVerwendungForEmailRequestType> createRepliziereDeleteKommVerbVerwendungForEmailRequest(RepliziereDeleteKommVerbVerwendungForEmailRequestType value) {
        return new JAXBElement<RepliziereDeleteKommVerbVerwendungForEmailRequestType>(_RepliziereDeleteKommVerbVerwendungForEmailRequest_QNAME, RepliziereDeleteKommVerbVerwendungForEmailRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungForTelefonResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteKommVerbVerwendungForTelefonResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteKommVerbVerwendungForTelefonResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungForTelefonRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungForTelefonRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungForTelefonRequest")
    public JAXBElement<RepliziereDeleteKommVerbVerwendungForTelefonRequestType> createRepliziereDeleteKommVerbVerwendungForTelefonRequest(RepliziereDeleteKommVerbVerwendungForTelefonRequestType value) {
        return new JAXBElement<RepliziereDeleteKommVerbVerwendungForTelefonRequestType>(_RepliziereDeleteKommVerbVerwendungForTelefonRequest_QNAME, RepliziereDeleteKommVerbVerwendungForTelefonRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungForWebResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeleteKommVerbVerwendungForWebResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeleteKommVerbVerwendungForWebResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungForWebRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeleteKommVerbVerwendungForWebRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeleteKommVerbVerwendungForWebRequest")
    public JAXBElement<RepliziereDeleteKommVerbVerwendungForWebRequestType> createRepliziereDeleteKommVerbVerwendungForWebRequest(RepliziereDeleteKommVerbVerwendungForWebRequestType value) {
        return new JAXBElement<RepliziereDeleteKommVerbVerwendungForWebRequestType>(_RepliziereDeleteKommVerbVerwendungForWebRequest_QNAME, RepliziereDeleteKommVerbVerwendungForWebRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeletePartneridentifikatorResponse")
    public JAXBElement<RepliziereResponseType> createRepliziereDeletePartneridentifikatorResponse(RepliziereResponseType value) {
        return new JAXBElement<RepliziereResponseType>(_RepliziereDeletePartneridentifikatorResponse_QNAME, RepliziereResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepliziereDeletePartneridentifikatorRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RepliziereDeletePartneridentifikatorRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema", name = "repliziereDeletePartneridentifikatorRequest")
    public JAXBElement<RepliziereDeletePartneridentifikatorRequestType> createRepliziereDeletePartneridentifikatorRequest(RepliziereDeletePartneridentifikatorRequestType value) {
        return new JAXBElement<RepliziereDeletePartneridentifikatorRequestType>(_RepliziereDeletePartneridentifikatorRequest_QNAME, RepliziereDeletePartneridentifikatorRequestType.class, null, value);
    }

}
