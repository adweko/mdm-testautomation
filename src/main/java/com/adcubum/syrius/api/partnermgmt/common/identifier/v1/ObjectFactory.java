
package com.adcubum.syrius.api.partnermgmt.common.identifier.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.adcubum.syrius.api.partnermgmt.common.identifier.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.adcubum.syrius.api.partnermgmt.common.identifier.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WsAdresseIdType }
     * 
     */
    public WsAdresseIdType createWsAdresseIdType() {
        return new WsAdresseIdType();
    }

    /**
     * Create an instance of {@link WsAdressrolleIdType }
     * 
     */
    public WsAdressrolleIdType createWsAdressrolleIdType() {
        return new WsAdressrolleIdType();
    }

    /**
     * Create an instance of {@link WsAkteIdType }
     * 
     */
    public WsAkteIdType createWsAkteIdType() {
        return new WsAkteIdType();
    }

    /**
     * Create an instance of {@link WsAuslaenderstatusIdType }
     * 
     */
    public WsAuslaenderstatusIdType createWsAuslaenderstatusIdType() {
        return new WsAuslaenderstatusIdType();
    }

    /**
     * Create an instance of {@link WsBenutzerIdType }
     * 
     */
    public WsBenutzerIdType createWsBenutzerIdType() {
        return new WsBenutzerIdType();
    }

    /**
     * Create an instance of {@link WsBeziehungIdType }
     * 
     */
    public WsBeziehungIdType createWsBeziehungIdType() {
        return new WsBeziehungIdType();
    }

    /**
     * Create an instance of {@link WsEmailIdType }
     * 
     */
    public WsEmailIdType createWsEmailIdType() {
        return new WsEmailIdType();
    }

    /**
     * Create an instance of {@link WsFaktIdType }
     * 
     */
    public WsFaktIdType createWsFaktIdType() {
        return new WsFaktIdType();
    }

    /**
     * Create an instance of {@link WsKommVerbVerwendungIdType }
     * 
     */
    public WsKommVerbVerwendungIdType createWsKommVerbVerwendungIdType() {
        return new WsKommVerbVerwendungIdType();
    }

    /**
     * Create an instance of {@link WsKontaktIdType }
     * 
     */
    public WsKontaktIdType createWsKontaktIdType() {
        return new WsKontaktIdType();
    }

    /**
     * Create an instance of {@link WsLeistungserbringerIdType }
     * 
     */
    public WsLeistungserbringerIdType createWsLeistungserbringerIdType() {
        return new WsLeistungserbringerIdType();
    }

    /**
     * Create an instance of {@link WsOrganisationseinheitIdType }
     * 
     */
    public WsOrganisationseinheitIdType createWsOrganisationseinheitIdType() {
        return new WsOrganisationseinheitIdType();
    }

    /**
     * Create an instance of {@link WsPartnerIdType }
     * 
     */
    public WsPartnerIdType createWsPartnerIdType() {
        return new WsPartnerIdType();
    }

    /**
     * Create an instance of {@link WsPartneridentifikatorIdType }
     * 
     */
    public WsPartneridentifikatorIdType createWsPartneridentifikatorIdType() {
        return new WsPartneridentifikatorIdType();
    }

    /**
     * Create an instance of {@link WsPartnerrolleIdType }
     * 
     */
    public WsPartnerrolleIdType createWsPartnerrolleIdType() {
        return new WsPartnerrolleIdType();
    }

    /**
     * Create an instance of {@link WsSepaMandatIdType }
     * 
     */
    public WsSepaMandatIdType createWsSepaMandatIdType() {
        return new WsSepaMandatIdType();
    }

    /**
     * Create an instance of {@link WsTelefonIdType }
     * 
     */
    public WsTelefonIdType createWsTelefonIdType() {
        return new WsTelefonIdType();
    }

    /**
     * Create an instance of {@link WsTextModulIdType }
     * 
     */
    public WsTextModulIdType createWsTextModulIdType() {
        return new WsTextModulIdType();
    }

    /**
     * Create an instance of {@link WsWebIdType }
     * 
     */
    public WsWebIdType createWsWebIdType() {
        return new WsWebIdType();
    }

    /**
     * Create an instance of {@link WsZahlungsverbindungIdType }
     * 
     */
    public WsZahlungsverbindungIdType createWsZahlungsverbindungIdType() {
        return new WsZahlungsverbindungIdType();
    }

    /**
     * Create an instance of {@link WsAdresseDefIdType }
     * 
     */
    public WsAdresseDefIdType createWsAdresseDefIdType() {
        return new WsAdresseDefIdType();
    }

    /**
     * Create an instance of {@link WsAdressrolleDefIdType }
     * 
     */
    public WsAdressrolleDefIdType createWsAdressrolleDefIdType() {
        return new WsAdressrolleDefIdType();
    }

    /**
     * Create an instance of {@link WsBeziehungDefIdType }
     * 
     */
    public WsBeziehungDefIdType createWsBeziehungDefIdType() {
        return new WsBeziehungDefIdType();
    }

    /**
     * Create an instance of {@link WsEmailDefIdType }
     * 
     */
    public WsEmailDefIdType createWsEmailDefIdType() {
        return new WsEmailDefIdType();
    }

    /**
     * Create an instance of {@link WsFaktDefIdType }
     * 
     */
    public WsFaktDefIdType createWsFaktDefIdType() {
        return new WsFaktDefIdType();
    }

    /**
     * Create an instance of {@link WsPartneridentifikatorDefIdType }
     * 
     */
    public WsPartneridentifikatorDefIdType createWsPartneridentifikatorDefIdType() {
        return new WsPartneridentifikatorDefIdType();
    }

    /**
     * Create an instance of {@link WsPartnerrolleDefIdType }
     * 
     */
    public WsPartnerrolleDefIdType createWsPartnerrolleDefIdType() {
        return new WsPartnerrolleDefIdType();
    }

    /**
     * Create an instance of {@link WsPartnerrolleAttrDefIdType }
     * 
     */
    public WsPartnerrolleAttrDefIdType createWsPartnerrolleAttrDefIdType() {
        return new WsPartnerrolleAttrDefIdType();
    }

    /**
     * Create an instance of {@link WsPartnerschutzDefIdType }
     * 
     */
    public WsPartnerschutzDefIdType createWsPartnerschutzDefIdType() {
        return new WsPartnerschutzDefIdType();
    }

    /**
     * Create an instance of {@link WsTelefonDefIdType }
     * 
     */
    public WsTelefonDefIdType createWsTelefonDefIdType() {
        return new WsTelefonDefIdType();
    }

    /**
     * Create an instance of {@link WsWebDefIdType }
     * 
     */
    public WsWebDefIdType createWsWebDefIdType() {
        return new WsWebDefIdType();
    }

    /**
     * Create an instance of {@link WsMutationsgrundIdType }
     * 
     */
    public WsMutationsgrundIdType createWsMutationsgrundIdType() {
        return new WsMutationsgrundIdType();
    }

    /**
     * Create an instance of {@link WsBankIdType }
     * 
     */
    public WsBankIdType createWsBankIdType() {
        return new WsBankIdType();
    }

    /**
     * Create an instance of {@link WsLandIdType }
     * 
     */
    public WsLandIdType createWsLandIdType() {
        return new WsLandIdType();
    }

    /**
     * Create an instance of {@link WsVertragIdType }
     * 
     */
    public WsVertragIdType createWsVertragIdType() {
        return new WsVertragIdType();
    }

    /**
     * Create an instance of {@link WsWaehrungIdType }
     * 
     */
    public WsWaehrungIdType createWsWaehrungIdType() {
        return new WsWaehrungIdType();
    }

    /**
     * Create an instance of {@link WsAdresseAenderungIdType }
     * 
     */
    public WsAdresseAenderungIdType createWsAdresseAenderungIdType() {
        return new WsAdresseAenderungIdType();
    }

    /**
     * Create an instance of {@link WsRechnungsstellerTypIdType }
     * 
     */
    public WsRechnungsstellerTypIdType createWsRechnungsstellerTypIdType() {
        return new WsRechnungsstellerTypIdType();
    }

    /**
     * Create an instance of {@link WsPartnerAenderungIdType }
     * 
     */
    public WsPartnerAenderungIdType createWsPartnerAenderungIdType() {
        return new WsPartnerAenderungIdType();
    }

    /**
     * Create an instance of {@link WsGbereichIdType }
     * 
     */
    public WsGbereichIdType createWsGbereichIdType() {
        return new WsGbereichIdType();
    }

    /**
     * Create an instance of {@link WsDokumentversandIdType }
     * 
     */
    public WsDokumentversandIdType createWsDokumentversandIdType() {
        return new WsDokumentversandIdType();
    }

    /**
     * Create an instance of {@link WsDokumenttypIdType }
     * 
     */
    public WsDokumenttypIdType createWsDokumenttypIdType() {
        return new WsDokumenttypIdType();
    }

    /**
     * Create an instance of {@link WsFalldossierIdType }
     * 
     */
    public WsFalldossierIdType createWsFalldossierIdType() {
        return new WsFalldossierIdType();
    }

    /**
     * Create an instance of {@link WsRegionTypIdType }
     * 
     */
    public WsRegionTypIdType createWsRegionTypIdType() {
        return new WsRegionTypIdType();
    }

    /**
     * Create an instance of {@link WsProcessIdType }
     * 
     */
    public WsProcessIdType createWsProcessIdType() {
        return new WsProcessIdType();
    }

    /**
     * Create an instance of {@link WsProcessDefIdType }
     * 
     */
    public WsProcessDefIdType createWsProcessDefIdType() {
        return new WsProcessDefIdType();
    }

    /**
     * Create an instance of {@link WsUserTaskIdType }
     * 
     */
    public WsUserTaskIdType createWsUserTaskIdType() {
        return new WsUserTaskIdType();
    }

    /**
     * Create an instance of {@link WsUserTaskDefIdType }
     * 
     */
    public WsUserTaskDefIdType createWsUserTaskDefIdType() {
        return new WsUserTaskDefIdType();
    }

    /**
     * Create an instance of {@link WsIntermediateEventDefIdType }
     * 
     */
    public WsIntermediateEventDefIdType createWsIntermediateEventDefIdType() {
        return new WsIntermediateEventDefIdType();
    }

    /**
     * Create an instance of {@link WsMessageTaskDefIdType }
     * 
     */
    public WsMessageTaskDefIdType createWsMessageTaskDefIdType() {
        return new WsMessageTaskDefIdType();
    }

    /**
     * Create an instance of {@link WsCodeIdType }
     * 
     */
    public WsCodeIdType createWsCodeIdType() {
        return new WsCodeIdType();
    }

}
