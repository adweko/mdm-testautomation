
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerIdType;


/**
 * Enthält die geladenen Partnerrollen eines Partners.
 * 
 * <p>Java class for WsPartnerPartnerrolleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPartnerPartnerrolleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partnerId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsPartnerIdType"/&gt;
 *         &lt;element name="partnerrolle" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema}WsPartnerrolleType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPartnerPartnerrolleType", propOrder = {
    "partnerId",
    "partnerrolle"
})
public class WsPartnerPartnerrolleType {

    @XmlElement(required = true)
    protected WsPartnerIdType partnerId;
    protected List<WsPartnerrolleType> partnerrolle;

    /**
     * Gets the value of the partnerId property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerIdType }
     *     
     */
    public WsPartnerIdType getPartnerId() {
        return partnerId;
    }

    /**
     * Sets the value of the partnerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerIdType }
     *     
     */
    public void setPartnerId(WsPartnerIdType value) {
        this.partnerId = value;
    }

    /**
     * Gets the value of the partnerrolle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partnerrolle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartnerrolle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsPartnerrolleType }
     * 
     * 
     */
    public List<WsPartnerrolleType> getPartnerrolle() {
        if (partnerrolle == null) {
            partnerrolle = new ArrayList<WsPartnerrolleType>();
        }
        return this.partnerrolle;
    }

}
