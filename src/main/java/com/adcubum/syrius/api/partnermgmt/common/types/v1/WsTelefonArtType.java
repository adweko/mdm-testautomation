
package com.adcubum.syrius.api.partnermgmt.common.types.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsTelefonArtType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WsTelefonArtType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="fax"/&gt;
 *     &lt;enumeration value="mobile"/&gt;
 *     &lt;enumeration value="telefon"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "WsTelefonArtType")
@XmlEnum
public enum WsTelefonArtType {

    @XmlEnumValue("fax")
    FAX("fax"),
    @XmlEnumValue("mobile")
    MOBILE("mobile"),
    @XmlEnumValue("telefon")
    TELEFON("telefon");
    private final String value;

    WsTelefonArtType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WsTelefonArtType fromValue(String v) {
        for (WsTelefonArtType c: WsTelefonArtType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
