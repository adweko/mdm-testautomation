
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Request für das Replizieren einer Telefon-Adresse.
 * 
 * <p>Java class for RepliziereTelefonRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepliziereTelefonRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="telefonReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsTelefonReferenzType"/&gt;
 *         &lt;element name="partnerReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerReferenzType"/&gt;
 *         &lt;element name="messageId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="telefonStruktur" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsTelefonStrukturType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepliziereTelefonRequestType", propOrder = {
    "telefonReferenz",
    "partnerReferenz",
    "messageId",
    "telefonStruktur"
})
public class RepliziereTelefonRequestType {

    @XmlElement(required = true)
    protected WsTelefonReferenzType telefonReferenz;
    @XmlElement(required = true)
    protected WsPartnerReferenzType partnerReferenz;
    @XmlElement(required = true)
    protected String messageId;
    @XmlElement(required = true)
    protected WsTelefonStrukturType telefonStruktur;

    /**
     * Gets the value of the telefonReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsTelefonReferenzType }
     *     
     */
    public WsTelefonReferenzType getTelefonReferenz() {
        return telefonReferenz;
    }

    /**
     * Sets the value of the telefonReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsTelefonReferenzType }
     *     
     */
    public void setTelefonReferenz(WsTelefonReferenzType value) {
        this.telefonReferenz = value;
    }

    /**
     * Gets the value of the partnerReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public WsPartnerReferenzType getPartnerReferenz() {
        return partnerReferenz;
    }

    /**
     * Sets the value of the partnerReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerReferenzType }
     *     
     */
    public void setPartnerReferenz(WsPartnerReferenzType value) {
        this.partnerReferenz = value;
    }

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the telefonStruktur property.
     * 
     * @return
     *     possible object is
     *     {@link WsTelefonStrukturType }
     *     
     */
    public WsTelefonStrukturType getTelefonStruktur() {
        return telefonStruktur;
    }

    /**
     * Sets the value of the telefonStruktur property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsTelefonStrukturType }
     *     
     */
    public void setTelefonStruktur(WsTelefonStrukturType value) {
        this.telefonStruktur = value;
    }

}
