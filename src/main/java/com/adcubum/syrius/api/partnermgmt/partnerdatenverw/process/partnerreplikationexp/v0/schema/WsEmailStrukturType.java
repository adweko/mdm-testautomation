
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsEmailDefIdType;


/**
 * Alle Fachlichen States einer Email-Adresse.
 * 
 * <p>Java class for WsEmailStrukturType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsEmailStrukturType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="gueltAb" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="gueltBis" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="externeVersion" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LongType" minOccurs="0"/&gt;
 *         &lt;element name="emailDefId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsEmailDefIdType" minOccurs="0"/&gt;
 *         &lt;element name="emailStates" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsEmailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="kommVerbVerwendung" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsKommVerbVerwendungType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsEmailStrukturType", propOrder = {
    "gueltAb",
    "gueltBis",
    "externeVersion",
    "emailDefId",
    "emailStates",
    "kommVerbVerwendung"
})
public class WsEmailStrukturType {

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltAb;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltBis;
    protected Long externeVersion;
    protected WsEmailDefIdType emailDefId;
    protected List<WsEmailType> emailStates;
    protected WsKommVerbVerwendungType kommVerbVerwendung;

    /**
     * Gets the value of the gueltAb property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltAb() {
        return gueltAb;
    }

    /**
     * Sets the value of the gueltAb property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltAb(XMLGregorianCalendar value) {
        this.gueltAb = value;
    }

    /**
     * Gets the value of the gueltBis property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltBis() {
        return gueltBis;
    }

    /**
     * Sets the value of the gueltBis property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltBis(XMLGregorianCalendar value) {
        this.gueltBis = value;
    }

    /**
     * Gets the value of the externeVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExterneVersion() {
        return externeVersion;
    }

    /**
     * Sets the value of the externeVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExterneVersion(Long value) {
        this.externeVersion = value;
    }

    /**
     * Gets the value of the emailDefId property.
     * 
     * @return
     *     possible object is
     *     {@link WsEmailDefIdType }
     *     
     */
    public WsEmailDefIdType getEmailDefId() {
        return emailDefId;
    }

    /**
     * Sets the value of the emailDefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsEmailDefIdType }
     *     
     */
    public void setEmailDefId(WsEmailDefIdType value) {
        this.emailDefId = value;
    }

    /**
     * Gets the value of the emailStates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the emailStates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmailStates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsEmailType }
     * 
     * 
     */
    public List<WsEmailType> getEmailStates() {
        if (emailStates == null) {
            emailStates = new ArrayList<WsEmailType>();
        }
        return this.emailStates;
    }

    /**
     * Gets the value of the kommVerbVerwendung property.
     * 
     * @return
     *     possible object is
     *     {@link WsKommVerbVerwendungType }
     *     
     */
    public WsKommVerbVerwendungType getKommVerbVerwendung() {
        return kommVerbVerwendung;
    }

    /**
     * Sets the value of the kommVerbVerwendung property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsKommVerbVerwendungType }
     *     
     */
    public void setKommVerbVerwendung(WsKommVerbVerwendungType value) {
        this.kommVerbVerwendung = value;
    }

}
