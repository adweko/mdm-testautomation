
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsArbeitslosCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsAufenthaltsbewCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsBerufsgruppeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsGeschlechtCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsInAusbildungCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsSozialhilfeCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsTitelCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsUnterstPflichtCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsZivilstandCodeType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsAuslaenderstatusIdType;


/**
 * Enthält die spezifischen Daten zu einer natürlichen Person.
 * 
 * <p>Java class for WsNatPersonType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsNatPersonType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arbeitslos" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsArbeitslosCodeType" minOccurs="0"/&gt;
 *         &lt;element name="aufenthaltsbew" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsAufenthaltsbewCodeType" minOccurs="0"/&gt;
 *         &lt;element name="aufenthaltsbewBis" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="auslaenderstatus" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsAuslaenderstatusIdType" minOccurs="0"/&gt;
 *         &lt;element name="bemerkung" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="berufsgruppe" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsBerufsgruppeCodeType" minOccurs="0"/&gt;
 *         &lt;element name="geburtsdatum" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="geschlecht" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsGeschlechtCodeType" minOccurs="0"/&gt;
 *         &lt;element name="heimatort" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="inAusbildung" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsInAusbildungCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ledigname" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="namePraefix" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="nameZusatz" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="nationalitaet" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="sozialhilfe" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsSozialhilfeCodeType" minOccurs="0"/&gt;
 *         &lt;element name="titel" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsTitelCodeType" minOccurs="0"/&gt;
 *         &lt;element name="todesdatum" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="unterstPflicht" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsUnterstPflichtCodeType" minOccurs="0"/&gt;
 *         &lt;element name="vorname" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="weitereVornamen" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="zivilstand" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsZivilstandCodeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsNatPersonType", propOrder = {
    "arbeitslos",
    "aufenthaltsbew",
    "aufenthaltsbewBis",
    "auslaenderstatus",
    "bemerkung",
    "berufsgruppe",
    "geburtsdatum",
    "geschlecht",
    "heimatort",
    "inAusbildung",
    "ledigname",
    "namePraefix",
    "nameZusatz",
    "nationalitaet",
    "sozialhilfe",
    "titel",
    "todesdatum",
    "unterstPflicht",
    "vorname",
    "weitereVornamen",
    "zivilstand"
})
public class WsNatPersonType
    extends WsPartnerType
{

    protected WsArbeitslosCodeType arbeitslos;
    protected WsAufenthaltsbewCodeType aufenthaltsbew;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar aufenthaltsbewBis;
    protected WsAuslaenderstatusIdType auslaenderstatus;
    protected String bemerkung;
    protected WsBerufsgruppeCodeType berufsgruppe;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar geburtsdatum;
    protected WsGeschlechtCodeType geschlecht;
    protected String heimatort;
    protected WsInAusbildungCodeType inAusbildung;
    protected String ledigname;
    protected String namePraefix;
    protected String nameZusatz;
    protected String nationalitaet;
    protected WsSozialhilfeCodeType sozialhilfe;
    protected WsTitelCodeType titel;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar todesdatum;
    protected WsUnterstPflichtCodeType unterstPflicht;
    protected String vorname;
    protected String weitereVornamen;
    protected WsZivilstandCodeType zivilstand;

    /**
     * Gets the value of the arbeitslos property.
     * 
     * @return
     *     possible object is
     *     {@link WsArbeitslosCodeType }
     *     
     */
    public WsArbeitslosCodeType getArbeitslos() {
        return arbeitslos;
    }

    /**
     * Sets the value of the arbeitslos property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsArbeitslosCodeType }
     *     
     */
    public void setArbeitslos(WsArbeitslosCodeType value) {
        this.arbeitslos = value;
    }

    /**
     * Gets the value of the aufenthaltsbew property.
     * 
     * @return
     *     possible object is
     *     {@link WsAufenthaltsbewCodeType }
     *     
     */
    public WsAufenthaltsbewCodeType getAufenthaltsbew() {
        return aufenthaltsbew;
    }

    /**
     * Sets the value of the aufenthaltsbew property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAufenthaltsbewCodeType }
     *     
     */
    public void setAufenthaltsbew(WsAufenthaltsbewCodeType value) {
        this.aufenthaltsbew = value;
    }

    /**
     * Gets the value of the aufenthaltsbewBis property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAufenthaltsbewBis() {
        return aufenthaltsbewBis;
    }

    /**
     * Sets the value of the aufenthaltsbewBis property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAufenthaltsbewBis(XMLGregorianCalendar value) {
        this.aufenthaltsbewBis = value;
    }

    /**
     * Gets the value of the auslaenderstatus property.
     * 
     * @return
     *     possible object is
     *     {@link WsAuslaenderstatusIdType }
     *     
     */
    public WsAuslaenderstatusIdType getAuslaenderstatus() {
        return auslaenderstatus;
    }

    /**
     * Sets the value of the auslaenderstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsAuslaenderstatusIdType }
     *     
     */
    public void setAuslaenderstatus(WsAuslaenderstatusIdType value) {
        this.auslaenderstatus = value;
    }

    /**
     * Gets the value of the bemerkung property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBemerkung() {
        return bemerkung;
    }

    /**
     * Sets the value of the bemerkung property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBemerkung(String value) {
        this.bemerkung = value;
    }

    /**
     * Gets the value of the berufsgruppe property.
     * 
     * @return
     *     possible object is
     *     {@link WsBerufsgruppeCodeType }
     *     
     */
    public WsBerufsgruppeCodeType getBerufsgruppe() {
        return berufsgruppe;
    }

    /**
     * Sets the value of the berufsgruppe property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsBerufsgruppeCodeType }
     *     
     */
    public void setBerufsgruppe(WsBerufsgruppeCodeType value) {
        this.berufsgruppe = value;
    }

    /**
     * Gets the value of the geburtsdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGeburtsdatum() {
        return geburtsdatum;
    }

    /**
     * Sets the value of the geburtsdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGeburtsdatum(XMLGregorianCalendar value) {
        this.geburtsdatum = value;
    }

    /**
     * Gets the value of the geschlecht property.
     * 
     * @return
     *     possible object is
     *     {@link WsGeschlechtCodeType }
     *     
     */
    public WsGeschlechtCodeType getGeschlecht() {
        return geschlecht;
    }

    /**
     * Sets the value of the geschlecht property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsGeschlechtCodeType }
     *     
     */
    public void setGeschlecht(WsGeschlechtCodeType value) {
        this.geschlecht = value;
    }

    /**
     * Gets the value of the heimatort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeimatort() {
        return heimatort;
    }

    /**
     * Sets the value of the heimatort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeimatort(String value) {
        this.heimatort = value;
    }

    /**
     * Gets the value of the inAusbildung property.
     * 
     * @return
     *     possible object is
     *     {@link WsInAusbildungCodeType }
     *     
     */
    public WsInAusbildungCodeType getInAusbildung() {
        return inAusbildung;
    }

    /**
     * Sets the value of the inAusbildung property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsInAusbildungCodeType }
     *     
     */
    public void setInAusbildung(WsInAusbildungCodeType value) {
        this.inAusbildung = value;
    }

    /**
     * Gets the value of the ledigname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLedigname() {
        return ledigname;
    }

    /**
     * Sets the value of the ledigname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLedigname(String value) {
        this.ledigname = value;
    }

    /**
     * Gets the value of the namePraefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNamePraefix() {
        return namePraefix;
    }

    /**
     * Sets the value of the namePraefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNamePraefix(String value) {
        this.namePraefix = value;
    }

    /**
     * Gets the value of the nameZusatz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameZusatz() {
        return nameZusatz;
    }

    /**
     * Sets the value of the nameZusatz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameZusatz(String value) {
        this.nameZusatz = value;
    }

    /**
     * Gets the value of the nationalitaet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalitaet() {
        return nationalitaet;
    }

    /**
     * Sets the value of the nationalitaet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalitaet(String value) {
        this.nationalitaet = value;
    }

    /**
     * Gets the value of the sozialhilfe property.
     * 
     * @return
     *     possible object is
     *     {@link WsSozialhilfeCodeType }
     *     
     */
    public WsSozialhilfeCodeType getSozialhilfe() {
        return sozialhilfe;
    }

    /**
     * Sets the value of the sozialhilfe property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsSozialhilfeCodeType }
     *     
     */
    public void setSozialhilfe(WsSozialhilfeCodeType value) {
        this.sozialhilfe = value;
    }

    /**
     * Gets the value of the titel property.
     * 
     * @return
     *     possible object is
     *     {@link WsTitelCodeType }
     *     
     */
    public WsTitelCodeType getTitel() {
        return titel;
    }

    /**
     * Sets the value of the titel property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsTitelCodeType }
     *     
     */
    public void setTitel(WsTitelCodeType value) {
        this.titel = value;
    }

    /**
     * Gets the value of the todesdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTodesdatum() {
        return todesdatum;
    }

    /**
     * Sets the value of the todesdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTodesdatum(XMLGregorianCalendar value) {
        this.todesdatum = value;
    }

    /**
     * Gets the value of the unterstPflicht property.
     * 
     * @return
     *     possible object is
     *     {@link WsUnterstPflichtCodeType }
     *     
     */
    public WsUnterstPflichtCodeType getUnterstPflicht() {
        return unterstPflicht;
    }

    /**
     * Sets the value of the unterstPflicht property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsUnterstPflichtCodeType }
     *     
     */
    public void setUnterstPflicht(WsUnterstPflichtCodeType value) {
        this.unterstPflicht = value;
    }

    /**
     * Gets the value of the vorname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Sets the value of the vorname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVorname(String value) {
        this.vorname = value;
    }

    /**
     * Gets the value of the weitereVornamen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeitereVornamen() {
        return weitereVornamen;
    }

    /**
     * Sets the value of the weitereVornamen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeitereVornamen(String value) {
        this.weitereVornamen = value;
    }

    /**
     * Gets the value of the zivilstand property.
     * 
     * @return
     *     possible object is
     *     {@link WsZivilstandCodeType }
     *     
     */
    public WsZivilstandCodeType getZivilstand() {
        return zivilstand;
    }

    /**
     * Sets the value of the zivilstand property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsZivilstandCodeType }
     *     
     */
    public void setZivilstand(WsZivilstandCodeType value) {
        this.zivilstand = value;
    }

}
