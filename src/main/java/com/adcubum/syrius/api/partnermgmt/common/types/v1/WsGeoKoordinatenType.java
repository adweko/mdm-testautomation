
package com.adcubum.syrius.api.partnermgmt.common.types.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Geographische
 * 				Koordinaten im Format WGS84.
 * 			
 * 
 * <p>Java class for WsGeoKoordinatenType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsGeoKoordinatenType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="breitengrad" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}DecimalType"/&gt;
 *         &lt;element name="laengengrad" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}DecimalType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsGeoKoordinatenType", propOrder = {
    "breitengrad",
    "laengengrad"
})
public class WsGeoKoordinatenType {

    @XmlElement(required = true)
    protected BigDecimal breitengrad;
    @XmlElement(required = true)
    protected BigDecimal laengengrad;

    /**
     * Gets the value of the breitengrad property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBreitengrad() {
        return breitengrad;
    }

    /**
     * Sets the value of the breitengrad property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBreitengrad(BigDecimal value) {
        this.breitengrad = value;
    }

    /**
     * Gets the value of the laengengrad property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLaengengrad() {
        return laengengrad;
    }

    /**
     * Sets the value of the laengengrad property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLaengengrad(BigDecimal value) {
        this.laengengrad = value;
    }

}
