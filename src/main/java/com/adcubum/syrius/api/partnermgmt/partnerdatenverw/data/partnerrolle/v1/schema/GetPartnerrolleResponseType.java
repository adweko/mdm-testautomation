
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Response, in dem die geladenen Partnerrollen enthalten sind.
 * 
 * <p>Java class for GetPartnerrolleResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPartnerrolleResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partnerrolle" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:data:partnerrolle:v1:schema}WsPartnerrolleType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPartnerrolleResponseType", propOrder = {
    "partnerrolle"
})
public class GetPartnerrolleResponseType {

    @XmlElement(required = true)
    protected List<WsPartnerrolleType> partnerrolle;

    /**
     * Gets the value of the partnerrolle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partnerrolle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartnerrolle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsPartnerrolleType }
     * 
     * 
     */
    public List<WsPartnerrolleType> getPartnerrolle() {
        if (partnerrolle == null) {
            partnerrolle = new ArrayList<WsPartnerrolleType>();
        }
        return this.partnerrolle;
    }

}
