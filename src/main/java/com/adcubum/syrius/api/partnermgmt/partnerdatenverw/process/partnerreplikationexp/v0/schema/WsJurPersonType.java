
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsBrancheCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsKonzernpartnerCodeType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsRechtsformCodeType;


/**
 * Enthält die spezifischen Daten zu einer juristischen Person.
 * 
 * <p>Java class for WsJurPersonType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsJurPersonType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aufloesungsdatum" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="branche" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsBrancheCodeType" minOccurs="0"/&gt;
 *         &lt;element name="gruendungsdatum" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="konzernpartner" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsKonzernpartnerCodeType" minOccurs="0"/&gt;
 *         &lt;element name="rechtsform" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsRechtsformCodeType" minOccurs="0"/&gt;
 *         &lt;element name="zusatz1" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="zusatz2" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="nogaCode" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsJurPersonType", propOrder = {
    "aufloesungsdatum",
    "branche",
    "gruendungsdatum",
    "konzernpartner",
    "rechtsform",
    "zusatz1",
    "zusatz2",
    "nogaCode"
})
public class WsJurPersonType
    extends WsPartnerType
{

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar aufloesungsdatum;
    protected WsBrancheCodeType branche;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gruendungsdatum;
    protected WsKonzernpartnerCodeType konzernpartner;
    protected WsRechtsformCodeType rechtsform;
    protected String zusatz1;
    protected String zusatz2;
    protected String nogaCode;

    /**
     * Gets the value of the aufloesungsdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAufloesungsdatum() {
        return aufloesungsdatum;
    }

    /**
     * Sets the value of the aufloesungsdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAufloesungsdatum(XMLGregorianCalendar value) {
        this.aufloesungsdatum = value;
    }

    /**
     * Gets the value of the branche property.
     * 
     * @return
     *     possible object is
     *     {@link WsBrancheCodeType }
     *     
     */
    public WsBrancheCodeType getBranche() {
        return branche;
    }

    /**
     * Sets the value of the branche property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsBrancheCodeType }
     *     
     */
    public void setBranche(WsBrancheCodeType value) {
        this.branche = value;
    }

    /**
     * Gets the value of the gruendungsdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGruendungsdatum() {
        return gruendungsdatum;
    }

    /**
     * Sets the value of the gruendungsdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGruendungsdatum(XMLGregorianCalendar value) {
        this.gruendungsdatum = value;
    }

    /**
     * Gets the value of the konzernpartner property.
     * 
     * @return
     *     possible object is
     *     {@link WsKonzernpartnerCodeType }
     *     
     */
    public WsKonzernpartnerCodeType getKonzernpartner() {
        return konzernpartner;
    }

    /**
     * Sets the value of the konzernpartner property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsKonzernpartnerCodeType }
     *     
     */
    public void setKonzernpartner(WsKonzernpartnerCodeType value) {
        this.konzernpartner = value;
    }

    /**
     * Gets the value of the rechtsform property.
     * 
     * @return
     *     possible object is
     *     {@link WsRechtsformCodeType }
     *     
     */
    public WsRechtsformCodeType getRechtsform() {
        return rechtsform;
    }

    /**
     * Sets the value of the rechtsform property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsRechtsformCodeType }
     *     
     */
    public void setRechtsform(WsRechtsformCodeType value) {
        this.rechtsform = value;
    }

    /**
     * Gets the value of the zusatz1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz1() {
        return zusatz1;
    }

    /**
     * Sets the value of the zusatz1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz1(String value) {
        this.zusatz1 = value;
    }

    /**
     * Gets the value of the zusatz2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZusatz2() {
        return zusatz2;
    }

    /**
     * Sets the value of the zusatz2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZusatz2(String value) {
        this.zusatz2 = value;
    }

    /**
     * Gets the value of the nogaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNogaCode() {
        return nogaCode;
    }

    /**
     * Sets the value of the nogaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNogaCode(String value) {
        this.nogaCode = value;
    }

}
