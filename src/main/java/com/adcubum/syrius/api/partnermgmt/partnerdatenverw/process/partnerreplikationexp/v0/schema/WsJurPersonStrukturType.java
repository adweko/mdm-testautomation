
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Alle Fachlichen States einer juristischen Person inkl. Partneridentifikatoren und OeZuteilungen.
 * 
 * <p>Java class for WsJurPersonStrukturType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsJurPersonStrukturType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="gueltAb" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="gueltBis" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="externeVersion" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LongType" minOccurs="0"/&gt;
 *         &lt;element name="jurPersonStates" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsJurPersonType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="partneridentifikatoren" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartneridentifikatorType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="oeZuteilungen" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsOeZuteilungStrukturType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="kommVerbVerwendungen" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsKommVerbVerwendungType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsJurPersonStrukturType", propOrder = {
    "gueltAb",
    "gueltBis",
    "externeVersion",
    "jurPersonStates",
    "partneridentifikatoren",
    "oeZuteilungen",
    "kommVerbVerwendungen"
})
public class WsJurPersonStrukturType {

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltAb;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltBis;
    protected Long externeVersion;
    protected List<WsJurPersonType> jurPersonStates;
    protected List<WsPartneridentifikatorType> partneridentifikatoren;
    protected List<WsOeZuteilungStrukturType> oeZuteilungen;
    protected List<WsKommVerbVerwendungType> kommVerbVerwendungen;

    /**
     * Gets the value of the gueltAb property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltAb() {
        return gueltAb;
    }

    /**
     * Sets the value of the gueltAb property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltAb(XMLGregorianCalendar value) {
        this.gueltAb = value;
    }

    /**
     * Gets the value of the gueltBis property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltBis() {
        return gueltBis;
    }

    /**
     * Sets the value of the gueltBis property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltBis(XMLGregorianCalendar value) {
        this.gueltBis = value;
    }

    /**
     * Gets the value of the externeVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExterneVersion() {
        return externeVersion;
    }

    /**
     * Sets the value of the externeVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExterneVersion(Long value) {
        this.externeVersion = value;
    }

    /**
     * Gets the value of the jurPersonStates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the jurPersonStates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getJurPersonStates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsJurPersonType }
     * 
     * 
     */
    public List<WsJurPersonType> getJurPersonStates() {
        if (jurPersonStates == null) {
            jurPersonStates = new ArrayList<WsJurPersonType>();
        }
        return this.jurPersonStates;
    }

    /**
     * Gets the value of the partneridentifikatoren property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partneridentifikatoren property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartneridentifikatoren().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsPartneridentifikatorType }
     * 
     * 
     */
    public List<WsPartneridentifikatorType> getPartneridentifikatoren() {
        if (partneridentifikatoren == null) {
            partneridentifikatoren = new ArrayList<WsPartneridentifikatorType>();
        }
        return this.partneridentifikatoren;
    }

    /**
     * Gets the value of the oeZuteilungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the oeZuteilungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOeZuteilungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsOeZuteilungStrukturType }
     * 
     * 
     */
    public List<WsOeZuteilungStrukturType> getOeZuteilungen() {
        if (oeZuteilungen == null) {
            oeZuteilungen = new ArrayList<WsOeZuteilungStrukturType>();
        }
        return this.oeZuteilungen;
    }

    /**
     * Gets the value of the kommVerbVerwendungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the kommVerbVerwendungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKommVerbVerwendungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsKommVerbVerwendungType }
     * 
     * 
     */
    public List<WsKommVerbVerwendungType> getKommVerbVerwendungen() {
        if (kommVerbVerwendungen == null) {
            kommVerbVerwendungen = new ArrayList<WsKommVerbVerwendungType>();
        }
        return this.kommVerbVerwendungen;
    }

}
