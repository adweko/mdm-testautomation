
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsMutationsgrundIdType;


/**
 * Enthält die spezifischen Daten zu einer Telefon-Nummer.
 * 
 * <p>Java class for WsTelefonType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsTelefonType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="stateFrom" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType"/&gt;
 *         &lt;element name="stateUpto" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="telefonNummer" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="isStandard" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="isDefStandard" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="mutationsgrundId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsMutationsgrundIdType"/&gt;
 *         &lt;element name="aenderungDurch" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungAm" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}OffsetDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsTelefonType", propOrder = {
    "stateFrom",
    "stateUpto",
    "telefonNummer",
    "isStandard",
    "isDefStandard",
    "mutationsgrundId",
    "aenderungDurch",
    "aenderungAm"
})
public class WsTelefonType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stateFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stateUpto;
    @XmlElement(required = true)
    protected String telefonNummer;
    protected Integer isStandard;
    protected Integer isDefStandard;
    @XmlElement(required = true)
    protected WsMutationsgrundIdType mutationsgrundId;
    protected String aenderungDurch;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aenderungAm;

    /**
     * Gets the value of the stateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStateFrom() {
        return stateFrom;
    }

    /**
     * Sets the value of the stateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStateFrom(XMLGregorianCalendar value) {
        this.stateFrom = value;
    }

    /**
     * Gets the value of the stateUpto property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStateUpto() {
        return stateUpto;
    }

    /**
     * Sets the value of the stateUpto property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStateUpto(XMLGregorianCalendar value) {
        this.stateUpto = value;
    }

    /**
     * Gets the value of the telefonNummer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefonNummer() {
        return telefonNummer;
    }

    /**
     * Sets the value of the telefonNummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefonNummer(String value) {
        this.telefonNummer = value;
    }

    /**
     * Gets the value of the isStandard property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsStandard() {
        return isStandard;
    }

    /**
     * Sets the value of the isStandard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsStandard(Integer value) {
        this.isStandard = value;
    }

    /**
     * Gets the value of the isDefStandard property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsDefStandard() {
        return isDefStandard;
    }

    /**
     * Sets the value of the isDefStandard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsDefStandard(Integer value) {
        this.isDefStandard = value;
    }

    /**
     * Gets the value of the mutationsgrundId property.
     * 
     * @return
     *     possible object is
     *     {@link WsMutationsgrundIdType }
     *     
     */
    public WsMutationsgrundIdType getMutationsgrundId() {
        return mutationsgrundId;
    }

    /**
     * Sets the value of the mutationsgrundId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsMutationsgrundIdType }
     *     
     */
    public void setMutationsgrundId(WsMutationsgrundIdType value) {
        this.mutationsgrundId = value;
    }

    /**
     * Gets the value of the aenderungDurch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAenderungDurch() {
        return aenderungDurch;
    }

    /**
     * Sets the value of the aenderungDurch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAenderungDurch(String value) {
        this.aenderungDurch = value;
    }

    /**
     * Gets the value of the aenderungAm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAenderungAm() {
        return aenderungAm;
    }

    /**
     * Sets the value of the aenderungAm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAenderungAm(XMLGregorianCalendar value) {
        this.aenderungAm = value;
    }

}
