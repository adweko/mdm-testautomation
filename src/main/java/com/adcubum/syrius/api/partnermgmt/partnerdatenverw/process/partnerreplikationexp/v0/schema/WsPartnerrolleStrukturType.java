
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerrolleDefIdType;


/**
 * Partnerrolle eines Partners.
 * 
 * <p>Java class for WsPartnerrolleStrukturType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPartnerrolleStrukturType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="gueltAb" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="gueltBis" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="externeVersion" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LongType" minOccurs="0"/&gt;
 *         &lt;element name="partnerrolleDefId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsPartnerrolleDefIdType"/&gt;
 *         &lt;element name="partnerrolleStates" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsPartnerrolleType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="aenderungDurch" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungAm" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}OffsetDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPartnerrolleStrukturType", propOrder = {
    "gueltAb",
    "gueltBis",
    "externeVersion",
    "partnerrolleDefId",
    "partnerrolleStates",
    "aenderungDurch",
    "aenderungAm"
})
public class WsPartnerrolleStrukturType {

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltAb;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltBis;
    protected Long externeVersion;
    @XmlElement(required = true)
    protected WsPartnerrolleDefIdType partnerrolleDefId;
    protected List<WsPartnerrolleType> partnerrolleStates;
    protected String aenderungDurch;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aenderungAm;

    /**
     * Gets the value of the gueltAb property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltAb() {
        return gueltAb;
    }

    /**
     * Sets the value of the gueltAb property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltAb(XMLGregorianCalendar value) {
        this.gueltAb = value;
    }

    /**
     * Gets the value of the gueltBis property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltBis() {
        return gueltBis;
    }

    /**
     * Sets the value of the gueltBis property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltBis(XMLGregorianCalendar value) {
        this.gueltBis = value;
    }

    /**
     * Gets the value of the externeVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExterneVersion() {
        return externeVersion;
    }

    /**
     * Sets the value of the externeVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExterneVersion(Long value) {
        this.externeVersion = value;
    }

    /**
     * Gets the value of the partnerrolleDefId property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerrolleDefIdType }
     *     
     */
    public WsPartnerrolleDefIdType getPartnerrolleDefId() {
        return partnerrolleDefId;
    }

    /**
     * Sets the value of the partnerrolleDefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerrolleDefIdType }
     *     
     */
    public void setPartnerrolleDefId(WsPartnerrolleDefIdType value) {
        this.partnerrolleDefId = value;
    }

    /**
     * Gets the value of the partnerrolleStates property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partnerrolleStates property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartnerrolleStates().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsPartnerrolleType }
     * 
     * 
     */
    public List<WsPartnerrolleType> getPartnerrolleStates() {
        if (partnerrolleStates == null) {
            partnerrolleStates = new ArrayList<WsPartnerrolleType>();
        }
        return this.partnerrolleStates;
    }

    /**
     * Gets the value of the aenderungDurch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAenderungDurch() {
        return aenderungDurch;
    }

    /**
     * Sets the value of the aenderungDurch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAenderungDurch(String value) {
        this.aenderungDurch = value;
    }

    /**
     * Gets the value of the aenderungAm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAenderungAm() {
        return aenderungAm;
    }

    /**
     * Sets the value of the aenderungAm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAenderungAm(XMLGregorianCalendar value) {
        this.aenderungAm = value;
    }

}
