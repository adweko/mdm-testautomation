
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsVipCodeType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsBenutzerIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsMutationsgrundIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerschutzDefIdType;


/**
 * Enthält die allgemeinen Daten zu einer Person (natürlich oder juristisch).
 * 
 * <p>Java class for WsPartnerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPartnerType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="stateFrom" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType"/&gt;
 *         &lt;element name="stateUpto" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType" minOccurs="0"/&gt;
 *         &lt;element name="intBetreuerId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsBenutzerIdType" minOccurs="0"/&gt;
 *         &lt;element name="mitarbeiterBenutzerId" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="mutationsgrundId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsMutationsgrundIdType"/&gt;
 *         &lt;element name="name" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="partnerschutzDefId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsPartnerschutzDefIdType"/&gt;
 *         &lt;element name="korrespondenzsprache" type="{urn:com:adcubum:syrius:api:common:externalTypes:v1}LanguageTagType"/&gt;
 *         &lt;element name="vip" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsVipCodeType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungDurch" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungAm" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}OffsetDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPartnerType", propOrder = {
    "stateFrom",
    "stateUpto",
    "intBetreuerId",
    "mitarbeiterBenutzerId",
    "mutationsgrundId",
    "name",
    "partnerschutzDefId",
    "korrespondenzsprache",
    "vip",
    "aenderungDurch",
    "aenderungAm"
})
@XmlSeeAlso({
    WsNatPersonType.class,
    WsJurPersonType.class
})
public abstract class WsPartnerType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stateFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stateUpto;
    protected WsBenutzerIdType intBetreuerId;
    protected String mitarbeiterBenutzerId;
    @XmlElement(required = true)
    protected WsMutationsgrundIdType mutationsgrundId;
    protected String name;
    @XmlElement(required = true)
    protected WsPartnerschutzDefIdType partnerschutzDefId;
    @XmlElement(required = true)
    protected String korrespondenzsprache;
    protected WsVipCodeType vip;
    protected String aenderungDurch;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aenderungAm;

    /**
     * Gets the value of the stateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStateFrom() {
        return stateFrom;
    }

    /**
     * Sets the value of the stateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStateFrom(XMLGregorianCalendar value) {
        this.stateFrom = value;
    }

    /**
     * Gets the value of the stateUpto property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStateUpto() {
        return stateUpto;
    }

    /**
     * Sets the value of the stateUpto property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStateUpto(XMLGregorianCalendar value) {
        this.stateUpto = value;
    }

    /**
     * Gets the value of the intBetreuerId property.
     * 
     * @return
     *     possible object is
     *     {@link WsBenutzerIdType }
     *     
     */
    public WsBenutzerIdType getIntBetreuerId() {
        return intBetreuerId;
    }

    /**
     * Sets the value of the intBetreuerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsBenutzerIdType }
     *     
     */
    public void setIntBetreuerId(WsBenutzerIdType value) {
        this.intBetreuerId = value;
    }

    /**
     * Gets the value of the mitarbeiterBenutzerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMitarbeiterBenutzerId() {
        return mitarbeiterBenutzerId;
    }

    /**
     * Sets the value of the mitarbeiterBenutzerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMitarbeiterBenutzerId(String value) {
        this.mitarbeiterBenutzerId = value;
    }

    /**
     * Gets the value of the mutationsgrundId property.
     * 
     * @return
     *     possible object is
     *     {@link WsMutationsgrundIdType }
     *     
     */
    public WsMutationsgrundIdType getMutationsgrundId() {
        return mutationsgrundId;
    }

    /**
     * Sets the value of the mutationsgrundId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsMutationsgrundIdType }
     *     
     */
    public void setMutationsgrundId(WsMutationsgrundIdType value) {
        this.mutationsgrundId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the partnerschutzDefId property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartnerschutzDefIdType }
     *     
     */
    public WsPartnerschutzDefIdType getPartnerschutzDefId() {
        return partnerschutzDefId;
    }

    /**
     * Sets the value of the partnerschutzDefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartnerschutzDefIdType }
     *     
     */
    public void setPartnerschutzDefId(WsPartnerschutzDefIdType value) {
        this.partnerschutzDefId = value;
    }

    /**
     * Gets the value of the korrespondenzsprache property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKorrespondenzsprache() {
        return korrespondenzsprache;
    }

    /**
     * Sets the value of the korrespondenzsprache property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKorrespondenzsprache(String value) {
        this.korrespondenzsprache = value;
    }

    /**
     * Gets the value of the vip property.
     * 
     * @return
     *     possible object is
     *     {@link WsVipCodeType }
     *     
     */
    public WsVipCodeType getVip() {
        return vip;
    }

    /**
     * Sets the value of the vip property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsVipCodeType }
     *     
     */
    public void setVip(WsVipCodeType value) {
        this.vip = value;
    }

    /**
     * Gets the value of the aenderungDurch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAenderungDurch() {
        return aenderungDurch;
    }

    /**
     * Sets the value of the aenderungDurch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAenderungDurch(String value) {
        this.aenderungDurch = value;
    }

    /**
     * Gets the value of the aenderungAm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAenderungAm() {
        return aenderungAm;
    }

    /**
     * Sets the value of the aenderungAm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAenderungAm(XMLGregorianCalendar value) {
        this.aenderungAm = value;
    }

}
