
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartneridentifikatorDefIdType;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartneridentifikatorIdType;


/**
 * Enthält die Daten eines Partneridentifikators.
 * 
 * <p>Java class for WsPartneridentifikatorType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsPartneridentifikatorType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="externeReferenz" type="{urn:com:adcubum:syrius:api:partnermgmt:partnerdatenverw:process:partnerreplikationexp:v0:schema}WsExterneReferenzType" minOccurs="0"/&gt;
 *         &lt;element name="id" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsPartneridentifikatorIdType" minOccurs="0"/&gt;
 *         &lt;element name="gueltAb" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType"/&gt;
 *         &lt;element name="gueltBis" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}LocalDateType"/&gt;
 *         &lt;element name="partneridentifikatorDefId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsPartneridentifikatorDefIdType"/&gt;
 *         &lt;element name="wert" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="aenderungDurch" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="aenderungAm" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}OffsetDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsPartneridentifikatorType", propOrder = {
    "externeReferenz",
    "id",
    "gueltAb",
    "gueltBis",
    "partneridentifikatorDefId",
    "wert",
    "aenderungDurch",
    "aenderungAm"
})
public class WsPartneridentifikatorType {

    protected WsExterneReferenzType externeReferenz;
    protected WsPartneridentifikatorIdType id;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltAb;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar gueltBis;
    @XmlElement(required = true)
    protected WsPartneridentifikatorDefIdType partneridentifikatorDefId;
    @XmlElement(required = true)
    protected String wert;
    protected String aenderungDurch;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aenderungAm;

    /**
     * Gets the value of the externeReferenz property.
     * 
     * @return
     *     possible object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public WsExterneReferenzType getExterneReferenz() {
        return externeReferenz;
    }

    /**
     * Sets the value of the externeReferenz property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsExterneReferenzType }
     *     
     */
    public void setExterneReferenz(WsExterneReferenzType value) {
        this.externeReferenz = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartneridentifikatorIdType }
     *     
     */
    public WsPartneridentifikatorIdType getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartneridentifikatorIdType }
     *     
     */
    public void setId(WsPartneridentifikatorIdType value) {
        this.id = value;
    }

    /**
     * Gets the value of the gueltAb property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltAb() {
        return gueltAb;
    }

    /**
     * Sets the value of the gueltAb property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltAb(XMLGregorianCalendar value) {
        this.gueltAb = value;
    }

    /**
     * Gets the value of the gueltBis property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltBis() {
        return gueltBis;
    }

    /**
     * Sets the value of the gueltBis property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltBis(XMLGregorianCalendar value) {
        this.gueltBis = value;
    }

    /**
     * Gets the value of the partneridentifikatorDefId property.
     * 
     * @return
     *     possible object is
     *     {@link WsPartneridentifikatorDefIdType }
     *     
     */
    public WsPartneridentifikatorDefIdType getPartneridentifikatorDefId() {
        return partneridentifikatorDefId;
    }

    /**
     * Sets the value of the partneridentifikatorDefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsPartneridentifikatorDefIdType }
     *     
     */
    public void setPartneridentifikatorDefId(WsPartneridentifikatorDefIdType value) {
        this.partneridentifikatorDefId = value;
    }

    /**
     * Gets the value of the wert property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWert() {
        return wert;
    }

    /**
     * Sets the value of the wert property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWert(String value) {
        this.wert = value;
    }

    /**
     * Gets the value of the aenderungDurch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAenderungDurch() {
        return aenderungDurch;
    }

    /**
     * Sets the value of the aenderungDurch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAenderungDurch(String value) {
        this.aenderungDurch = value;
    }

    /**
     * Gets the value of the aenderungAm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAenderungAm() {
        return aenderungAm;
    }

    /**
     * Sets the value of the aenderungAm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAenderungAm(XMLGregorianCalendar value) {
        this.aenderungAm = value;
    }

}
