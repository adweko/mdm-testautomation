
package com.adcubum.syrius.api.partnermgmt.common.types.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WsOrganisationseinheitArtType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WsOrganisationseinheitArtType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="admin"/&gt;
 *     &lt;enumeration value="inexkasso"/&gt;
 *     &lt;enumeration value="leistung"/&gt;
 *     &lt;enumeration value="vertrieb"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "WsOrganisationseinheitArtType")
@XmlEnum
public enum WsOrganisationseinheitArtType {

    @XmlEnumValue("admin")
    ADMIN("admin"),
    @XmlEnumValue("inexkasso")
    INEXKASSO("inexkasso"),
    @XmlEnumValue("leistung")
    LEISTUNG("leistung"),
    @XmlEnumValue("vertrieb")
    VERTRIEB("vertrieb");
    private final String value;

    WsOrganisationseinheitArtType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WsOrganisationseinheitArtType fromValue(String v) {
        for (WsOrganisationseinheitArtType c: WsOrganisationseinheitArtType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
