
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.WsSystemCodeType;


/**
 * Referenz auf ein externes Objekt eines Umsystems.
 * 
 * <p>Java class for WsExterneReferenzType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsExterneReferenzType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{urn:com:adcubum:syrius:api:common:simpleTypes:v1}StringType"/&gt;
 *         &lt;element name="system" type="{urn:com:adcubum:syrius:api:partnermgmt:common:codes:v1}WsSystemCodeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsExterneReferenzType", propOrder = {
    "id",
    "system"
})
public class WsExterneReferenzType {

    @XmlElement(required = true)
    protected String id;
    protected WsSystemCodeType system;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the system property.
     * 
     * @return
     *     possible object is
     *     {@link WsSystemCodeType }
     *     
     */
    public WsSystemCodeType getSystem() {
        return system;
    }

    /**
     * Sets the value of the system property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsSystemCodeType }
     *     
     */
    public void setSystem(WsSystemCodeType value) {
        this.system = value;
    }

}
