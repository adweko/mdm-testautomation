
package com.adcubum.syrius.api.partnermgmt.common.codes.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.adcubum.syrius.api.common.codes.v1.WsCodeType;


/**
 * Kanton der Gemeinde.
 * 
 * <p>Java class for WsGemeindeKantonCodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WsGemeindeKantonCodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:com:adcubum:syrius:api:common:codes:v1}WsCodeType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WsGemeindeKantonCodeType")
public class WsGemeindeKantonCodeType
    extends WsCodeType
{


}
