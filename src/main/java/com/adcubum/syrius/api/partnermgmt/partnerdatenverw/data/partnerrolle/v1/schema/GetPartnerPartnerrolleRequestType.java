
package com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.schema;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.WsPartnerIdType;


/**
 * Request, mit dem die Partnerrollen von bestimmten Partnern geladen werden.
 * 
 * <p>Java class for GetPartnerPartnerrolleRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPartnerPartnerrolleRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partnerId" type="{urn:com:adcubum:syrius:api:partnermgmt:common:identifier:v1}WsPartnerIdType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="stichtag" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPartnerPartnerrolleRequestType", propOrder = {
    "partnerId",
    "stichtag"
})
public class GetPartnerPartnerrolleRequestType {

    @XmlElement(required = true)
    protected List<WsPartnerIdType> partnerId;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stichtag;

    /**
     * Gets the value of the partnerId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partnerId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartnerId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WsPartnerIdType }
     * 
     * 
     */
    public List<WsPartnerIdType> getPartnerId() {
        if (partnerId == null) {
            partnerId = new ArrayList<WsPartnerIdType>();
        }
        return this.partnerId;
    }

    /**
     * Gets the value of the stichtag property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStichtag() {
        return stichtag;
    }

    /**
     * Sets the value of the stichtag property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStichtag(XMLGregorianCalendar value) {
        this.stichtag = value;
    }

}
