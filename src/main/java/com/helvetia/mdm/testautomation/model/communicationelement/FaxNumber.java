package com.helvetia.mdm.testautomation.model.communicationelement;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Value
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode
public class FaxNumber implements CommunicationElement {

    @Builder.Default
    String faxNumber = "+41 24 1234567";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FaxNumber faxNumber1 = (FaxNumber) o;
        return Objects.equals(getFaxNumber(), faxNumber1.getFaxNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFaxNumber());
    }
}
