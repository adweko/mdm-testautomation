package com.helvetia.mdm.testautomation.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Value
@SuperBuilder(toBuilder = true)
public class Address {

    @Builder.Default
    String ZIPCode = "4001";

    @Builder.Default
    String city = "Basel";

    @Builder.Default
    String regionName = "BS";

    @Builder.Default
    String country = "CH";

    String flat;

    String locationDescription;

    String ZIPCodeExtension;

    @Builder.Default
    String street = "Hutgasse";

    @Builder.Default
    String houseNumber = "1";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(getZIPCode(), address.getZIPCode()) &&
                Objects.equals(getCity(), address.getCity()) &&
                Objects.equals(getRegionName(), address.getRegionName()) &&
                Objects.equals(getCountry(), address.getCountry()) &&
                Objects.equals(getFlat(), address.getFlat()) &&
                Objects.equals(getLocationDescription(), address.getLocationDescription()) &&
                Objects.equals(getZIPCodeExtension(), address.getZIPCodeExtension()) &&
                Objects.equals(getStreet(), address.getStreet()) &&
                Objects.equals(getHouseNumber(), address.getHouseNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getZIPCode(), getCity(), getRegionName(), getCountry(), getFlat(), getLocationDescription(), getZIPCodeExtension(), getStreet(), getHouseNumber());
    }
}
