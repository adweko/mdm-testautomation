package com.helvetia.mdm.testautomation.model.attributes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Language {
    DE("D", "DE"),EN("E", "EN"),FR("F", "FR"), IT("I", "IT");

    private static final Map<String,Language> ALIAS_MAP = new HashMap<>();

    static {
        for (Language language:Language.values()) {
            // ignoring the case by normalizing to uppercase
            ALIAS_MAP.put(language.name().toUpperCase(),language);
            for (String alias:language.aliases)
                ALIAS_MAP.put(alias.toUpperCase(),language);
        }
    }

    static public boolean has(String value) {
        // ignoring the case by normalizing to uppercase
        return ALIAS_MAP.containsKey(value.toUpperCase());
    }

    static public Language fromString(String value) {
        if (value == null) throw new NullPointerException("alias null");
        Language language = ALIAS_MAP.get(value.toUpperCase());
        if (language == null)
            throw new IllegalArgumentException("Not an alias: "+value);
        return language;
    }

    private List<String> aliases;
    Language(String... aliases) {
        this.aliases = Arrays.asList(aliases);
    }

    @Override
    public String toString() {
        return this.aliases.get(0);
    }
}
