package com.helvetia.mdm.testautomation.model;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.helvetia.mdm.testautomation.model.attributes.Language;
import com.helvetia.mdm.testautomation.model.communicationelement.*;
import inboundTrafoZepasNaturalPerson.com.helvetia.custom.dateFromXsDate;
import statePartner.json.Response.RowSchema.Versions.VersionsEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.hasChannelID.hasChannelIDEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasLocationID.hasLocationIDSchema.LocationID.LocationIDSchema.hasAddressLocationID.hasAddressLocationIDSchema.AddressLocationID.AddressLocationIDSchema.hasAddressLocation.hasAddressLocationSchema.AddressLocation.AddressLocationEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.isLegalPerson.isLegalPersonSchema.LegalPerson.LegalPersonEntry;
import statePartner.json.Response.RowSchema.Versions.VersionsSchema.isNaturalPerson.isNaturalPersonSchema.NaturalPerson.NaturalPersonEntry;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Partners {

    public static Partner fromStatePartner(readFullPartnerAllVersionsFromDateResponse state) {
        VersionsEntry newestVersion = getNewestVersion(state).get();
        if (isLegalPersonResponse(newestVersion)) {
            return legalPersonFromState(newestVersion);
        } else if (isNaturalPersonResponse(newestVersion)) {
            return naturalPersonFromState(newestVersion);
        } else {
            throw new RuntimeException(("expected partner to be either legal or natural person, " +
                    "not").concat(state.toString()));
        }
    }

    public static Partner fromChangePartner(changeMdmResponse change) {
        if (isLegalPersonResponse(change)) {
            return legalPersonFromChange(change);
        } else if (isNaturalPersonResponse(change)) {
            return naturalPersonFromChange(change);
        } else {
            throw new RuntimeException(("expected partner to be either legal or natural person, " +
                    "not").concat(change.toString()));
        }
    }

    private static Partner naturalPersonFromChange(changeMdmResponse change) {
        inboundTrafoZepasNaturalPerson.json.Response.RowSchema.isNaturalPerson.isNaturalPersonSchema.NaturalPerson.NaturalPersonEntry naturalPerson = change.getBusinessPartnerID().get(0).getIsNaturalPerson().get(0).getNaturalPerson().get(0);
        Optional<String> name = getAsOptionalNonNullNonBlankString(naturalPerson.getName());
        Optional<String> secondName = getAsOptionalNonNullNonBlankString(naturalPerson.getSecondName());
        Optional<String> thirdName = getAsOptionalNonNullNonBlankString(naturalPerson.getThirdName());
        Optional<String> academicDegree = getAsOptionalNonNullNonBlankString(naturalPerson.getAcademicDegree());
        Optional<String> otherTitle = getAsOptionalNonNullNonBlankString(naturalPerson.getOtherTitle());
        Optional<String> salutation = getAsOptionalNonNullNonBlankString(naturalPerson.getSalutation());
        Optional<String> language = getAsOptionalNonNullNonBlankString(naturalPerson.getLanguage());
        Optional<String> sex = getAsOptionalNonNullNonBlankString(naturalPerson.getSex());
        Optional<LocalDate> dateOfBirth = naturalPerson.getDateOfBirth() != null ? Optional.of(dateFromXsdate(naturalPerson.getDateOfBirth())) : Optional.empty();
        Optional<String> nameAtBirth = getAsOptionalNonNullNonBlankString(naturalPerson.getNameAtBirth());
        Optional<String> employmentStatus = getAsOptionalNonNullNonBlankString(naturalPerson.getEmploymentStatus());
        Optional<String> homeTown = getAsOptionalNonNullNonBlankString(naturalPerson.getHomeTown());
        Optional<String> residencePermit = getAsOptionalNonNullNonBlankString(naturalPerson.getResidencePermit());
        Optional<String> nationality = getAsOptionalNonNullNonBlankString(naturalPerson.getNationality());
        Optional<String> maritalStatus = getAsOptionalNonNullNonBlankString(naturalPerson.getMaritalStatus());
        Optional<String> socialSecurityNumber = getAsOptionalNonNullNonBlankString(naturalPerson.getSocialSecurityNumber());
        Optional<LocalDate> dateOfDeath = naturalPerson.getDateOfDeath() != null ? Optional.of(dateFromXsdate(naturalPerson.getDateOfDeath())) : Optional.empty();

        List<inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry> channels = change.getBusinessPartnerID().get(0).getHasChannelID();
        List<CommunicationElement> communicationElements = channels.stream()
                .map(Partners::communicationElementFromChannel)
                .flatMap(e -> e.stream().flatMap(Stream::of))
                .collect(Collectors.toList());

        Optional<Address> mainAddress = getHouseAddress("6", channels);
        Optional<Address> secondAddress = getHouseAddress("7", channels);

        NaturalPerson.NaturalPersonBuilder<?, ?> builder = NaturalPerson.builder();
        name.ifPresent(builder::name);
        secondName.ifPresent(builder::secondName);
        thirdName.ifPresent(builder::thirdName);
        academicDegree.ifPresent(builder::academicDegree);
        otherTitle.ifPresent(builder::otherTitle);
        salutation.ifPresent(builder::salutation);
        language.ifPresent(l -> builder.language(Language.fromString(l)));
        sex.ifPresent(builder::sex);
        dateOfBirth.ifPresent(builder::dateOfBirth);
        nameAtBirth.ifPresent(builder::nameAtBirth);
        employmentStatus.ifPresent(builder::employmentStatus);
        homeTown.ifPresent(builder::homeTown);
        residencePermit.ifPresent(builder::residencePermit);
        nationality.ifPresent(builder::nationality);
        maritalStatus.ifPresent(builder::maritalStatus);
        socialSecurityNumber.ifPresent(builder::socialSecurityNumber);
        dateOfDeath.ifPresent(builder::dateOfDeath);

        mainAddress.ifPresent(builder::mainAddress);
        secondAddress.ifPresent(builder::secondAddress);
        builder.communicationElements(communicationElements);

        return builder.build();


    }


    private static Partner legalPersonFromChange(changeMdmResponse change) {
        inboundTrafoZepasLegalPerson.json.Response.RowSchema.isLegalPerson.isLegalPersonSchema.LegalPerson.LegalPersonEntry legalPerson = change.getBusinessPartnerID().get(0).getIsLegalPerson().get(0).getLegalPerson().get(0);
        Optional<String> language = getAsOptionalNonNullNonBlankString(legalPerson.getLanguage());
        Optional<String> companyName = getAsOptionalNonNullNonBlankString(legalPerson.getCompanyName().toString());
        Optional<String> legalType = getAsOptionalNonNullNonBlankString(legalPerson.getLegalType().toString());
        Optional<String> nogaCode = getAsOptionalNonNullNonBlankString(legalPerson.getNOGACode().toString());

        List<inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry> channels = change.getBusinessPartnerID().get(0).getHasChannelID();
        List<CommunicationElement> communicationElements = channels.stream()
                .map(Partners::communicationElementFromChannel)
                .flatMap(e -> e.stream().flatMap(Stream::of))
                .collect(Collectors.toList());

        Optional<Address> mainAddress = getHouseAddress("6", channels);
        Optional<Address> secondAddress = getHouseAddress("7", channels);

        LegalPerson.LegalPersonBuilder<?, ?> builder = LegalPerson.builder();
        language.ifPresent(l -> builder.language(Language.valueOf(l)));
        companyName.ifPresent(builder::companyName);
        legalType.ifPresent(builder::legalType);
        nogaCode.ifPresent(builder::NOGACode);
        mainAddress.ifPresent(builder::mainAddress);
        secondAddress.ifPresent(builder::secondAddress);
        builder.communicationElements(communicationElements);
        return builder.build();

    }


    private static Optional<String> getAsOptionalNonNullNonBlankString(CharSequence chars) {
        if (chars == null) {
            return Optional.empty();
        }
        String string = chars.toString();
        if (string.isBlank() || string.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(chars.toString());
    }


    private static Partner naturalPersonFromState(VersionsEntry state) {
        NaturalPersonEntry naturalPerson = state.getIsNaturalPerson().get(0).getNaturalPerson().get(0);
        Optional<String> name = getAsOptionalNonNullNonBlankString(naturalPerson.getName());
        Optional<String> secondName = getAsOptionalNonNullNonBlankString(naturalPerson.getSecondName());
        Optional<String> thirdName = getAsOptionalNonNullNonBlankString(naturalPerson.getThirdName());
        Optional<String> academicDegree = getAsOptionalNonNullNonBlankString(naturalPerson.getAcademicDegree());
        Optional<String> otherTitle = getAsOptionalNonNullNonBlankString(naturalPerson.getOtherTitle());
        Optional<String> salutation = getAsOptionalNonNullNonBlankString(naturalPerson.getSalutation());
        Optional<String> language = getAsOptionalNonNullNonBlankString(naturalPerson.getLanguage());
        Optional<String> sex = getAsOptionalNonNullNonBlankString(naturalPerson.getSex());
        Optional<LocalDate> dateOfBirth = naturalPerson.getDateOfBirth() != null ? Optional.of(dateFromXsdate(naturalPerson.getDateOfBirth())) : Optional.empty();
        Optional<String> nameAtBirth = getAsOptionalNonNullNonBlankString(naturalPerson.getNameAtBirth());
        Optional<String> employmentStatus = getAsOptionalNonNullNonBlankString(naturalPerson.getEmploymentStatus());
        Optional<String> homeTown = getAsOptionalNonNullNonBlankString(naturalPerson.getHomeTown());
        Optional<String> residencePermit = getAsOptionalNonNullNonBlankString(naturalPerson.getResidencePermit());
        Optional<String> nationality = getAsOptionalNonNullNonBlankString(naturalPerson.getNationality());
        Optional<String> maritalStatus = getAsOptionalNonNullNonBlankString(naturalPerson.getMaritalStatus());
        Optional<String> socialSecurityNumber = getAsOptionalNonNullNonBlankString(naturalPerson.getSocialSecurityNumber());
        Optional<LocalDate> dateOfDeath = naturalPerson.getDateOfDeath() != null ? Optional.of(dateFromXsdate(naturalPerson.getDateOfDeath())) : Optional.empty();

        List<hasChannelIDEntry> channels = state.getHasChannelID();
        List<CommunicationElement> communicationElements = channels.stream()
                .map(Partners::communicationElementFromChannel)
                .flatMap(e -> e.stream().flatMap(Stream::of))
                .collect(Collectors.toList());

        Optional<Address> mainAddress = getHouseAddress(channels, "6");
        Optional<Address> secondAddress = getHouseAddress(channels, "7");

        NaturalPerson.NaturalPersonBuilder<?, ?> builder = NaturalPerson.builder();
        name.ifPresent(builder::name);
        secondName.ifPresent(builder::secondName);
        thirdName.ifPresent(builder::thirdName);
        academicDegree.ifPresent(builder::academicDegree);
        otherTitle.ifPresent(builder::otherTitle);
        salutation.ifPresent(builder::salutation);
        language.ifPresent(l -> builder.language(Language.fromString(l)));
        sex.ifPresent(builder::sex);
        dateOfBirth.ifPresent(builder::dateOfBirth);
        nameAtBirth.ifPresent(builder::nameAtBirth);
        employmentStatus.ifPresent(builder::employmentStatus);
        homeTown.ifPresent(builder::homeTown);
        residencePermit.ifPresent(builder::residencePermit);
        nationality.ifPresent(builder::nationality);
        maritalStatus.ifPresent(builder::maritalStatus);
        socialSecurityNumber.ifPresent(builder::socialSecurityNumber);
        dateOfDeath.ifPresent(builder::dateOfDeath);

        mainAddress.ifPresent(builder::mainAddress);
        secondAddress.ifPresent(builder::secondAddress);
        builder.communicationElements(communicationElements);

        return builder.build();

    }


    private static Partner legalPersonFromState(VersionsEntry state) {
        LegalPersonEntry legalPerson = state.getIsLegalPerson().get(0).getLegalPerson().get(0);
        Optional<String> language = getAsOptionalNonNullNonBlankString(legalPerson.getLanguage());
        Optional<String> companyName = getAsOptionalNonNullNonBlankString(legalPerson.getCompanyName().toString());
        Optional<String> legalType = getAsOptionalNonNullNonBlankString(legalPerson.getLegalType().toString());
        Optional<String> nogaCode = getAsOptionalNonNullNonBlankString(legalPerson.getNOGACode().toString());

        List<hasChannelIDEntry> channels = state.getHasChannelID();
        List<CommunicationElement> communicationElements = channels.stream()
                .map(Partners::communicationElementFromChannel)
                .flatMap(e -> e.stream().flatMap(Stream::of))
                .collect(Collectors.toList());

        Optional<Address> mainAddress = getHouseAddress(channels, "6");
        Optional<Address> secondAddress = getHouseAddress(channels, "7");

        LegalPerson.LegalPersonBuilder<?, ?> builder = LegalPerson.builder();
        language.ifPresent(l -> builder.language(Language.valueOf(l)));
        companyName.ifPresent(builder::companyName);
        legalType.ifPresent(builder::legalType);
        nogaCode.ifPresent(builder::NOGACode);
        mainAddress.ifPresent(builder::mainAddress);
        secondAddress.ifPresent(builder::secondAddress);
        builder.communicationElements(communicationElements);
        return builder.build();
    }

    private static Optional<Address> getHouseAddress(List<hasChannelIDEntry> channels, String channeltype) {
        return channels.stream()
                .filter(channel -> String.valueOf(channel.getChannelID().get(0).getChannelType()).equals(channeltype))
                .map(channel -> {
                    AddressLocationEntry addressLocationEntry = channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID().get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0).getAddressLocation().get(0);
                    String ZIP = addressLocationEntry.getZIPCode().toString();
                    String city = addressLocationEntry.getCity().toString();
                    String region = null;
                    if (channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasRegion().get(0).getRegion().get(0) != null) {
                        region = String.valueOf(channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasRegion().get(0).getRegion().get(0).getRegionName());
                    }
                    String country = String.valueOf(addressLocationEntry.getCountry());
                    String street = String.valueOf(addressLocationEntry.getStreet());
                    String houseNumber = String.valueOf(addressLocationEntry.getHouseNumber());
                    Address address = Address.builder()
                            .ZIPCode(ZIP)
                            .city(city)
                            .regionName(region)
                            .country(country)
                            .street(street)
                            .houseNumber(houseNumber)
                            .build();
                    return address;
                })
                .findFirst();
    }

    private static Optional<Address> getHouseAddress(String channeltype, List<inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry> channels) {
        return channels.stream()
                .filter(channel -> String.valueOf(channel.getChannelID().get(0).getChannelType()).equals(channeltype))
                .map(channel -> {
                    inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.hasLocationID.hasLocationIDSchema.LocationID.LocationIDSchema.hasAddressLocationID.hasAddressLocationIDSchema.AddressLocationID.AddressLocationIDSchema.hasAddressLocation.hasAddressLocationSchema.AddressLocation.AddressLocationEntry addressLocationEntry = channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasAddressLocationID().get(0).getAddressLocationID().get(0).getHasAddressLocation().get(0).getAddressLocation().get(0);
                    String ZIP = addressLocationEntry.getZIPCode().toString();
                    String city = addressLocationEntry.getCity().toString();
                    String region = null;
                    if (channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasRegion().get(0).getRegion().get(0) != null) {
                        region = String.valueOf(channel.getChannelID().get(0).getHasLocationID().get(0).getLocationID().get(0).getHasRegion().get(0).getRegion().get(0).getRegionName());
                    }
                    String country = String.valueOf(addressLocationEntry.getCountry());
                    String street = String.valueOf(addressLocationEntry.getStreet());
                    String houseNumber = String.valueOf(addressLocationEntry.getHouseNumber());
                    Address address = Address.builder()
                            .ZIPCode(ZIP)
                            .city(city)
                            .regionName(region)
                            .country(country)
                            .street(street)
                            .houseNumber(houseNumber)
                            .build();
                    return address;
                })
                .findFirst();
    }


    private static Optional<CommunicationElement> communicationElementFromChannel(hasChannelIDEntry channel) {
        String channelType = String.valueOf(channel.getChannelID().get(0).getChannelType());
        switch (channelType) {
            case "1":
                return Optional.of(TelephoneNumber.builder()
                        .telephoneNumber(
                                channel.getChannelID().get(0)
                                        .getHasTelephone().get(0)
                                        .getTelephone().get(0)
                                        .getTelephoneNumber().toString()
                        ).build());
            case "2":

                return Optional.of(MobileNumber.builder()
                        .mobileNumber(
                                channel.getChannelID().get(0).
                                        getHasMobile().get(0)
                                        .getMobile().get(0)
                                        .getMobileNumber().toString()
                        ).build());
            case "3":
                return Optional.of(FaxNumber.builder()
                        .faxNumber(
                                channel.getChannelID().get(0)
                                        .getHasFax().get(0)
                                        .getFax().get(0)
                                        .getFaxNumber().toString()
                        ).build());
            case "4":
                return Optional.of(EmailAddress.builder()
                        .emailAddress(
                                channel.getChannelID().get(0)
                                        .getHasEmail().get(0)
                                        .getEmail().get(0)
                                        .getEmailAddress().toString()
                        ).build());

            case "5":
                return Optional.of(SkypeAddress.builder()
                        .skypeAddress(
                                channel.getChannelID().get(0)
                                        .getHasSkype().get(0)
                                        .getSkype().get(0)
                                        .getSkypeAddress().toString()
                        ).build());
            case "6": // main address, fallthrough intended
            case "7": // second address, fallthrough intended
            case "8":
                // postbox address, addresses are irrelevant here, mapped elsewhere
                return Optional.empty();
            default:
                throw new RuntimeException("unknown channel type " + channelType);
        }
    }


    private static Optional<CommunicationElement> communicationElementFromChannel(inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry channel) {
        String channelType = String.valueOf(channel.getChannelID().get(0).getChannelType());
        switch (channelType) {
            case "1":
                return Optional.of(TelephoneNumber.builder()
                        .telephoneNumber(
                                channel.getChannelID().get(0)
                                        .getHasTelephone().get(0)
                                        .getTelephone().get(0)
                                        .getTelephoneNumber().toString()
                        ).build());
            case "2":
                return Optional.of(MobileNumber.builder()
                        .mobileNumber(
                                channel.getChannelID().get(0).
                                        getHasMobile().get(0)
                                        .getMobile().get(0)
                                        .getMobileNumber().toString()
                        ).build());
            case "3":
                return Optional.of(FaxNumber.builder()
                        .faxNumber(
                                channel.getChannelID().get(0)
                                        .getHasFax().get(0)
                                        .getFax().get(0)
                                        .getFaxNumber().toString()
                        ).build());
            case "4":
                return Optional.of(EmailAddress.builder()
                        .emailAddress(
                                channel.getChannelID().get(0)
                                        .getHasEmail().get(0)
                                        .getEmail().get(0)
                                        .getEmailAddress().toString()
                        ).build());

            case "5":
                return Optional.of(SkypeAddress.builder()
                        .skypeAddress(
                                channel.getChannelID().get(0)
                                        .getHasSkype().get(0)
                                        .getSkype().get(0)
                                        .getSkypeAddress().toString()
                        ).build());
            case "6": // main address, fallthrough intended
            case "7": // second address, fallthrough intended
            case "8":
                // postbox address, addresses are irrelevant here, mapped elsewhere
                return Optional.empty();
            default:
                throw new RuntimeException("unknown channel type " + channelType);
        }
    }


    private static Optional<VersionsEntry> getNewestVersion(readFullPartnerAllVersionsFromDateResponse state) {
        return state.getBusinessPartnerID().get(0).getVersions()
                .stream().max(Comparator.comparing(e -> dateFromXsdate(e.getBITEMPValidFrom())));
    }

    private static boolean isLegalPersonResponse(changeMdmResponse change) {
        return change.getBusinessPartnerID().get(0).getIsLegalPerson() != null && !change.getBusinessPartnerID().get(0).getIsLegalPerson().isEmpty();
    }

    private static boolean isLegalPersonResponse(statePartner.json.Response.RowSchema.Versions.VersionsEntry state) {
        return state.getIsLegalPerson() != null && !state.getIsLegalPerson().isEmpty();
    }

    private static boolean isNaturalPersonResponse(changeMdmResponse change) {
        return change.getBusinessPartnerID().get(0).getIsNaturalPerson() != null && !change.getBusinessPartnerID().get(0).getIsNaturalPerson().isEmpty();
    }

    private static boolean isNaturalPersonResponse(statePartner.json.Response.RowSchema.Versions.VersionsEntry state) {
        return state.getIsNaturalPerson() != null && !state.getIsNaturalPerson().isEmpty();
    }

    private static LocalDate dateFromXsdate(statePartner.com.helvetia.custom.dateFromXsDate xsDate) {
        return LocalDate.of(xsDate.getYear(), xsDate.getMonth(), xsDate.getDay());
    }

    private static LocalDate dateFromXsdate(dateFromXsDate xsDate) {
        return LocalDate.of(xsDate.getYear(), xsDate.getMonth(), xsDate.getDay());
    }


}
