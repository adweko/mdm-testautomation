package com.helvetia.mdm.testautomation.model.communicationelement;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Value
@SuperBuilder(toBuilder = true)
public class SkypeAddress implements CommunicationElement {

    @Builder.Default
    String skypeAddress = "mannis.skypename";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SkypeAddress that = (SkypeAddress) o;
        return Objects.equals(getSkypeAddress(), that.getSkypeAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSkypeAddress());
    }
}
