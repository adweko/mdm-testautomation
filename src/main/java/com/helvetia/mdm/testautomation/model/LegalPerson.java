package com.helvetia.mdm.testautomation.model;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Value
@SuperBuilder(toBuilder = true)
public class LegalPerson extends Partner {

    @Builder.Default
    String companyName = "Oettinger Brauerei GmbH";

    @Builder.Default
    String NOGACode = "Abbrucharbeiten - 431100";

    @Builder.Default
    String legalType = "Aktiengesellschaft (AG)";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LegalPerson that = (LegalPerson) o;
        return Objects.equals(getCompanyName(), that.getCompanyName()) &&
                Objects.equals(getNOGACode(), that.getNOGACode()) &&
                Objects.equals(getLegalType(), that.getLegalType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCompanyName(), getNOGACode(), getLegalType());
    }
}
