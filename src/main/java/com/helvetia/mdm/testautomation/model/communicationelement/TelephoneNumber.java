package com.helvetia.mdm.testautomation.model.communicationelement;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Value
@SuperBuilder(toBuilder = true)
public class TelephoneNumber implements CommunicationElement {

    @Builder.Default
    String telephoneNumber = "+41 12 345 6789";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TelephoneNumber that = (TelephoneNumber) o;
        return Objects.equals(getTelephoneNumber(), that.getTelephoneNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTelephoneNumber());
    }
}
