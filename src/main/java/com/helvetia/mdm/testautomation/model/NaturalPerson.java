package com.helvetia.mdm.testautomation.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.Objects;


@Value
@SuperBuilder(toBuilder = true)
public class NaturalPerson extends Partner {

    @Builder.Default
    String surname = "Baxxter";

    @Builder.Default
    String name = "Hans";

    @Builder.Default
    String secondName = "Peter";

    String thirdName;

    String academicDegree;

    String otherTitle;

    @Builder.Default
    String salutation = "01";

    @Builder.Default
    String sex = "1";

    @Builder.Default
    LocalDate dateOfBirth = LocalDate.of(1980, 1, 1);


    String nameAtBirth;

    String employmentStatus;

    String homeTown;

    String residencePermit;

    String nationality;


    String maritalStatus;

    String socialSecurityNumber;

    LocalDate dateOfDeath;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        NaturalPerson that = (NaturalPerson) o;
        return Objects.equals(getSurname(), that.getSurname()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getSecondName(), that.getSecondName()) &&
                Objects.equals(getThirdName(), that.getThirdName()) &&
                Objects.equals(getAcademicDegree(), that.getAcademicDegree()) &&
                Objects.equals(getOtherTitle(), that.getOtherTitle()) &&
                Objects.equals(getSalutation(), that.getSalutation()) &&
                Objects.equals(getSex(), that.getSex()) &&
                Objects.equals(getDateOfBirth(), that.getDateOfBirth()) &&
                Objects.equals(getNameAtBirth(), that.getNameAtBirth()) &&
                Objects.equals(getEmploymentStatus(), that.getEmploymentStatus()) &&
                Objects.equals(getHomeTown(), that.getHomeTown()) &&
                Objects.equals(getResidencePermit(), that.getResidencePermit()) &&
                Objects.equals(getNationality(), that.getNationality()) &&
                Objects.equals(getMaritalStatus(), that.getMaritalStatus()) &&
                Objects.equals(getSocialSecurityNumber(), that.getSocialSecurityNumber()) &&
                Objects.equals(getDateOfDeath(), that.getDateOfDeath());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSurname(), getName(), getSecondName(), getThirdName(), getAcademicDegree(), getOtherTitle(), getSalutation(), getSex(), getDateOfBirth(), getNameAtBirth(), getEmploymentStatus(), getHomeTown(), getResidencePermit(), getNationality(), getMaritalStatus(), getSocialSecurityNumber(), getDateOfDeath());
    }
}
