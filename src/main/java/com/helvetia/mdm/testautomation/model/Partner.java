package com.helvetia.mdm.testautomation.model;

import com.helvetia.mdm.testautomation.model.attributes.Language;
import com.helvetia.mdm.testautomation.model.communicationelement.CommunicationElement;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Getter
@ToString
@SuperBuilder(toBuilder = true)
public abstract class Partner {

    @Builder.Default
    final Language language = Language.DE;

    @Getter(AccessLevel.NONE)
    final List<CommunicationElement> communicationElements;

    @Getter(AccessLevel.NONE)
    @Builder.Default
    final Address mainAddress = Address.builder().build();

    @Getter(AccessLevel.NONE)
    final Address secondAddress;

    public List<CommunicationElement> getCommunicationElements() {
        if (communicationElements == null) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>(communicationElements);
        }
    }

    public Address getMainAddress() {
        return mainAddress.toBuilder().build();
    }

    public Address getSecondAddress() {
        if (secondAddress != null) {
            return secondAddress.toBuilder().build();
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Partner partner = (Partner) o;
        final boolean a = getLanguage() == partner.getLanguage();
        final boolean b = Objects.equals(getCommunicationElements(), partner.getCommunicationElements());
        final boolean c = Objects.equals(getMainAddress(), partner.getMainAddress());
        final boolean d = Objects.equals(getSecondAddress(), partner.getSecondAddress());
        final boolean equal = a && b && c && d;
        return equal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLanguage(), getCommunicationElements(), getMainAddress(), getSecondAddress());
    }
}

//    // ZEPAS:
//
//    // NaturalPerson
//    surname;
//    name;
//    secondName;
//    thirdName;
//    academicDegree;
//    otherTitle;
//    salutation;
//    language;
//    sex;
//    dateOfBirth;
//    nameAtBirth;
//    employmentStatus;
//    homeTown;
//    residencePermit;
//    nationality;
//    maritalStatus;
//    // SYRIUS socialSecurityNumber;
//    // SYRIUS dateOfDeath
//
//
//    // LegalPerson
//    language;
//    companyName;


//    // ZEPAS Personmapper
//    telephoneNumber;
//    mobileNumber;
//    channelNote;
//    faxNumber;
//    emailAddress;
//    skypeAddress;


//    // ZEPAS common address
//    ZIPCode;
//    city;
//    regionName;
//    country;
//    flat;
//    locationDescription;
//    // SYRIUS ZIPCodeExtension
//
//    // mapMdmWohnGeschaeftAdresse
//    // mapMdmZweitsitzAdresse
//    // mapMdmWohnGeschaeftAdresse
//    street;
//    houseNumber;
//
//    // mapMdmPostfachAdresse, additional field to those above
//    postbox;
//
//    // LegalPerson
//    FINMANumber;
//    NOGACode;
//
//    // UnderWriting
//    stopCode;
//    TECHUser;
