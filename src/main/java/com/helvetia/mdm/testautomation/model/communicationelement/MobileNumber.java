package com.helvetia.mdm.testautomation.model.communicationelement;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

@Value
@SuperBuilder(toBuilder = true)
public class MobileNumber implements CommunicationElement {

    @Builder.Default
    String mobileNumber = "+41 33 333 3333";

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MobileNumber that = (MobileNumber) o;
        return Objects.equals(getMobileNumber(), that.getMobileNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMobileNumber());
    }
}
