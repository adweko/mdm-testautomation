package com.helvetia.mdm.testautomation.creation.syrius;

import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.common.codes.v1.*;
import com.adcubum.syrius.api.partnermgmt.common.identifier.v1.*;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.*;
import com.helvetia.app.mdm.ReferenceDataMapper;
import com.helvetia.mdm.testautomation.creation.syrius.config.SyriusConfig;
import com.helvetia.mdm.testautomation.model.LegalPerson;
import com.helvetia.mdm.testautomation.model.NaturalPerson;
import com.helvetia.mdm.testautomation.model.Partner;
import inboundTrafoZepasChannel.json.Response.RowSchema.hasChannelID.hasChannelIDEntry;
import inboundTrafoZepasLegalPerson.json.Response.RowSchema.isLegalPerson.isLegalPersonEntry;
import inboundTrafoZepasNaturalPerson.json.Response.RowSchema.isNaturalPerson.isNaturalPersonSchema.NaturalPerson.NaturalPersonEntry;
import inboundTrafoZepasUnderwriting.json.Response.RowSchema.hasUnderwritingCodeID.hasUnderwritingCodeIDEntry;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PartnerSyrius {

    private static ReferenceDataMapper singleReferenceDataMapper;

    public static ReferenceDataMapper getReferenceDataMapper() throws IOException {
        if (singleReferenceDataMapper == null) {
            singleReferenceDataMapper = new ReferenceDataMapper();
        }
        return singleReferenceDataMapper;
    }

    private static final Logger logger = LoggerFactory.getLogger(PartnerSyrius.class);

    private RepliziereNatPersonRequestType repliziereNatPersonRequestType;
    private RepliziereJurPersonRequestType repliziereJurPersonRequestType;
    private final List<RepliziereChannelRequestWrapper> repliziereChannelRequestWrappers = new ArrayList<>();
    private final List<ReplizierePartnerrolleRequestType> replizierePartnerrolleRequestTypeList = new ArrayList<>();
    private RepliziereDublettenRequestType repliziereDublettenRequestType;

    private void setRepliziereNatPersonRequestType(RepliziereNatPersonRequestType repliziereNatPersonRequestType) {
        this.repliziereNatPersonRequestType = repliziereNatPersonRequestType;
    }

    private void setRepliziereJurPersonRequestType(RepliziereJurPersonRequestType repliziereJurPersonRequestType) {
        this.repliziereJurPersonRequestType = repliziereJurPersonRequestType;
    }

    private void addReplizierePartnerrolleRequestType(ReplizierePartnerrolleRequestType replizierePartnerrolleRequestType) {
        this.replizierePartnerrolleRequestTypeList.add(replizierePartnerrolleRequestType);
    }

    private void addRepliziereChannelRequestWrapper(RepliziereChannelRequestWrapper repliziereChannelRequestWrapper) {
        this.repliziereChannelRequestWrappers.add(repliziereChannelRequestWrapper);
    }

    private void setRepliziereDublettenRequestType(RepliziereDublettenRequestType repliziereDublettenRequestType) {
        this.repliziereDublettenRequestType = repliziereDublettenRequestType;
    }

    public static PartnerSyrius fromPartner(Partner partner) throws Exception {
        PartnerSyrius syriusPartner = new PartnerSyrius();
        if (partner instanceof NaturalPerson) {
            syriusPartner.setRepliziereNatPersonRequestType(natPersonFromPartnerToSyrius((NaturalPerson) partner));
            syriusPartner.setRepliziereJurPersonRequestType(null);
        } else if (partner instanceof LegalPerson) {
            syriusPartner.setRepliziereJurPersonRequestType(jurPersonFromPartnerToSyrius((LegalPerson) partner));
            syriusPartner.setRepliziereNatPersonRequestType(null);
        } else {
            throw new IllegalStateException("Partner is neither Natural nor Juristic Person");
        }

        List<RepliziereChannelRequestWrapper> channels = channelsFromPartnerToSyrius(partner);
        channels.forEach(syriusPartner::addRepliziereChannelRequestWrapper);
        return syriusPartner;
    }

    private static List<RepliziereChannelRequestWrapper> channelsFromPartnerToSyrius(Partner partner) {
        return Collections.emptyList();

    }

    private static RepliziereNatPersonRequestType natPersonFromPartnerToSyrius(NaturalPerson partner) throws IOException, DatatypeConfigurationException {
        RepliziereNatPersonRequestType natPersonRequestType = new RepliziereNatPersonRequestType();
        natPersonRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());

        WsPartnerReferenzType wsPartnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType wsPartnerIdType = new WsPartnerIdType();

        final String partnerId = UUID.randomUUID().toString().toLowerCase();

        final Random random = new Random();
        final int randomExterneReferenz = random.nextInt();

        wsPartnerIdType.setId(partnerId);
        wsPartnerReferenzType.setPartnerId(wsPartnerIdType);
        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
        wsExterneReferenzType.setId(String.valueOf(randomExterneReferenz));
        wsPartnerReferenzType.setExterneReferenz(wsExterneReferenzType);

        WsNatPersonStrukturType wsNatPersonStrukturType = new WsNatPersonStrukturType();
        WsPartneridentifikatorType wsPartneridentifikatorType = new WsPartneridentifikatorType();
        WsPartneridentifikatorDefIdType wsPartneridentifikatorDefIdType = new WsPartneridentifikatorDefIdType();
        wsPartneridentifikatorDefIdType.setId("SYR_Partner_PartnerNr");
        wsPartneridentifikatorType.setPartneridentifikatorDefId(wsPartneridentifikatorDefIdType);
        wsPartneridentifikatorType.setWert(String.valueOf(randomExterneReferenz));
        wsNatPersonStrukturType.getPartneridentifikatoren().add(wsPartneridentifikatorType);

        if (partner.getSocialSecurityNumber() != null) {
            WsPartneridentifikatorType wsPartneridentifikatorType2 = new WsPartneridentifikatorType();
            WsPartneridentifikatorDefIdType wsPartneridentifikatorDefIdType2 = new WsPartneridentifikatorDefIdType();
            wsPartneridentifikatorDefIdType2.setId("SYR_Partner_SozialVersNr");
            wsPartneridentifikatorType2.setPartneridentifikatorDefId(wsPartneridentifikatorDefIdType2);
            wsPartneridentifikatorType2.setWert(partner.getSocialSecurityNumber());
            wsNatPersonStrukturType.getPartneridentifikatoren().add(wsPartneridentifikatorType2);
        }

        WsNatPersonType wsNatPersonType = new WsNatPersonType();

        WsAufenthaltsbewCodeType wsAufenthaltsbewCodeType = new WsAufenthaltsbewCodeType();
        String residencePermit = getReferenceDataMapper().getResidencePermitMapMdmToSyr().get(String.valueOf(partner.getResidencePermit()));
        wsAufenthaltsbewCodeType.setCodeId(residencePermit);
        wsNatPersonType.setAufenthaltsbew(wsAufenthaltsbewCodeType);
        wsNatPersonType.setMitarbeiterBenutzerId(null);

        WsBerufsgruppeCodeType wsBerufsgruppeCodeType = new WsBerufsgruppeCodeType();
        String employmentStatus = getReferenceDataMapper().getEmploymentStatusMapMdmToSyr().get(String.valueOf(partner.getEmploymentStatus()));
        wsBerufsgruppeCodeType.setCodeId(employmentStatus);
        wsNatPersonType.setBerufsgruppe(wsBerufsgruppeCodeType);

        if (partner.getDateOfBirth() != null) {
            final XMLGregorianCalendar dateOfBirth = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            dateOfBirth.setYear(partner.getDateOfBirth().getYear());
            dateOfBirth.setMonth(partner.getDateOfBirth().getMonthValue());
            dateOfBirth.setDay(partner.getDateOfBirth().getDayOfMonth());
            wsNatPersonType.setGeburtsdatum(dateOfBirth);
        }

        WsGeschlechtCodeType wsGeschlechtCodeType = new WsGeschlechtCodeType();
        wsGeschlechtCodeType.setCodeId(getReferenceDataMapper().getSexMapMdmToSyr().get(String.valueOf(partner.getSex())));
        wsNatPersonType.setGeschlecht(wsGeschlechtCodeType);
        wsNatPersonType.setNameZusatz(Objects.toString(partner.getOtherTitle(), null));

        wsNatPersonType.setHeimatort(Objects.toString(partner.getHomeTown(), null));
        wsNatPersonType.setLedigname(Objects.toString(partner.getNameAtBirth(), null));
        wsNatPersonType.setNationalitaet(getReferenceDataMapper().getNationalityMapMdmToSyr().get(String.valueOf(partner.getNationality())));
        WsTitelCodeType wsTitelCodeType = new WsTitelCodeType();

        if (partner.getSalutation().equals("07")) { //Anrede Schwester
            wsTitelCodeType.setCodeId("9f2d1f1e-b3cb-47f0-8d8f-7c6df9d581a2"); // Nonne
        } else {
            wsTitelCodeType.setCodeId(getReferenceDataMapper().getTitleMapMdmToSyr().get(String.valueOf(partner.getAcademicDegree())));
        }
        wsNatPersonType.setTitel(wsTitelCodeType);

        WsInAusbildungCodeType wsInAusbildungCodeType = new WsInAusbildungCodeType();
        wsInAusbildungCodeType.setCodeId("-10111"); //default: no
        wsNatPersonType.setInAusbildung(wsInAusbildungCodeType);

        WsArbeitslosCodeType wsArbeitslosCodeType = new WsArbeitslosCodeType();
        wsArbeitslosCodeType.setCodeId("-10111"); //default no
        wsNatPersonType.setArbeitslos(wsArbeitslosCodeType);

        if (partner.getDateOfDeath() != null) {
            final XMLGregorianCalendar dateOfDeath = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(
                    partner.getDateOfDeath().getYear(), partner.getDateOfDeath().getMonthValue(),
                    partner.getDateOfDeath().getDayOfMonth(), 0);
            wsNatPersonType.setTodesdatum(dateOfDeath);
        }
        wsNatPersonType.setVorname(partner.getName());
        if (partner.getSecondName() != null) {
            if (partner.getThirdName() != null) {
                wsNatPersonType.setWeitereVornamen(partner.getSecondName() + " " + partner.getThirdName());
            } else {
                wsNatPersonType.setWeitereVornamen(partner.getSecondName());
            }
        }
        wsNatPersonType.setName(partner.getSurname());
        WsZivilstandCodeType wsZivilstandCodeType = new WsZivilstandCodeType();
        String maritalStatus = getReferenceDataMapper().getMaritalStatusMapMdmToSyr().get(String.valueOf(partner.getMaritalStatus()));
        wsZivilstandCodeType.setCodeId(maritalStatus);
        wsNatPersonType.setZivilstand(wsZivilstandCodeType);

        String syriusKorrespondenzsprache = getReferenceDataMapper().getLanguageMapMdmToSyr().get(String.valueOf(partner.getLanguage().toString()));
        wsNatPersonType.setKorrespondenzsprache(syriusKorrespondenzsprache);
        wsNatPersonType.setMitarbeiterBenutzerId(null);

        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a"); //specific syrius-boid
        wsNatPersonType.setMutationsgrundId(wsMutationsgrundIdType);

        final XMLGregorianCalendar validFrom = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        final LocalDate now = LocalDate.now();
        validFrom.setYear(now.getYear());
        validFrom.setMonth(now.getMonthValue());
        validFrom.setDay(now.getDayOfMonth());
        wsNatPersonType.setStateFrom(validFrom);

        final XMLGregorianCalendar validTo = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        validTo.setYear(3000);
        validTo.setMonth(1);
        validTo.setDay(1);
        wsNatPersonType.setStateUpto(validTo);


        long epoch = LocalDateTime.now().atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L; //nanoseconds since epoch
        wsNatPersonStrukturType.setExterneVersion(epoch); //needed for processing order in syrius
        wsNatPersonStrukturType.getNatPersonStates().add(wsNatPersonType);
        wsNatPersonStrukturType.setGueltAb(wsNatPersonType.getStateFrom());
        wsNatPersonStrukturType.setGueltBis(wsNatPersonType.getStateUpto());
        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();
        wsErlaubnisCodeType.setCodeId("-64830"); // keine Werbung

        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);


        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
        wsErlaubnisartCodeType.setCodeId("-64868"); //Anweisung Kunde
        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
        wsNatPersonStrukturType.getKommVerbVerwendungen().add(wsKommVerbVerwendungType);

        natPersonRequestType.setPartnerReferenz(wsPartnerReferenzType);
        natPersonRequestType.setNatPersonStruktur(wsNatPersonStrukturType);
        return natPersonRequestType;

    }

    private static RepliziereJurPersonRequestType jurPersonFromPartnerToSyrius(LegalPerson partner) throws DatatypeConfigurationException, IOException {
        RepliziereJurPersonRequestType repliziereJurPersonRequestType = new RepliziereJurPersonRequestType();
        WsPartnerReferenzType partnerReferenzType = new WsPartnerReferenzType();
        WsPartnerIdType partnerIdType = new WsPartnerIdType();
        final String partnerId = UUID.randomUUID().toString().toLowerCase();
        partnerIdType.setId(partnerId);
        partnerReferenzType.setPartnerId(partnerIdType);
        repliziereJurPersonRequestType.setMessageId(UUID.randomUUID().toString().toLowerCase());

        WsExterneReferenzType wsExterneReferenzType = new WsExterneReferenzType();
//        wsExterneReferenzType.setId(changeMdmResponse.getBusinessPartnerID().get(0).getHasIdentificationNumber().get(0).getIdentificationNumber().get(0).getIDNumber().toString());
//        partnerReferenzType.setExterneReferenz(wsExterneReferenzType);
        repliziereJurPersonRequestType.setPartnerReferenz(partnerReferenzType);

        WsJurPersonStrukturType wsJurPersonStrukturType = new WsJurPersonStrukturType();
        WsJurPersonType wsJurPersonType = new WsJurPersonType();

        final XMLGregorianCalendar validFrom = DatatypeFactory.newInstance().newXMLGregorianCalendar(Instant.now().toString());
        wsJurPersonType.setStateFrom(validFrom);

        final XMLGregorianCalendar validTo = DatatypeFactory.newInstance().newXMLGregorianCalendar(3000, 1, 1, 0, 0, 0, 0, 0);
        wsJurPersonType.setStateUpto(validTo);

        WsRechtsformCodeType wsRechtsformCodeType = new WsRechtsformCodeType();
        String legalType = partner.getLegalType();
        wsRechtsformCodeType.setCodeId(legalType);
        wsJurPersonType.setRechtsform(wsRechtsformCodeType);
        wsJurPersonType.setNogaCode(Objects.toString(partner.getNOGACode(), null));
        WsPartnerschutzDefIdType wsPartnerschutzDefIdType = new WsPartnerschutzDefIdType();

        String syriusLanguage = getReferenceDataMapper().getLanguageMapMdmToSyr().get(partner.getLanguage().toString());
        wsJurPersonType.setKorrespondenzsprache(syriusLanguage);

        WsMutationsgrundIdType wsMutationsgrundIdType = new WsMutationsgrundIdType();
        wsMutationsgrundIdType.setId("45015770-3e24-4ed0-8519-aacbb8fe454a");
        wsJurPersonType.setMutationsgrundId(wsMutationsgrundIdType);
        wsJurPersonType.setName(partner.getCompanyName());

        wsJurPersonStrukturType.getJurPersonStates().add(wsJurPersonType);

        WsKommVerbVerwendungType wsKommVerbVerwendungType = new WsKommVerbVerwendungType();
        WsKommVerbTypCodeType wsKommVerbTypCodeType = new WsKommVerbTypCodeType();
        wsKommVerbVerwendungType.setVerbindungstyp(wsKommVerbTypCodeType);

        WsErlaubnisCodeType wsErlaubnisCodeType = new WsErlaubnisCodeType();

        wsKommVerbVerwendungType.setErlaubnis(wsErlaubnisCodeType);
        WsErlaubnisartCodeType wsErlaubnisartCodeType = new WsErlaubnisartCodeType();
        wsErlaubnisartCodeType.setCodeId("-64868"); //Anweisung Kunde
        wsKommVerbVerwendungType.setErlaubnisart(wsErlaubnisartCodeType);
        wsJurPersonStrukturType.getKommVerbVerwendungen().add(wsKommVerbVerwendungType);

        WsPartneridentifikatorIdType techMdmPartnerIDAsPartnerIdentifikatorId = new WsPartneridentifikatorIdType();
        techMdmPartnerIDAsPartnerIdentifikatorId.setId(partnerId);

        WsPartneridentifikatorType wsPartneridentifikatorType = new WsPartneridentifikatorType();
        wsPartneridentifikatorType.setId(techMdmPartnerIDAsPartnerIdentifikatorId);
        WsPartneridentifikatorDefIdType wsPartneridentifikatorDefIdType = new WsPartneridentifikatorDefIdType();
        wsPartneridentifikatorDefIdType.setId("SYR_Partner_PartnerNr");
        wsPartneridentifikatorType.setPartneridentifikatorDefId(wsPartneridentifikatorDefIdType);
        wsPartneridentifikatorType.setWert(partnerId);
        wsJurPersonStrukturType.getPartneridentifikatoren().add(wsPartneridentifikatorType);
        wsJurPersonStrukturType.setGueltAb(wsJurPersonType.getStateFrom());
        wsJurPersonStrukturType.setGueltBis(wsJurPersonType.getStateUpto());


        String FinmaNr = "n/a";
        WsPartneridentifikatorType wsPartneridentifikatorType3 = new WsPartneridentifikatorType();
        wsPartneridentifikatorType3.setGueltAb(wsJurPersonType.getStateFrom());
        wsPartneridentifikatorType3.setGueltBis(wsJurPersonType.getStateUpto());
        wsPartneridentifikatorType3.setId(techMdmPartnerIDAsPartnerIdentifikatorId);
        WsPartneridentifikatorDefIdType wsPartneridentifikatorDefIdType3 = new WsPartneridentifikatorDefIdType();
        wsPartneridentifikatorDefIdType3.setId("2890e963-c97f-491e-bb55-b431391f88d2");
        wsPartneridentifikatorType3.setPartneridentifikatorDefId(wsPartneridentifikatorDefIdType3);
        wsPartneridentifikatorType3.setWert(FinmaNr);
        wsJurPersonStrukturType.getPartneridentifikatoren().

                add(wsPartneridentifikatorType3);


        long epoch = LocalDateTime.now().atZone(ZoneId.of("Z")).toEpochSecond() * 1000000000L;
        wsJurPersonStrukturType.setExterneVersion(epoch); //nanoseconds since epoch
        repliziereJurPersonRequestType.setJurPersonStruktur(wsJurPersonStrukturType);
        return repliziereJurPersonRequestType;
    }


}