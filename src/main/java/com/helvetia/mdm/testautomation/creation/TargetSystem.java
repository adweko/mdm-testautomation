package com.helvetia.mdm.testautomation.creation;

import com.helvetia.mdm.testautomation.model.Partner;

public interface TargetSystem {

    boolean createPartner(Partner partner);

}
