/*
 * Copyright (C) 2019 Helvetia Versicherungen. All Rights Reserved.
 */
package com.helvetia.mdm.testautomation.creation.zepas.config;


import com.helvetia.security.saml.SAMLFactory;
import org.w3c.dom.Element;
import zepas.Status;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import java.time.LocalDate;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class HelperWsClient {

    // Timeout: All properties to be put on getRequestContext() in milliseconds
    private static final int TIMEOUT = 10000; // 10s


    public static void prepareWs(Object port, String webServiceUrl, WsConfiguration wsConfiguration)
            throws Exception {
        ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, webServiceUrl);
        ((BindingProvider) port).getRequestContext().put("javax.xml.ws.client.connectionTimeout", TIMEOUT);
        ((BindingProvider) port).getRequestContext().put("javax.xml.ws.client.receiveTimeout", TIMEOUT);
        SAMLAssertionCreator assertionCreator = new SAMLAssertionCreator();
        // Generate SAML assertion
        Element assertion = assertionCreator.getSAMLAssertion(wsConfiguration, false);
        // Assertion dem Request hinzufügen
        BindingProvider bindingProvider = (BindingProvider) port;
        Map<String, Object> context = bindingProvider.getRequestContext();
        SAMLFactory.getInstance().addSamlAssertion(context, assertion);
        // Handler dem Request hinzufügen, für die Serialisierung
        SAMLFactory.getInstance().addSamlHandler(bindingProvider);
    }


    public static XMLGregorianCalendar getCalendar(Date date) throws Exception {
        if (date == null) {
            return null;
        }
        GregorianCalendar gregory = new GregorianCalendar();
        gregory.setTime(date);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
    }

    public static XMLGregorianCalendar formatLocalDate(LocalDate date) {
        if (date == null) {
            return null;
        }
        XMLGregorianCalendar calendar = null;
        try {
            calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(date.toString());
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
        calendar.setTime(0,0,0);
        return calendar;
    }


    public static void printStatus(List<Status> statusList) {
        System.out.println("Status Web Service:");
        System.out.println("===================");
        for (Status s : statusList) {
            System.out.println("Return code: " + s.getReturnCode() + ", Message type: " + s.getMessageType()
                    + ", Message text: " + s.getMessageText());
        }
    }
}

