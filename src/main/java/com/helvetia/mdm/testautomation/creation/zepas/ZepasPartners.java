package com.helvetia.mdm.testautomation.creation.zepas;

import com.helvetia.mdm.testautomation.creation.zepas.config.HelperWsClient;
import com.helvetia.mdm.testautomation.model.Address;
import com.helvetia.mdm.testautomation.model.LegalPerson;
import com.helvetia.mdm.testautomation.model.NaturalPerson;
import com.helvetia.mdm.testautomation.model.Partner;
import com.helvetia.mdm.testautomation.model.communicationelement.*;
import zepas.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class ZepasPartners {

    public static final String TEST_USER = "CH-TEST";

    public static MdmFullPerson fromPartner(Partner partner) {
        if (partner instanceof NaturalPerson) {
            return mapNaturalPerson((NaturalPerson) partner);
        } else if (partner instanceof LegalPerson) {
            return mapLegalPerson((LegalPerson) partner);
        } else {
            throw new RuntimeException("unexpected class " + partner.getClass().toString() + " in fromPartner mapping");
        }
    }

    private static MdmFullPerson mapLegalPerson(LegalPerson partner) {
        MdmFullPerson mdmFullPerson = new MdmFullPerson();
        MdmUnternehmung mdmUnternehmung = mapLegalPersonToUnternehmung(partner);
        MdmCompositePerson mdmCompositePerson = new MdmCompositePerson();
        mdmCompositePerson.setMdmUnternehmung(mdmUnternehmung);
        mdmCompositePerson.setDelete(mdmUnternehmung.isDelete());
        mdmFullPerson.setMdmCompositePerson(mdmCompositePerson);
        addPartnersChannelsToMdmFullPerson(mdmFullPerson, partner);
        addPartnersAddressesToMdmFullPerson(mdmFullPerson, partner);
        return mdmFullPerson;
    }

    private static void addPartnersChannelsToMdmFullPerson(MdmFullPerson mdmFullPerson, Partner partner) {
        final Optional<List<CommunicationElement>> communicationElements = Optional.ofNullable(partner.getCommunicationElements());
        communicationElements.ifPresent(elements -> {
                    List<MdmKommunikationselement> kommunikationselements = elements.stream()
                            .map(ZepasPartners::mapCommunicationElement)
                            .collect(Collectors.toList());
                    mdmFullPerson.getMdmKommunikationselemente().addAll(kommunikationselements);
                }
        );
    }

    private static void addPartnersAddressesToMdmFullPerson(MdmFullPerson mdmFullPerson, Partner partner) {
        MdmAdresse mainAddress = mapPartnersAddress(partner.getMainAddress());
        mainAddress.setAdressartCode("001");
        mdmFullPerson.getMdmAdressen().add(mainAddress);
        if (partner.getSecondAddress() != null) {
            MdmAdresse secondAddress = mapPartnersAddress(partner.getSecondAddress());
            secondAddress.setAdressartCode("002");
            mdmFullPerson.getMdmAdressen().add(secondAddress);
        }
    }

    private static MdmAdresse mapPartnersAddress(Address address) {
        final MdmAdresse mdmAdresse = new MdmAdresse();
        mdmAdresse.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
        mdmAdresse.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        mdmAdresse.setEingabeSb(TEST_USER);
        mdmAdresse.setPostleitzahl(address.getZIPCode());
        mdmAdresse.setStrassenname(address.getStreet());
        mdmAdresse.setHausnummer(address.getHouseNumber());
        mdmAdresse.setOrtsname(address.getCity());
        mdmAdresse.setLandkennzeichenCode(address.getCountry());
        Optional.ofNullable(address.getRegionName()).ifPresent(mdmAdresse::setRegion);
        Optional.ofNullable(address.getLocationDescription()).ifPresent(mdmAdresse::setLokalitaetsbezeichnung);
        return mdmAdresse;
    }


    private static MdmKommunikationselement mapCommunicationElement(CommunicationElement ce) {
        final MdmKommunikationselement mdmKommunikationselement = new MdmKommunikationselement();
        mdmKommunikationselement.setEingabeAm(HelperWsClient.formatLocalDate(LocalDate.now()));
        mdmKommunikationselement.setDelete(false);
        mdmKommunikationselement.setEingabeSb(TEST_USER);
        mdmKommunikationselement.setPrioritaet(0);

        if (ce instanceof TelephoneNumber) {
            mdmKommunikationselement.setElementtypCode("120");
            mdmKommunikationselement.setKommkanalCode("TEL");
            mdmKommunikationselement.setValue(((TelephoneNumber) ce).getTelephoneNumber());
        } else if (ce instanceof MobileNumber) {
            mdmKommunikationselement.setElementtypCode("120");
            mdmKommunikationselement.setKommkanalCode("MOB");
            mdmKommunikationselement.setValue(((MobileNumber) ce).getMobileNumber());
        } else if (ce instanceof FaxNumber) {
            mdmKommunikationselement.setElementtypCode("120");
            mdmKommunikationselement.setKommkanalCode("FAX");
            mdmKommunikationselement.setValue(((FaxNumber) ce).getFaxNumber());
        } else if (ce instanceof EmailAddress) {
            mdmKommunikationselement.setElementtypCode("120");
            mdmKommunikationselement.setKommkanalCode("MAIL");
            mdmKommunikationselement.setValue(((EmailAddress) ce).getEmailAddress());
        } else if (ce instanceof SkypeAddress) {
            mdmKommunikationselement.setElementtypCode("120");
            mdmKommunikationselement.setKommkanalCode("SKYPE");
            mdmKommunikationselement.setValue(((SkypeAddress) ce).getSkypeAddress());
        } else {
            throw new RuntimeException("unexpected type of communicationelement");
        }
        return mdmKommunikationselement;
    }

    private static MdmUnternehmung mapLegalPersonToUnternehmung(LegalPerson partner) {
        MdmUnternehmung mdmUnternehmung = new MdmUnternehmung();
        mdmUnternehmung.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
        mdmUnternehmung.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        mdmUnternehmung.setEingabeAm(HelperWsClient.formatLocalDate(LocalDate.now()));
        mdmUnternehmung.setDelete(false);
        mdmUnternehmung.setEingabeSb(TEST_USER);
        mdmUnternehmung.setKennzeichenCode("U");
        mdmUnternehmung.setSprachCode(partner.getLanguage().name());
        mdmUnternehmung.setAnredeCode("");
        mdmUnternehmung.setName(partner.getCompanyName());
        mdmUnternehmung.setNamenZusatz1(null);
        mdmUnternehmung.setNamenZusatz2(null);
        mdmUnternehmung.setNogaCode(partner.getNOGACode());
        mdmUnternehmung.setRechtsformCode(partner.getLegalType());
        mdmUnternehmung.setUnternehmensId(UUID.randomUUID().toString().replace(".", "").replace("-", "")
                .toUpperCase());
        mdmUnternehmung.setMehrwertssteuernummer(0L);
        mdmUnternehmung.setGegenparteiId("");
        mdmUnternehmung.setStiftungstext1("");
        mdmUnternehmung.setStiftungstext2("");
        mdmUnternehmung.setLaufnummer(0L);
        return mdmUnternehmung;
    }

    private static MdmFullPerson mapNaturalPerson(NaturalPerson partner) {
        MdmFullPerson mdmFullPerson = new MdmFullPerson();
        MdmPrivatperson mdmPrivatperson = mapNaturalPersonToPrivatPerson(partner);
        MdmCompositePerson mdmCompositePerson = new MdmCompositePerson();
        mdmCompositePerson.setMdmPrivatperson(mdmPrivatperson);
        mdmCompositePerson.setMdmPersonenLaufnummer(0L);
        mdmCompositePerson.setDelete(mdmPrivatperson.isDelete());
        mdmFullPerson.setMdmCompositePerson(mdmCompositePerson);
        addPartnersChannelsToMdmFullPerson(mdmFullPerson, partner);
        addPartnersAddressesToMdmFullPerson(mdmFullPerson, partner);
        return mdmFullPerson;
    }

    private static MdmPrivatperson mapNaturalPersonToPrivatPerson(NaturalPerson partner) {
        final MdmPrivatperson mdmPrivatperson = new MdmPrivatperson();

        mdmPrivatperson.setDelete(false);
        mdmPrivatperson.setGueltigAb(HelperWsClient.formatLocalDate(LocalDate.now()));
        mdmPrivatperson.setGueltigBis(HelperWsClient.formatLocalDate(LocalDate.of(9999, 12, 31)));
        mdmPrivatperson.setEingabeSb(TEST_USER);
        mdmPrivatperson.setLaufnummer(0L);
        mdmPrivatperson.setKennzeichenCode("P");

        Optional.ofNullable(partner.getAcademicDegree()).ifPresent(mdmPrivatperson::setAkademischerTitelCode);
        Optional.ofNullable(partner.getDateOfBirth()).ifPresent(birthDate -> mdmPrivatperson.setGeburtsdatum(HelperWsClient.formatLocalDate(birthDate)));
        Optional.ofNullable(partner.getDateOfDeath()).ifPresent(deathDate -> mdmPrivatperson.setTodesdatum(HelperWsClient.formatLocalDate(deathDate)));
        Optional.ofNullable(partner.getEmploymentStatus()).ifPresent(mdmPrivatperson::setAnstellungsVerhaeltnisCode);
        Optional.ofNullable(partner.getHomeTown()).ifPresent(mdmPrivatperson::setBuergerort);
        Optional.ofNullable(partner.getLanguage()).ifPresent(l -> mdmPrivatperson.setSprachCode(l.name()));
        Optional.ofNullable(partner.getMaritalStatus()).ifPresent(mdmPrivatperson::setZivilstandsCode);
        Optional.ofNullable(partner.getSurname()).ifPresent(mdmPrivatperson::setName);
        Optional.ofNullable(partner.getName()).ifPresent(mdmPrivatperson::setVorname);
        Optional.ofNullable(partner.getSecondName()).ifPresent(mdmPrivatperson::setNamenZusatz1);
        Optional.ofNullable(partner.getThirdName()).ifPresent(mdmPrivatperson::setNamenZusatz2);
        Optional.ofNullable(partner.getNameAtBirth()).ifPresent(mdmPrivatperson::setGeburtsname);
        Optional.ofNullable(partner.getSex()).ifPresent(mdmPrivatperson::setGeschlechtCode);
        Optional.ofNullable(partner.getSocialSecurityNumber()).ifPresent(nr -> mdmPrivatperson.setSozialversicherungsnummer(Long.parseLong(nr)));
        Optional.ofNullable(partner.getOtherTitle()).ifPresent(mdmPrivatperson::setSonstigeTitel);
        Optional.ofNullable(partner.getSalutation()).ifPresent(mdmPrivatperson::setAnredeCode);
        Optional.ofNullable(partner.getResidencePermit()).ifPresent(mdmPrivatperson::setAufenthaltsbewilligungCode);

        return mdmPrivatperson;
    }

}
