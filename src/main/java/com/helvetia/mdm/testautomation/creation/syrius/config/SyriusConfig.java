package com.helvetia.mdm.testautomation.creation.syrius.config;

import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.data.partnerrolle.v1.PartnerrolleServiceV1;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServiceV0;
import lombok.Data;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
@Data
public class SyriusConfig {

    @ConfigProperty(name = "ch.helvetia.syrius.endpoint")
    String endpoint;

    @Inject
    SSLConfig sslConfig;

    final String SYRIUS = "SYRIUS_MF";

    PartnerReplikationExpServiceV0 replikationExpService = new PartnerReplikationExpServiceV0(getClass().getResource("schemata/wsdl/syrius/partnermgmt/partnerdatenverw/process/partnerreplikationexp/v0/PartnerReplikationExpService.wsdl"));
    PartnerrolleServiceV1 partnerrolleService = new PartnerrolleServiceV1(getClass().getResource("schemata/wsdl/syrius/partnermgmt/partnerdatenverw/data/partnerrolle/v1/PartnerrolleService.wsdl"));
}
