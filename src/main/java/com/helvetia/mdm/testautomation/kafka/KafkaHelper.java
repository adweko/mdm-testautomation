package com.helvetia.mdm.testautomation.kafka;

import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public class KafkaHelper {

    public static Properties getConsumerConfig(boolean useSpecificAvro) {
        Properties consumerConfig = getConsumerConfigWithoutSerdes();
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        consumerConfig.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, useSpecificAvro);
        return consumerConfig;
    }

    public static Properties getConsumerConfigWithoutSerdes() {
        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "ch-mdm_test1234");
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "cluster-main-kafka-bootstrap-kafka-devl.apps.helvetia.io:443");
        consumerConfig.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "https://cp-schema-registry-kafka-devl.apps.helvetia.io:443");
        consumerConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        consumerConfig.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);

        consumerConfig.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL.name);
        consumerConfig.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, Paths.get("src/main/resources/cert/user-truststore.jks").toAbsolutePath().toString());
        consumerConfig.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, System.getProperty("SSL_TRUSTSTORE_PASSWORD"));
        consumerConfig.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, Paths.get("src/main/resources/cert/user-keystore.jks").toAbsolutePath().toString());
        consumerConfig.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, System.getProperty("SSL_KEYSTORE_PASSWORD"));
        consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

        return consumerConfig;
    }

    public static Map<TopicPartition, Long> getEndOffsets(String topic) {
        Consumer<String, GenericRecord> consumer = new KafkaConsumer<>(getConsumerConfig(false));
        final List<PartitionInfo> partitionInfos = consumer.partitionsFor(topic);
        final List<TopicPartition> topicPartitions = partitionInfos.stream()
                .map(info -> new TopicPartition(info.topic(), info.partition()))
                .collect(Collectors.toList());
        consumer.assign(topicPartitions);
        return consumer.endOffsets(consumer.assignment());

    }

}
