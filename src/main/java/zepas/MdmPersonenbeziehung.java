
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmPersonenbeziehung complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmPersonenbeziehung"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCommon"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="kupeLaufnummer1" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="kupeLaufnummer2" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="personenbeziehungsartCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmPersonenbeziehung", propOrder = {
    "kupeLaufnummer1",
    "kupeLaufnummer2",
    "personenbeziehungsartCode"
})
public class MdmPersonenbeziehung
    extends MdmCommon
{

    protected long kupeLaufnummer1;
    protected long kupeLaufnummer2;
    @XmlElement(required = true)
    protected String personenbeziehungsartCode;

    /**
     * Gets the value of the kupeLaufnummer1 property.
     * 
     */
    public long getKupeLaufnummer1() {
        return kupeLaufnummer1;
    }

    /**
     * Sets the value of the kupeLaufnummer1 property.
     * 
     */
    public void setKupeLaufnummer1(long value) {
        this.kupeLaufnummer1 = value;
    }

    /**
     * Gets the value of the kupeLaufnummer2 property.
     * 
     */
    public long getKupeLaufnummer2() {
        return kupeLaufnummer2;
    }

    /**
     * Sets the value of the kupeLaufnummer2 property.
     * 
     */
    public void setKupeLaufnummer2(long value) {
        this.kupeLaufnummer2 = value;
    }

    /**
     * Gets the value of the personenbeziehungsartCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonenbeziehungsartCode() {
        return personenbeziehungsartCode;
    }

    /**
     * Sets the value of the personenbeziehungsartCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonenbeziehungsartCode(String value) {
        this.personenbeziehungsartCode = value;
    }

}
