
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmAdresse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmAdresse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCommon"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="adressartCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="lokalitaetsbezeichnung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="wohnungsbezeichnung" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="postfachnummer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="strassenname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="hausnummer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="postleitzahl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ortsname" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="landkennzeichenCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="geodaten" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}geodaten" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmAdresse", propOrder = {
    "adressartCode",
    "lokalitaetsbezeichnung",
    "wohnungsbezeichnung",
    "postfachnummer",
    "strassenname",
    "hausnummer",
    "postleitzahl",
    "ortsname",
    "region",
    "landkennzeichenCode",
    "geodaten"
})
public class MdmAdresse
    extends MdmCommon
{

    @XmlElement(required = true)
    protected String adressartCode;
    protected String lokalitaetsbezeichnung;
    protected String wohnungsbezeichnung;
    protected String postfachnummer;
    protected String strassenname;
    protected String hausnummer;
    protected String postleitzahl;
    @XmlElement(required = true)
    protected String ortsname;
    protected String region;
    @XmlElement(required = true)
    protected String landkennzeichenCode;
    protected Geodaten geodaten;

    /**
     * Gets the value of the adressartCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdressartCode() {
        return adressartCode;
    }

    /**
     * Sets the value of the adressartCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdressartCode(String value) {
        this.adressartCode = value;
    }

    /**
     * Gets the value of the lokalitaetsbezeichnung property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLokalitaetsbezeichnung() {
        return lokalitaetsbezeichnung;
    }

    /**
     * Sets the value of the lokalitaetsbezeichnung property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLokalitaetsbezeichnung(String value) {
        this.lokalitaetsbezeichnung = value;
    }

    /**
     * Gets the value of the wohnungsbezeichnung property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWohnungsbezeichnung() {
        return wohnungsbezeichnung;
    }

    /**
     * Sets the value of the wohnungsbezeichnung property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWohnungsbezeichnung(String value) {
        this.wohnungsbezeichnung = value;
    }

    /**
     * Gets the value of the postfachnummer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostfachnummer() {
        return postfachnummer;
    }

    /**
     * Sets the value of the postfachnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostfachnummer(String value) {
        this.postfachnummer = value;
    }

    /**
     * Gets the value of the strassenname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrassenname() {
        return strassenname;
    }

    /**
     * Sets the value of the strassenname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrassenname(String value) {
        this.strassenname = value;
    }

    /**
     * Gets the value of the hausnummer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHausnummer() {
        return hausnummer;
    }

    /**
     * Sets the value of the hausnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHausnummer(String value) {
        this.hausnummer = value;
    }

    /**
     * Gets the value of the postleitzahl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostleitzahl() {
        return postleitzahl;
    }

    /**
     * Sets the value of the postleitzahl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostleitzahl(String value) {
        this.postleitzahl = value;
    }

    /**
     * Gets the value of the ortsname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrtsname() {
        return ortsname;
    }

    /**
     * Sets the value of the ortsname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrtsname(String value) {
        this.ortsname = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Gets the value of the landkennzeichenCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandkennzeichenCode() {
        return landkennzeichenCode;
    }

    /**
     * Sets the value of the landkennzeichenCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandkennzeichenCode(String value) {
        this.landkennzeichenCode = value;
    }

    /**
     * Gets the value of the geodaten property.
     * 
     * @return
     *     possible object is
     *     {@link Geodaten }
     *     
     */
    public Geodaten getGeodaten() {
        return geodaten;
    }

    /**
     * Sets the value of the geodaten property.
     * 
     * @param value
     *     allowed object is
     *     {@link Geodaten }
     *     
     */
    public void setGeodaten(Geodaten value) {
        this.geodaten = value;
    }

}
