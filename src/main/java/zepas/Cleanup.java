
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cleanup complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cleanup"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="personenLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="wsControl" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}wsControl" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cleanup", propOrder = {
    "personenLaufnummer",
    "wsControl"
})
public class Cleanup {

    protected Long personenLaufnummer;
    protected WsControl wsControl;

    /**
     * Gets the value of the personenLaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPersonenLaufnummer() {
        return personenLaufnummer;
    }

    /**
     * Sets the value of the personenLaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPersonenLaufnummer(Long value) {
        this.personenLaufnummer = value;
    }

    /**
     * Gets the value of the wsControl property.
     * 
     * @return
     *     possible object is
     *     {@link WsControl }
     *     
     */
    public WsControl getWsControl() {
        return wsControl;
    }

    /**
     * Sets the value of the wsControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsControl }
     *     
     */
    public void setWsControl(WsControl value) {
        this.wsControl = value;
    }

}
