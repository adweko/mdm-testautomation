
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmKommelementVerwendung complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmKommelementVerwendung"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCommon"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="verwendungCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="kommelementLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmKommelementVerwendung", propOrder = {
    "verwendungCode",
    "kommelementLaufnummer"
})
public class MdmKommelementVerwendung
    extends MdmCommon
{

    @XmlElement(required = true)
    protected String verwendungCode;
    protected long kommelementLaufnummer;

    /**
     * Gets the value of the verwendungCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerwendungCode() {
        return verwendungCode;
    }

    /**
     * Sets the value of the verwendungCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerwendungCode(String value) {
        this.verwendungCode = value;
    }

    /**
     * Gets the value of the kommelementLaufnummer property.
     * 
     */
    public long getKommelementLaufnummer() {
        return kommelementLaufnummer;
    }

    /**
     * Sets the value of the kommelementLaufnummer property.
     * 
     */
    public void setKommelementLaufnummer(long value) {
        this.kommelementLaufnummer = value;
    }

}
