
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmCompositePerson complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmCompositePerson"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mdmPersonenLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="delete" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="mdmPrivatperson" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmPrivatperson" minOccurs="0"/&gt;
 *         &lt;element name="mdmUnternehmung" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmUnternehmung" minOccurs="0"/&gt;
 *         &lt;element name="mdmOrganisationseinheit" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmOrganisationseinheit" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmCompositePerson", propOrder = {
    "mdmPersonenLaufnummer",
    "delete",
    "mdmPrivatperson",
    "mdmUnternehmung",
    "mdmOrganisationseinheit"
})
public class MdmCompositePerson {

    protected long mdmPersonenLaufnummer;
    protected boolean delete;
    protected MdmPrivatperson mdmPrivatperson;
    protected MdmUnternehmung mdmUnternehmung;
    protected MdmOrganisationseinheit mdmOrganisationseinheit;

    /**
     * Gets the value of the mdmPersonenLaufnummer property.
     * 
     */
    public long getMdmPersonenLaufnummer() {
        return mdmPersonenLaufnummer;
    }

    /**
     * Sets the value of the mdmPersonenLaufnummer property.
     * 
     */
    public void setMdmPersonenLaufnummer(long value) {
        this.mdmPersonenLaufnummer = value;
    }

    /**
     * Gets the value of the delete property.
     * 
     */
    public boolean isDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     * 
     */
    public void setDelete(boolean value) {
        this.delete = value;
    }

    /**
     * Gets the value of the mdmPrivatperson property.
     * 
     * @return
     *     possible object is
     *     {@link MdmPrivatperson }
     *     
     */
    public MdmPrivatperson getMdmPrivatperson() {
        return mdmPrivatperson;
    }

    /**
     * Sets the value of the mdmPrivatperson property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdmPrivatperson }
     *     
     */
    public void setMdmPrivatperson(MdmPrivatperson value) {
        this.mdmPrivatperson = value;
    }

    /**
     * Gets the value of the mdmUnternehmung property.
     * 
     * @return
     *     possible object is
     *     {@link MdmUnternehmung }
     *     
     */
    public MdmUnternehmung getMdmUnternehmung() {
        return mdmUnternehmung;
    }

    /**
     * Sets the value of the mdmUnternehmung property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdmUnternehmung }
     *     
     */
    public void setMdmUnternehmung(MdmUnternehmung value) {
        this.mdmUnternehmung = value;
    }

    /**
     * Gets the value of the mdmOrganisationseinheit property.
     * 
     * @return
     *     possible object is
     *     {@link MdmOrganisationseinheit }
     *     
     */
    public MdmOrganisationseinheit getMdmOrganisationseinheit() {
        return mdmOrganisationseinheit;
    }

    /**
     * Sets the value of the mdmOrganisationseinheit property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdmOrganisationseinheit }
     *     
     */
    public void setMdmOrganisationseinheit(MdmOrganisationseinheit value) {
        this.mdmOrganisationseinheit = value;
    }

}
