
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmZahlungsverbindung complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmZahlungsverbindung"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCommon"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bankkontonummer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="zahlungsartCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="rolleLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="personenLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="ibanNummer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmZahlungsverbindung", propOrder = {
    "bankkontonummer",
    "zahlungsartCode",
    "rolleLaufnummer",
    "personenLaufnummer",
    "ibanNummer"
})
public class MdmZahlungsverbindung
    extends MdmCommon
{

    protected String bankkontonummer;
    protected String zahlungsartCode;
    protected Long rolleLaufnummer;
    protected Long personenLaufnummer;
    @XmlElement(required = true)
    protected String ibanNummer;

    /**
     * Gets the value of the bankkontonummer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankkontonummer() {
        return bankkontonummer;
    }

    /**
     * Sets the value of the bankkontonummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankkontonummer(String value) {
        this.bankkontonummer = value;
    }

    /**
     * Gets the value of the zahlungsartCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlungsartCode() {
        return zahlungsartCode;
    }

    /**
     * Sets the value of the zahlungsartCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlungsartCode(String value) {
        this.zahlungsartCode = value;
    }

    /**
     * Gets the value of the rolleLaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRolleLaufnummer() {
        return rolleLaufnummer;
    }

    /**
     * Sets the value of the rolleLaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRolleLaufnummer(Long value) {
        this.rolleLaufnummer = value;
    }

    /**
     * Gets the value of the personenLaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPersonenLaufnummer() {
        return personenLaufnummer;
    }

    /**
     * Sets the value of the personenLaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPersonenLaufnummer(Long value) {
        this.personenLaufnummer = value;
    }

    /**
     * Gets the value of the ibanNummer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIbanNummer() {
        return ibanNummer;
    }

    /**
     * Sets the value of the ibanNummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbanNummer(String value) {
        this.ibanNummer = value;
    }

}
