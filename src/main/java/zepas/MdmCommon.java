
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for mdmCommon complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmCommon"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="eingabeAm" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="eingabeSb" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="gueltigAb" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="gueltigBis" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="laufnummer" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="delete" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmCommon", propOrder = {
    "eingabeAm",
    "eingabeSb",
    "gueltigAb",
    "gueltigBis",
    "laufnummer",
    "delete"
})
@XmlSeeAlso({
    MdmMitarbeiter.class,
    MdmPerson.class,
    MdmPersonenbeziehung.class,
    MdmAdresse.class,
    MdmKommunikationselement.class,
    MdmKommelementVerwendung.class,
    MdmVersandanweisung.class,
    MdmZahlungsverbindung.class,
    MdmFinmaRegister.class,
    MdmZusatzinformation.class,
    MdmCodeZurPerson.class
})
public abstract class MdmCommon {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eingabeAm;
    @XmlElement(required = true)
    protected String eingabeSb;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gueltigAb;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar gueltigBis;
    protected long laufnummer;
    protected boolean delete;

    /**
     * Gets the value of the eingabeAm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEingabeAm() {
        return eingabeAm;
    }

    /**
     * Sets the value of the eingabeAm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEingabeAm(XMLGregorianCalendar value) {
        this.eingabeAm = value;
    }

    /**
     * Gets the value of the eingabeSb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEingabeSb() {
        return eingabeSb;
    }

    /**
     * Sets the value of the eingabeSb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEingabeSb(String value) {
        this.eingabeSb = value;
    }

    /**
     * Gets the value of the gueltigAb property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltigAb() {
        return gueltigAb;
    }

    /**
     * Sets the value of the gueltigAb property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltigAb(XMLGregorianCalendar value) {
        this.gueltigAb = value;
    }

    /**
     * Gets the value of the gueltigBis property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGueltigBis() {
        return gueltigBis;
    }

    /**
     * Sets the value of the gueltigBis property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGueltigBis(XMLGregorianCalendar value) {
        this.gueltigBis = value;
    }

    /**
     * Gets the value of the laufnummer property.
     * 
     */
    public long getLaufnummer() {
        return laufnummer;
    }

    /**
     * Sets the value of the laufnummer property.
     * 
     */
    public void setLaufnummer(long value) {
        this.laufnummer = value;
    }

    /**
     * Gets the value of the delete property.
     * 
     */
    public boolean isDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     * 
     */
    public void setDelete(boolean value) {
        this.delete = value;
    }

}
