
package zepas;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the zepas package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Cleanup_QNAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/", "cleanup");
    private final static QName _CleanupResponse_QNAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/", "cleanupResponse");
    private final static QName _Get_QNAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/", "get");
    private final static QName _GetLaufnummernRange_QNAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/", "getLaufnummernRange");
    private final static QName _GetLaufnummernRangeResponse_QNAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/", "getLaufnummernRangeResponse");
    private final static QName _GetResponse_QNAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/", "getResponse");
    private final static QName _Synchronize_QNAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/", "synchronize");
    private final static QName _SynchronizeResponse_QNAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/", "synchronizeResponse");
    private final static QName _ZepaswebServiceException_QNAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/", "ZepaswebServiceException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: zepas
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Cleanup }
     * 
     */
    public Cleanup createCleanup() {
        return new Cleanup();
    }

    /**
     * Create an instance of {@link CleanupResponse }
     * 
     */
    public CleanupResponse createCleanupResponse() {
        return new CleanupResponse();
    }

    /**
     * Create an instance of {@link Get }
     * 
     */
    public Get createGet() {
        return new Get();
    }

    /**
     * Create an instance of {@link GetLaufnummernRange }
     * 
     */
    public GetLaufnummernRange createGetLaufnummernRange() {
        return new GetLaufnummernRange();
    }

    /**
     * Create an instance of {@link GetLaufnummernRangeResponse }
     * 
     */
    public GetLaufnummernRangeResponse createGetLaufnummernRangeResponse() {
        return new GetLaufnummernRangeResponse();
    }

    /**
     * Create an instance of {@link GetResponse }
     * 
     */
    public GetResponse createGetResponse() {
        return new GetResponse();
    }

    /**
     * Create an instance of {@link Synchronize }
     * 
     */
    public Synchronize createSynchronize() {
        return new Synchronize();
    }

    /**
     * Create an instance of {@link SynchronizeResponse }
     * 
     */
    public SynchronizeResponse createSynchronizeResponse() {
        return new SynchronizeResponse();
    }

    /**
     * Create an instance of {@link ZepaswebServiceException }
     * 
     */
    public ZepaswebServiceException createZepaswebServiceException() {
        return new ZepaswebServiceException();
    }

    /**
     * Create an instance of {@link WsControl }
     * 
     */
    public WsControl createWsControl() {
        return new WsControl();
    }

    /**
     * Create an instance of {@link MdmFullPerson }
     * 
     */
    public MdmFullPerson createMdmFullPerson() {
        return new MdmFullPerson();
    }

    /**
     * Create an instance of {@link MdmCompositePerson }
     * 
     */
    public MdmCompositePerson createMdmCompositePerson() {
        return new MdmCompositePerson();
    }

    /**
     * Create an instance of {@link MdmPrivatperson }
     * 
     */
    public MdmPrivatperson createMdmPrivatperson() {
        return new MdmPrivatperson();
    }

    /**
     * Create an instance of {@link MdmMitarbeiter }
     * 
     */
    public MdmMitarbeiter createMdmMitarbeiter() {
        return new MdmMitarbeiter();
    }

    /**
     * Create an instance of {@link MdmUnternehmung }
     * 
     */
    public MdmUnternehmung createMdmUnternehmung() {
        return new MdmUnternehmung();
    }

    /**
     * Create an instance of {@link MdmOrganisationseinheit }
     * 
     */
    public MdmOrganisationseinheit createMdmOrganisationseinheit() {
        return new MdmOrganisationseinheit();
    }

    /**
     * Create an instance of {@link MdmPersonenbeziehung }
     * 
     */
    public MdmPersonenbeziehung createMdmPersonenbeziehung() {
        return new MdmPersonenbeziehung();
    }

    /**
     * Create an instance of {@link MdmAdresse }
     * 
     */
    public MdmAdresse createMdmAdresse() {
        return new MdmAdresse();
    }

    /**
     * Create an instance of {@link Geodaten }
     * 
     */
    public Geodaten createGeodaten() {
        return new Geodaten();
    }

    /**
     * Create an instance of {@link MdmKommunikationselement }
     * 
     */
    public MdmKommunikationselement createMdmKommunikationselement() {
        return new MdmKommunikationselement();
    }

    /**
     * Create an instance of {@link MdmKommelementVerwendung }
     * 
     */
    public MdmKommelementVerwendung createMdmKommelementVerwendung() {
        return new MdmKommelementVerwendung();
    }

    /**
     * Create an instance of {@link MdmVersandanweisung }
     * 
     */
    public MdmVersandanweisung createMdmVersandanweisung() {
        return new MdmVersandanweisung();
    }

    /**
     * Create an instance of {@link MdmZahlungsverbindung }
     * 
     */
    public MdmZahlungsverbindung createMdmZahlungsverbindung() {
        return new MdmZahlungsverbindung();
    }

    /**
     * Create an instance of {@link MdmFinmaRegister }
     * 
     */
    public MdmFinmaRegister createMdmFinmaRegister() {
        return new MdmFinmaRegister();
    }

    /**
     * Create an instance of {@link MdmZusatzinformation }
     * 
     */
    public MdmZusatzinformation createMdmZusatzinformation() {
        return new MdmZusatzinformation();
    }

    /**
     * Create an instance of {@link MdmCodeZurPerson }
     * 
     */
    public MdmCodeZurPerson createMdmCodeZurPerson() {
        return new MdmCodeZurPerson();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cleanup }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Cleanup }{@code >}
     */
    @XmlElementDecl(namespace = "http://v5.soap.wst.ws.zepasweb.helvetia.ch/", name = "cleanup")
    public JAXBElement<Cleanup> createCleanup(Cleanup value) {
        return new JAXBElement<Cleanup>(_Cleanup_QNAME, Cleanup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CleanupResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CleanupResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://v5.soap.wst.ws.zepasweb.helvetia.ch/", name = "cleanupResponse")
    public JAXBElement<CleanupResponse> createCleanupResponse(CleanupResponse value) {
        return new JAXBElement<CleanupResponse>(_CleanupResponse_QNAME, CleanupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Get }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Get }{@code >}
     */
    @XmlElementDecl(namespace = "http://v5.soap.wst.ws.zepasweb.helvetia.ch/", name = "get")
    public JAXBElement<Get> createGet(Get value) {
        return new JAXBElement<Get>(_Get_QNAME, Get.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLaufnummernRange }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLaufnummernRange }{@code >}
     */
    @XmlElementDecl(namespace = "http://v5.soap.wst.ws.zepasweb.helvetia.ch/", name = "getLaufnummernRange")
    public JAXBElement<GetLaufnummernRange> createGetLaufnummernRange(GetLaufnummernRange value) {
        return new JAXBElement<GetLaufnummernRange>(_GetLaufnummernRange_QNAME, GetLaufnummernRange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLaufnummernRangeResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetLaufnummernRangeResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://v5.soap.wst.ws.zepasweb.helvetia.ch/", name = "getLaufnummernRangeResponse")
    public JAXBElement<GetLaufnummernRangeResponse> createGetLaufnummernRangeResponse(GetLaufnummernRangeResponse value) {
        return new JAXBElement<GetLaufnummernRangeResponse>(_GetLaufnummernRangeResponse_QNAME, GetLaufnummernRangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://v5.soap.wst.ws.zepasweb.helvetia.ch/", name = "getResponse")
    public JAXBElement<GetResponse> createGetResponse(GetResponse value) {
        return new JAXBElement<GetResponse>(_GetResponse_QNAME, GetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Synchronize }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Synchronize }{@code >}
     */
    @XmlElementDecl(namespace = "http://v5.soap.wst.ws.zepasweb.helvetia.ch/", name = "synchronize")
    public JAXBElement<Synchronize> createSynchronize(Synchronize value) {
        return new JAXBElement<Synchronize>(_Synchronize_QNAME, Synchronize.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SynchronizeResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SynchronizeResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://v5.soap.wst.ws.zepasweb.helvetia.ch/", name = "synchronizeResponse")
    public JAXBElement<SynchronizeResponse> createSynchronizeResponse(SynchronizeResponse value) {
        return new JAXBElement<SynchronizeResponse>(_SynchronizeResponse_QNAME, SynchronizeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZepaswebServiceException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ZepaswebServiceException }{@code >}
     */
    @XmlElementDecl(namespace = "http://v5.soap.wst.ws.zepasweb.helvetia.ch/", name = "ZepaswebServiceException")
    public JAXBElement<ZepaswebServiceException> createZepaswebServiceException(ZepaswebServiceException value) {
        return new JAXBElement<ZepaswebServiceException>(_ZepaswebServiceException_QNAME, ZepaswebServiceException.class, null, value);
    }

}
