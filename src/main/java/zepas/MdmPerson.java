
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmPerson complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmPerson"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCommon"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="anredeCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="kennzeichenCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="namenZusatz1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="namenZusatz2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="rabattklasseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sprachCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmPerson", propOrder = {
    "anredeCode",
    "kennzeichenCode",
    "name",
    "namenZusatz1",
    "namenZusatz2",
    "rabattklasseCode",
    "sprachCode"
})
@XmlSeeAlso({
    MdmPrivatperson.class,
    MdmUnternehmung.class,
    MdmOrganisationseinheit.class
})
public abstract class MdmPerson
    extends MdmCommon
{

    @XmlElement(required = true)
    protected String anredeCode;
    @XmlElement(required = true)
    protected String kennzeichenCode;
    @XmlElement(required = true)
    protected String name;
    protected String namenZusatz1;
    protected String namenZusatz2;
    protected String rabattklasseCode;
    @XmlElement(required = true)
    protected String sprachCode;

    /**
     * Gets the value of the anredeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnredeCode() {
        return anredeCode;
    }

    /**
     * Sets the value of the anredeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnredeCode(String value) {
        this.anredeCode = value;
    }

    /**
     * Gets the value of the kennzeichenCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKennzeichenCode() {
        return kennzeichenCode;
    }

    /**
     * Sets the value of the kennzeichenCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKennzeichenCode(String value) {
        this.kennzeichenCode = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the namenZusatz1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNamenZusatz1() {
        return namenZusatz1;
    }

    /**
     * Sets the value of the namenZusatz1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNamenZusatz1(String value) {
        this.namenZusatz1 = value;
    }

    /**
     * Gets the value of the namenZusatz2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNamenZusatz2() {
        return namenZusatz2;
    }

    /**
     * Sets the value of the namenZusatz2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNamenZusatz2(String value) {
        this.namenZusatz2 = value;
    }

    /**
     * Gets the value of the rabattklasseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRabattklasseCode() {
        return rabattklasseCode;
    }

    /**
     * Sets the value of the rabattklasseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRabattklasseCode(String value) {
        this.rabattklasseCode = value;
    }

    /**
     * Gets the value of the sprachCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSprachCode() {
        return sprachCode;
    }

    /**
     * Sets the value of the sprachCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSprachCode(String value) {
        this.sprachCode = value;
    }

}
