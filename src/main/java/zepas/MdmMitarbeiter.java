
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for mdmMitarbeiter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmMitarbeiter"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCommon"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="austrittsdatum" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="benutzerkennzeichen" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="benutzerKurzzeichen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bueronummer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eintrittsdatum" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="funktionsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="gaCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="hukId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mitarbeiterkennzeichen" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="personalnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="rentnerkennzeichen" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="rentnerPersonalnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmMitarbeiter", propOrder = {
    "austrittsdatum",
    "benutzerkennzeichen",
    "benutzerKurzzeichen",
    "bueronummer",
    "eintrittsdatum",
    "funktionsCode",
    "gaCode",
    "hukId",
    "mitarbeiterkennzeichen",
    "personalnummer",
    "rentnerkennzeichen",
    "rentnerPersonalnummer",
    "userId"
})
public class MdmMitarbeiter
    extends MdmCommon
{

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar austrittsdatum;
    protected Boolean benutzerkennzeichen;
    protected String benutzerKurzzeichen;
    protected String bueronummer;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eintrittsdatum;
    protected String funktionsCode;
    protected String gaCode;
    protected String hukId;
    protected Boolean mitarbeiterkennzeichen;
    protected Long personalnummer;
    protected Boolean rentnerkennzeichen;
    protected Long rentnerPersonalnummer;
    protected String userId;

    /**
     * Gets the value of the austrittsdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAustrittsdatum() {
        return austrittsdatum;
    }

    /**
     * Sets the value of the austrittsdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAustrittsdatum(XMLGregorianCalendar value) {
        this.austrittsdatum = value;
    }

    /**
     * Gets the value of the benutzerkennzeichen property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBenutzerkennzeichen() {
        return benutzerkennzeichen;
    }

    /**
     * Sets the value of the benutzerkennzeichen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBenutzerkennzeichen(Boolean value) {
        this.benutzerkennzeichen = value;
    }

    /**
     * Gets the value of the benutzerKurzzeichen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBenutzerKurzzeichen() {
        return benutzerKurzzeichen;
    }

    /**
     * Sets the value of the benutzerKurzzeichen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBenutzerKurzzeichen(String value) {
        this.benutzerKurzzeichen = value;
    }

    /**
     * Gets the value of the bueronummer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBueronummer() {
        return bueronummer;
    }

    /**
     * Sets the value of the bueronummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBueronummer(String value) {
        this.bueronummer = value;
    }

    /**
     * Gets the value of the eintrittsdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEintrittsdatum() {
        return eintrittsdatum;
    }

    /**
     * Sets the value of the eintrittsdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEintrittsdatum(XMLGregorianCalendar value) {
        this.eintrittsdatum = value;
    }

    /**
     * Gets the value of the funktionsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunktionsCode() {
        return funktionsCode;
    }

    /**
     * Sets the value of the funktionsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunktionsCode(String value) {
        this.funktionsCode = value;
    }

    /**
     * Gets the value of the gaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGaCode() {
        return gaCode;
    }

    /**
     * Sets the value of the gaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGaCode(String value) {
        this.gaCode = value;
    }

    /**
     * Gets the value of the hukId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHukId() {
        return hukId;
    }

    /**
     * Sets the value of the hukId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHukId(String value) {
        this.hukId = value;
    }

    /**
     * Gets the value of the mitarbeiterkennzeichen property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMitarbeiterkennzeichen() {
        return mitarbeiterkennzeichen;
    }

    /**
     * Sets the value of the mitarbeiterkennzeichen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMitarbeiterkennzeichen(Boolean value) {
        this.mitarbeiterkennzeichen = value;
    }

    /**
     * Gets the value of the personalnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPersonalnummer() {
        return personalnummer;
    }

    /**
     * Sets the value of the personalnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPersonalnummer(Long value) {
        this.personalnummer = value;
    }

    /**
     * Gets the value of the rentnerkennzeichen property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRentnerkennzeichen() {
        return rentnerkennzeichen;
    }

    /**
     * Sets the value of the rentnerkennzeichen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRentnerkennzeichen(Boolean value) {
        this.rentnerkennzeichen = value;
    }

    /**
     * Gets the value of the rentnerPersonalnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRentnerPersonalnummer() {
        return rentnerPersonalnummer;
    }

    /**
     * Sets the value of the rentnerPersonalnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRentnerPersonalnummer(Long value) {
        this.rentnerPersonalnummer = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

}
