
package zepas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmFullPerson complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmFullPerson"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mdmCompositePerson" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCompositePerson"/&gt;
 *         &lt;element name="mdmPersonenbeziehungen" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmPersonenbeziehung" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdmAdressen" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmAdresse" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdmKommunikationselemente" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmKommunikationselement" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdmVersandanweisungen" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmVersandanweisung" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdmZahlungsverbindungen" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmZahlungsverbindung" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdmFinmaRegisters" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmFinmaRegister" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdmZusatzinformationen" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmZusatzinformation" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdmCodesZurPerson" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCodeZurPerson" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmFullPerson", propOrder = {
    "mdmCompositePerson",
    "mdmPersonenbeziehungen",
    "mdmAdressen",
    "mdmKommunikationselemente",
    "mdmVersandanweisungen",
    "mdmZahlungsverbindungen",
    "mdmFinmaRegisters",
    "mdmZusatzinformationen",
    "mdmCodesZurPerson"
})
public class MdmFullPerson {

    @XmlElement(required = true)
    protected MdmCompositePerson mdmCompositePerson;
    protected List<MdmPersonenbeziehung> mdmPersonenbeziehungen;
    protected List<MdmAdresse> mdmAdressen;
    protected List<MdmKommunikationselement> mdmKommunikationselemente;
    protected List<MdmVersandanweisung> mdmVersandanweisungen;
    protected List<MdmZahlungsverbindung> mdmZahlungsverbindungen;
    protected List<MdmFinmaRegister> mdmFinmaRegisters;
    protected List<MdmZusatzinformation> mdmZusatzinformationen;
    protected List<MdmCodeZurPerson> mdmCodesZurPerson;

    /**
     * Gets the value of the mdmCompositePerson property.
     * 
     * @return
     *     possible object is
     *     {@link MdmCompositePerson }
     *     
     */
    public MdmCompositePerson getMdmCompositePerson() {
        return mdmCompositePerson;
    }

    /**
     * Sets the value of the mdmCompositePerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdmCompositePerson }
     *     
     */
    public void setMdmCompositePerson(MdmCompositePerson value) {
        this.mdmCompositePerson = value;
    }

    /**
     * Gets the value of the mdmPersonenbeziehungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdmPersonenbeziehungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdmPersonenbeziehungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdmPersonenbeziehung }
     * 
     * 
     */
    public List<MdmPersonenbeziehung> getMdmPersonenbeziehungen() {
        if (mdmPersonenbeziehungen == null) {
            mdmPersonenbeziehungen = new ArrayList<MdmPersonenbeziehung>();
        }
        return this.mdmPersonenbeziehungen;
    }

    /**
     * Gets the value of the mdmAdressen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdmAdressen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdmAdressen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdmAdresse }
     * 
     * 
     */
    public List<MdmAdresse> getMdmAdressen() {
        if (mdmAdressen == null) {
            mdmAdressen = new ArrayList<MdmAdresse>();
        }
        return this.mdmAdressen;
    }

    /**
     * Gets the value of the mdmKommunikationselemente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdmKommunikationselemente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdmKommunikationselemente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdmKommunikationselement }
     * 
     * 
     */
    public List<MdmKommunikationselement> getMdmKommunikationselemente() {
        if (mdmKommunikationselemente == null) {
            mdmKommunikationselemente = new ArrayList<MdmKommunikationselement>();
        }
        return this.mdmKommunikationselemente;
    }

    /**
     * Gets the value of the mdmVersandanweisungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdmVersandanweisungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdmVersandanweisungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdmVersandanweisung }
     * 
     * 
     */
    public List<MdmVersandanweisung> getMdmVersandanweisungen() {
        if (mdmVersandanweisungen == null) {
            mdmVersandanweisungen = new ArrayList<MdmVersandanweisung>();
        }
        return this.mdmVersandanweisungen;
    }

    /**
     * Gets the value of the mdmZahlungsverbindungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdmZahlungsverbindungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdmZahlungsverbindungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdmZahlungsverbindung }
     * 
     * 
     */
    public List<MdmZahlungsverbindung> getMdmZahlungsverbindungen() {
        if (mdmZahlungsverbindungen == null) {
            mdmZahlungsverbindungen = new ArrayList<MdmZahlungsverbindung>();
        }
        return this.mdmZahlungsverbindungen;
    }

    /**
     * Gets the value of the mdmFinmaRegisters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdmFinmaRegisters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdmFinmaRegisters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdmFinmaRegister }
     * 
     * 
     */
    public List<MdmFinmaRegister> getMdmFinmaRegisters() {
        if (mdmFinmaRegisters == null) {
            mdmFinmaRegisters = new ArrayList<MdmFinmaRegister>();
        }
        return this.mdmFinmaRegisters;
    }

    /**
     * Gets the value of the mdmZusatzinformationen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdmZusatzinformationen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdmZusatzinformationen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdmZusatzinformation }
     * 
     * 
     */
    public List<MdmZusatzinformation> getMdmZusatzinformationen() {
        if (mdmZusatzinformationen == null) {
            mdmZusatzinformationen = new ArrayList<MdmZusatzinformation>();
        }
        return this.mdmZusatzinformationen;
    }

    /**
     * Gets the value of the mdmCodesZurPerson property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdmCodesZurPerson property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdmCodesZurPerson().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdmCodeZurPerson }
     * 
     * 
     */
    public List<MdmCodeZurPerson> getMdmCodesZurPerson() {
        if (mdmCodesZurPerson == null) {
            mdmCodesZurPerson = new ArrayList<MdmCodeZurPerson>();
        }
        return this.mdmCodesZurPerson;
    }

}
