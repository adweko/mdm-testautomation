
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for mdmUnternehmung complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmUnternehmung"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmPerson"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aufloesungsdatum" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="mehrwertssteuernummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="nogaCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rechtsformCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="stiftungstext1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="stiftungstext2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="unternehmensId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="gegenparteiId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmUnternehmung", propOrder = {
    "aufloesungsdatum",
    "mehrwertssteuernummer",
    "nogaCode",
    "rechtsformCode",
    "stiftungstext1",
    "stiftungstext2",
    "unternehmensId",
    "gegenparteiId"
})
public class MdmUnternehmung
    extends MdmPerson
{

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar aufloesungsdatum;
    protected Long mehrwertssteuernummer;
    @XmlElement(required = true)
    protected String nogaCode;
    @XmlElement(required = true)
    protected String rechtsformCode;
    protected String stiftungstext1;
    protected String stiftungstext2;
    protected String unternehmensId;
    protected String gegenparteiId;

    /**
     * Gets the value of the aufloesungsdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAufloesungsdatum() {
        return aufloesungsdatum;
    }

    /**
     * Sets the value of the aufloesungsdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAufloesungsdatum(XMLGregorianCalendar value) {
        this.aufloesungsdatum = value;
    }

    /**
     * Gets the value of the mehrwertssteuernummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMehrwertssteuernummer() {
        return mehrwertssteuernummer;
    }

    /**
     * Sets the value of the mehrwertssteuernummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMehrwertssteuernummer(Long value) {
        this.mehrwertssteuernummer = value;
    }

    /**
     * Gets the value of the nogaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNogaCode() {
        return nogaCode;
    }

    /**
     * Sets the value of the nogaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNogaCode(String value) {
        this.nogaCode = value;
    }

    /**
     * Gets the value of the rechtsformCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRechtsformCode() {
        return rechtsformCode;
    }

    /**
     * Sets the value of the rechtsformCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRechtsformCode(String value) {
        this.rechtsformCode = value;
    }

    /**
     * Gets the value of the stiftungstext1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStiftungstext1() {
        return stiftungstext1;
    }

    /**
     * Sets the value of the stiftungstext1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStiftungstext1(String value) {
        this.stiftungstext1 = value;
    }

    /**
     * Gets the value of the stiftungstext2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStiftungstext2() {
        return stiftungstext2;
    }

    /**
     * Sets the value of the stiftungstext2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStiftungstext2(String value) {
        this.stiftungstext2 = value;
    }

    /**
     * Gets the value of the unternehmensId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnternehmensId() {
        return unternehmensId;
    }

    /**
     * Sets the value of the unternehmensId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnternehmensId(String value) {
        this.unternehmensId = value;
    }

    /**
     * Gets the value of the gegenparteiId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGegenparteiId() {
        return gegenparteiId;
    }

    /**
     * Sets the value of the gegenparteiId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGegenparteiId(String value) {
        this.gegenparteiId = value;
    }

}
