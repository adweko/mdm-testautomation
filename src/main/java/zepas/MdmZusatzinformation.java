
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmZusatzinformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmZusatzinformation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCommon"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="infoartCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="zuordnung" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="personenLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmZusatzinformation", propOrder = {
    "infoartCode",
    "zuordnung",
    "personenLaufnummer"
})
public class MdmZusatzinformation
    extends MdmCommon
{

    protected String infoartCode;
    protected Boolean zuordnung;
    protected Long personenLaufnummer;

    /**
     * Gets the value of the infoartCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoartCode() {
        return infoartCode;
    }

    /**
     * Sets the value of the infoartCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoartCode(String value) {
        this.infoartCode = value;
    }

    /**
     * Gets the value of the zuordnung property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isZuordnung() {
        return zuordnung;
    }

    /**
     * Sets the value of the zuordnung property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setZuordnung(Boolean value) {
        this.zuordnung = value;
    }

    /**
     * Gets the value of the personenLaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPersonenLaufnummer() {
        return personenLaufnummer;
    }

    /**
     * Sets the value of the personenLaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPersonenLaufnummer(Long value) {
        this.personenLaufnummer = value;
    }

}
