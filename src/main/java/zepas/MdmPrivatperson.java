
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for mdmPrivatperson complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmPrivatperson"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmPerson"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="vorname" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="akademischerTitelCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="anstellungsVerhaeltnisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="aufenthaltsbewilligungCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="beruflicheTaetigkeitCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="buergerort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="geburtsdatum" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="geburtsname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="geschlechtCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nationalitaetCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sonstigeTitel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sozialversicherungsnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="todesdatum" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="zivilstandsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mdmMitarbeiter" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmMitarbeiter" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmPrivatperson", propOrder = {
    "vorname",
    "akademischerTitelCode",
    "anstellungsVerhaeltnisCode",
    "aufenthaltsbewilligungCode",
    "beruflicheTaetigkeitCode",
    "buergerort",
    "geburtsdatum",
    "geburtsname",
    "geschlechtCode",
    "nationalitaetCode",
    "sonstigeTitel",
    "sozialversicherungsnummer",
    "todesdatum",
    "zivilstandsCode",
    "mdmMitarbeiter"
})
public class MdmPrivatperson
    extends MdmPerson
{

    @XmlElement(required = true)
    protected String vorname;
    protected String akademischerTitelCode;
    protected String anstellungsVerhaeltnisCode;
    protected String aufenthaltsbewilligungCode;
    protected String beruflicheTaetigkeitCode;
    protected String buergerort;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar geburtsdatum;
    protected String geburtsname;
    protected String geschlechtCode;
    protected String nationalitaetCode;
    protected String sonstigeTitel;
    protected Long sozialversicherungsnummer;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar todesdatum;
    protected String zivilstandsCode;
    protected MdmMitarbeiter mdmMitarbeiter;

    /**
     * Gets the value of the vorname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Sets the value of the vorname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVorname(String value) {
        this.vorname = value;
    }

    /**
     * Gets the value of the akademischerTitelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAkademischerTitelCode() {
        return akademischerTitelCode;
    }

    /**
     * Sets the value of the akademischerTitelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAkademischerTitelCode(String value) {
        this.akademischerTitelCode = value;
    }

    /**
     * Gets the value of the anstellungsVerhaeltnisCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnstellungsVerhaeltnisCode() {
        return anstellungsVerhaeltnisCode;
    }

    /**
     * Sets the value of the anstellungsVerhaeltnisCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnstellungsVerhaeltnisCode(String value) {
        this.anstellungsVerhaeltnisCode = value;
    }

    /**
     * Gets the value of the aufenthaltsbewilligungCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAufenthaltsbewilligungCode() {
        return aufenthaltsbewilligungCode;
    }

    /**
     * Sets the value of the aufenthaltsbewilligungCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAufenthaltsbewilligungCode(String value) {
        this.aufenthaltsbewilligungCode = value;
    }

    /**
     * Gets the value of the beruflicheTaetigkeitCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeruflicheTaetigkeitCode() {
        return beruflicheTaetigkeitCode;
    }

    /**
     * Sets the value of the beruflicheTaetigkeitCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeruflicheTaetigkeitCode(String value) {
        this.beruflicheTaetigkeitCode = value;
    }

    /**
     * Gets the value of the buergerort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuergerort() {
        return buergerort;
    }

    /**
     * Sets the value of the buergerort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuergerort(String value) {
        this.buergerort = value;
    }

    /**
     * Gets the value of the geburtsdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGeburtsdatum() {
        return geburtsdatum;
    }

    /**
     * Sets the value of the geburtsdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGeburtsdatum(XMLGregorianCalendar value) {
        this.geburtsdatum = value;
    }

    /**
     * Gets the value of the geburtsname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeburtsname() {
        return geburtsname;
    }

    /**
     * Sets the value of the geburtsname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeburtsname(String value) {
        this.geburtsname = value;
    }

    /**
     * Gets the value of the geschlechtCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeschlechtCode() {
        return geschlechtCode;
    }

    /**
     * Sets the value of the geschlechtCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeschlechtCode(String value) {
        this.geschlechtCode = value;
    }

    /**
     * Gets the value of the nationalitaetCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalitaetCode() {
        return nationalitaetCode;
    }

    /**
     * Sets the value of the nationalitaetCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalitaetCode(String value) {
        this.nationalitaetCode = value;
    }

    /**
     * Gets the value of the sonstigeTitel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSonstigeTitel() {
        return sonstigeTitel;
    }

    /**
     * Sets the value of the sonstigeTitel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSonstigeTitel(String value) {
        this.sonstigeTitel = value;
    }

    /**
     * Gets the value of the sozialversicherungsnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSozialversicherungsnummer() {
        return sozialversicherungsnummer;
    }

    /**
     * Sets the value of the sozialversicherungsnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSozialversicherungsnummer(Long value) {
        this.sozialversicherungsnummer = value;
    }

    /**
     * Gets the value of the todesdatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTodesdatum() {
        return todesdatum;
    }

    /**
     * Sets the value of the todesdatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTodesdatum(XMLGregorianCalendar value) {
        this.todesdatum = value;
    }

    /**
     * Gets the value of the zivilstandsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZivilstandsCode() {
        return zivilstandsCode;
    }

    /**
     * Sets the value of the zivilstandsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZivilstandsCode(String value) {
        this.zivilstandsCode = value;
    }

    /**
     * Gets the value of the mdmMitarbeiter property.
     * 
     * @return
     *     possible object is
     *     {@link MdmMitarbeiter }
     *     
     */
    public MdmMitarbeiter getMdmMitarbeiter() {
        return mdmMitarbeiter;
    }

    /**
     * Sets the value of the mdmMitarbeiter property.
     * 
     * @param value
     *     allowed object is
     *     {@link MdmMitarbeiter }
     *     
     */
    public void setMdmMitarbeiter(MdmMitarbeiter value) {
        this.mdmMitarbeiter = value;
    }

}
