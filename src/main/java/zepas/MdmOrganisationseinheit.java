
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmOrganisationseinheit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmOrganisationseinheit"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmPerson"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="oekurzname" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="oecode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="oestufecode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmOrganisationseinheit", propOrder = {
    "oekurzname",
    "oecode",
    "oestufecode"
})
public class MdmOrganisationseinheit
    extends MdmPerson
{

    @XmlElement(required = true)
    protected String oekurzname;
    @XmlElement(required = true)
    protected String oecode;
    @XmlElement(required = true)
    protected String oestufecode;

    /**
     * Gets the value of the oekurzname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOekurzname() {
        return oekurzname;
    }

    /**
     * Sets the value of the oekurzname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOekurzname(String value) {
        this.oekurzname = value;
    }

    /**
     * Gets the value of the oecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOecode() {
        return oecode;
    }

    /**
     * Sets the value of the oecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOecode(String value) {
        this.oecode = value;
    }

    /**
     * Gets the value of the oestufecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOestufecode() {
        return oestufecode;
    }

    /**
     * Sets the value of the oestufecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOestufecode(String value) {
        this.oestufecode = value;
    }

}
