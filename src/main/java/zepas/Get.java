
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for get complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="get"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="personenLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="stichtag" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="wsControl" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}wsControl" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "get", propOrder = {
    "personenLaufnummer",
    "stichtag",
    "wsControl"
})
public class Get {

    protected Long personenLaufnummer;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar stichtag;
    protected WsControl wsControl;

    /**
     * Gets the value of the personenLaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPersonenLaufnummer() {
        return personenLaufnummer;
    }

    /**
     * Sets the value of the personenLaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPersonenLaufnummer(Long value) {
        this.personenLaufnummer = value;
    }

    /**
     * Gets the value of the stichtag property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStichtag() {
        return stichtag;
    }

    /**
     * Sets the value of the stichtag property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStichtag(XMLGregorianCalendar value) {
        this.stichtag = value;
    }

    /**
     * Gets the value of the wsControl property.
     * 
     * @return
     *     possible object is
     *     {@link WsControl }
     *     
     */
    public WsControl getWsControl() {
        return wsControl;
    }

    /**
     * Sets the value of the wsControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link WsControl }
     *     
     */
    public void setWsControl(WsControl value) {
        this.wsControl = value;
    }

}
