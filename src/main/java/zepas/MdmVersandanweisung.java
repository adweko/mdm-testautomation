
package zepas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmVersandanweisung complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmVersandanweisung"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCommon"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="personenLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="rolleLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="toPersonenLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="anweisungsartCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="adresseLaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="spracheCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="behandlungsCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="adressformCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="briefanrede" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vazusatztext1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vazusatztext2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="kommunikationselementlaufnummer" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmVersandanweisung", propOrder = {
    "personenLaufnummer",
    "rolleLaufnummer",
    "toPersonenLaufnummer",
    "anweisungsartCode",
    "adresseLaufnummer",
    "spracheCode",
    "behandlungsCode",
    "adressformCode",
    "briefanrede",
    "vazusatztext1",
    "vazusatztext2",
    "kommunikationselementlaufnummer"
})
public class MdmVersandanweisung
    extends MdmCommon
{

    protected Long personenLaufnummer;
    protected Long rolleLaufnummer;
    protected Long toPersonenLaufnummer;
    protected String anweisungsartCode;
    protected Long adresseLaufnummer;
    protected String spracheCode;
    protected String behandlungsCode;
    protected String adressformCode;
    protected String briefanrede;
    protected String vazusatztext1;
    protected String vazusatztext2;
    protected Long kommunikationselementlaufnummer;

    /**
     * Gets the value of the personenLaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPersonenLaufnummer() {
        return personenLaufnummer;
    }

    /**
     * Sets the value of the personenLaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPersonenLaufnummer(Long value) {
        this.personenLaufnummer = value;
    }

    /**
     * Gets the value of the rolleLaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRolleLaufnummer() {
        return rolleLaufnummer;
    }

    /**
     * Sets the value of the rolleLaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRolleLaufnummer(Long value) {
        this.rolleLaufnummer = value;
    }

    /**
     * Gets the value of the toPersonenLaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getToPersonenLaufnummer() {
        return toPersonenLaufnummer;
    }

    /**
     * Sets the value of the toPersonenLaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setToPersonenLaufnummer(Long value) {
        this.toPersonenLaufnummer = value;
    }

    /**
     * Gets the value of the anweisungsartCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnweisungsartCode() {
        return anweisungsartCode;
    }

    /**
     * Sets the value of the anweisungsartCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnweisungsartCode(String value) {
        this.anweisungsartCode = value;
    }

    /**
     * Gets the value of the adresseLaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAdresseLaufnummer() {
        return adresseLaufnummer;
    }

    /**
     * Sets the value of the adresseLaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAdresseLaufnummer(Long value) {
        this.adresseLaufnummer = value;
    }

    /**
     * Gets the value of the spracheCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpracheCode() {
        return spracheCode;
    }

    /**
     * Sets the value of the spracheCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpracheCode(String value) {
        this.spracheCode = value;
    }

    /**
     * Gets the value of the behandlungsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBehandlungsCode() {
        return behandlungsCode;
    }

    /**
     * Sets the value of the behandlungsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBehandlungsCode(String value) {
        this.behandlungsCode = value;
    }

    /**
     * Gets the value of the adressformCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdressformCode() {
        return adressformCode;
    }

    /**
     * Sets the value of the adressformCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdressformCode(String value) {
        this.adressformCode = value;
    }

    /**
     * Gets the value of the briefanrede property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBriefanrede() {
        return briefanrede;
    }

    /**
     * Sets the value of the briefanrede property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBriefanrede(String value) {
        this.briefanrede = value;
    }

    /**
     * Gets the value of the vazusatztext1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVazusatztext1() {
        return vazusatztext1;
    }

    /**
     * Sets the value of the vazusatztext1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVazusatztext1(String value) {
        this.vazusatztext1 = value;
    }

    /**
     * Gets the value of the vazusatztext2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVazusatztext2() {
        return vazusatztext2;
    }

    /**
     * Sets the value of the vazusatztext2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVazusatztext2(String value) {
        this.vazusatztext2 = value;
    }

    /**
     * Gets the value of the kommunikationselementlaufnummer property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getKommunikationselementlaufnummer() {
        return kommunikationselementlaufnummer;
    }

    /**
     * Sets the value of the kommunikationselementlaufnummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setKommunikationselementlaufnummer(Long value) {
        this.kommunikationselementlaufnummer = value;
    }

}
