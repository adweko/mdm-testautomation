
package zepas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mdmKommunikationselement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mdmKommunikationselement"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmCommon"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="kommentar" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="prioritaet" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="elementtypCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="kommkanalCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mdmVerwendungen" type="{http://v5.soap.wst.ws.zepasweb.helvetia.ch/}mdmKommelementVerwendung" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mdmKommunikationselement", propOrder = {
    "kommentar",
    "value",
    "prioritaet",
    "elementtypCode",
    "kommkanalCode",
    "mdmVerwendungen"
})
public class MdmKommunikationselement
    extends MdmCommon
{

    @XmlElement(required = true)
    protected String kommentar;
    @XmlElement(required = true)
    protected String value;
    protected Integer prioritaet;
    @XmlElement(required = true)
    protected String elementtypCode;
    @XmlElement(required = true)
    protected String kommkanalCode;
    @XmlElement(required = true)
    protected List<MdmKommelementVerwendung> mdmVerwendungen;

    /**
     * Gets the value of the kommentar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKommentar() {
        return kommentar;
    }

    /**
     * Sets the value of the kommentar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKommentar(String value) {
        this.kommentar = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the prioritaet property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrioritaet() {
        return prioritaet;
    }

    /**
     * Sets the value of the prioritaet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrioritaet(Integer value) {
        this.prioritaet = value;
    }

    /**
     * Gets the value of the elementtypCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementtypCode() {
        return elementtypCode;
    }

    /**
     * Sets the value of the elementtypCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementtypCode(String value) {
        this.elementtypCode = value;
    }

    /**
     * Gets the value of the kommkanalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKommkanalCode() {
        return kommkanalCode;
    }

    /**
     * Sets the value of the kommkanalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKommkanalCode(String value) {
        this.kommkanalCode = value;
    }

    /**
     * Gets the value of the mdmVerwendungen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdmVerwendungen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdmVerwendungen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MdmKommelementVerwendung }
     * 
     * 
     */
    public List<MdmKommelementVerwendung> getMdmVerwendungen() {
        if (mdmVerwendungen == null) {
            mdmVerwendungen = new ArrayList<MdmKommelementVerwendung>();
        }
        return this.mdmVerwendungen;
    }

}
