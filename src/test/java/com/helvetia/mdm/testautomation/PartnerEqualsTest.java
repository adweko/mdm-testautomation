package com.helvetia.mdm.testautomation;

import com.helvetia.mdm.testautomation.model.NaturalPerson;
import com.helvetia.mdm.testautomation.model.Partner;
import com.helvetia.mdm.testautomation.model.Partners;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PartnerEqualsTest {

    @Test
    public void testEquals() {
        Partner partner1 = NaturalPerson.builder().build();
        Partner partner2 = NaturalPerson.builder().build();
        Assertions.assertTrue(partner1.equals(partner2));
    }


}
