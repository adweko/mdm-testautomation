package com.helvetia.mdm.testautomation.creation.zepas.config;

import io.quarkus.test.Mock;
import lombok.Data;

import javax.enterprise.context.ApplicationScoped;
import java.io.FileInputStream;
import java.io.IOException;

@Mock
@Data
@ApplicationScoped
public class MockWsConfiguration extends WsConfiguration {


    @Override
    public void setup() throws IOException {
        loggedInUser = "CHTI-MDM";
        samltokenSignerKeystore = new FileInputStream("src/main/resources/security/TKSIGN_DEVL_MDM.jks").readAllBytes();
        samltokenSignerPassword = "hib1t3juM79Jy6Vk21el";
    }

}