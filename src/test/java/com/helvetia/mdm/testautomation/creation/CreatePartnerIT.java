package com.helvetia.mdm.testautomation.creation;


import changePartner.com.helvetia.custom.changeMdmResponse;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.PartnerReplikationExpServicePortType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereNatPersonRequestType;
import com.adcubum.syrius.api.partnermgmt.partnerdatenverw.process.partnerreplikationexp.v0.schema.RepliziereResponseType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.helvetia.mdm.testautomation.creation.syrius.PartnerSyrius;
import com.helvetia.mdm.testautomation.creation.syrius.config.SyriusConfig;
import com.helvetia.mdm.testautomation.creation.zepas.ZepasPartners;
import com.helvetia.mdm.testautomation.creation.zepas.config.HelperWsClient;
import com.helvetia.mdm.testautomation.creation.zepas.config.MockWsConfiguration;
import com.helvetia.mdm.testautomation.kafka.KafkaHelper;
import com.helvetia.mdm.testautomation.model.LegalPerson;
import com.helvetia.mdm.testautomation.model.NaturalPerson;
import com.helvetia.mdm.testautomation.model.Partner;
import com.helvetia.mdm.testautomation.model.Partners;
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.quarkus.test.junit.QuarkusTest;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import statePartner.json.readFullPartnerAllVersionsFromDateResponse;
import zepas.*;

import javax.inject.Inject;
import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.*;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.fail;

@QuarkusTest
public class CreatePartnerIT {


    static {
        System.setProperty("quarkus.profile", "devl");
        timeAtStartOfTest = LocalDateTime.now();
    }

    static Map<TopicPartition, Long> changeTopicStartingPoints;
    static Map<TopicPartition, Long> stateTopicStartingPoints;

    @Inject
    MockWsConfiguration wsConfiguration;

    @Inject
    SyriusConfig syriusConfig;


    private static final String CHANGE_TOPIC = "ch-mdm.change.avro";
    private static final String STATE_TOPIC = "ch-mdm.state.avro";

    static LocalDateTime timeAtStartOfTest;

    MdmFullPersonV50_Service service = new MdmFullPersonV50_Service(
            getClass().getResource("schemata/wsdl/zepas/MdmFullPerson_v5_0.wsdl"));
    static final QName PORT_NAME = new QName("http://v5.soap.wst.ws.zepasweb.helvetia.ch/",
            "MdmFullPerson_v5_0Port");
    MdmFullPersonV50 port;
    Holder<Long> personenLaufNummer = new Holder<>(0L);
    Holder<List<Status>> status;
    WsControl wsControl;

    /**
     * create a file called src/main/resources/cert/passwords.properties with the content
     * SSL_KEYSTORE_PASSWORD=your-password
     * SSL_TRUSTSTORE_PASSWORD=your-password
     * the available keystores are those of the DEVL cvboost kafka agent, so if you know the passwords for them
     * you can use them.
     * Else, you will have to apply for your own via https://wiki.helvetia.group/x/iBktEw
     */
    @BeforeAll
    public static void beforeAll() throws IOException {
        Properties passwords = new Properties();
        passwords.load(Files.newInputStream(Paths.get("src/main/resources/cert/passwords.properties")));
        passwords.forEach((k, v) -> System.setProperty(k.toString(), v.toString()));

        changeTopicStartingPoints = KafkaHelper.getEndOffsets(CHANGE_TOPIC);
        stateTopicStartingPoints = KafkaHelper.getEndOffsets(STATE_TOPIC);
    }

    @BeforeEach
    public void setup() {
        personenLaufNummer = new Holder<>(0L);
    }

    @Test
    public void testCreateSyrius() throws Exception {
        final Faker faker = new Faker();
        final String firstname = faker.name().firstName();
        final String lastName = faker.name().lastName();
        NaturalPerson originalPartner = NaturalPerson.builder()
                .name(firstname)
                .surname(lastName)
                .build();
        final PartnerSyrius createPartner = PartnerSyrius.fromPartner(originalPartner);
        System.out.println("will create " + firstname + " " + lastName);

        PartnerReplikationExpServicePortType partnerReplikationExpServicePortType = syriusConfig.getReplikationExpService().getPartnerReplikationExpServiceV0();
        ((BindingProvider) partnerReplikationExpServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, syriusConfig.getEndpoint());

        syriusConfig.getSslConfig().setupSSL(partnerReplikationExpServicePortType);
        final RepliziereNatPersonRequestType repliziereNatPersonRequestType = createPartner.getRepliziereNatPersonRequestType();
        System.out.println(objectToXml(repliziereNatPersonRequestType));
        RepliziereResponseType repliziereResponseType = partnerReplikationExpServicePortType.repliziereNatPerson(repliziereNatPersonRequestType);
        System.out.println(repliziereResponseType.getReplikationErfolgreich());
        validateArrivedInKafkaTopics(originalPartner);

    }

    @Test
    public void testCreateZEPAS() throws ZepaswebServiceException_Exception, InterruptedException {
        NaturalPerson originalPartner = NaturalPerson.builder().build();
        final MdmFullPerson createPartner = ZepasPartners.fromPartner(originalPartner);
        System.out.println("kupeLaufnummer is " + personenLaufNummer.value);
        initPort();
        port.synchronize(createPartner, HelperWsClient.formatLocalDate(LocalDate.now()), wsControl, personenLaufNummer, status);
        HelperWsClient.printStatus(status.value);
        System.out.println("kupeLaufnummer is " + personenLaufNummer.value);

        validateArrivedInKafkaTopics(originalPartner);

        // TODO: update not working, need to check with ZEPAS
//        NaturalPerson mutatedPartner = originalPartner.toBuilder().language(Language.FR).build();
//        initPort();
//        MdmFullPerson zepasMutated = ZepasPartners.fromPartner(mutatedPartner);
//        setLaufnummerOnFullPerson(zepasMutated);
//        port.synchronize(zepasMutated, HelperWsClient.formatLocalDate(LocalDate.now()), wsControl, personenLaufNummer, status);
//        HelperWsClient.printStatus(status.value);
//        System.out.println("kupeLaufnummer is " + personenLaufNummer.value);
    }

    private void validateArrivedInKafkaTopics(Partner partner) {
        final boolean arrivedInStateTopic = validateArrivedInStateTopic(partner);
        final boolean arrivedInChangeTopic = validateArrivedInChangeTopic(partner);
        if (!arrivedInChangeTopic && !arrivedInStateTopic) {
            fail("partner did not arrive in any topic");
        } else if (!arrivedInChangeTopic) {
            fail("partner did not arrive in change topic");
        } else if (!arrivedInStateTopic) {
            fail("partner did not arrive in state topic");
        } else {
            // success
        }

    }

    private boolean validateArrivedInChangeTopic(Partner partner) {
        Consumer<String, changeMdmResponse> consumer = new KafkaConsumer<>(KafkaHelper.getConsumerConfig(true));
        consumer.assign(changeTopicStartingPoints.keySet());
        changeTopicStartingPoints.forEach((consumer::seek));

        Duration maxPollTime = Duration.ofSeconds(20L);
        final LocalDateTime startPollingTime = LocalDateTime.now();
        while (Duration.between(startPollingTime, LocalDateTime.now()).compareTo(maxPollTime) < 0) {
            final ConsumerRecords<String, changeMdmResponse> records = consumer.poll(Duration.ofSeconds(1L));
            for (ConsumerRecord<String, changeMdmResponse> record : records) {
                System.out.println(partner.toString());
                System.out.println(Partners.fromChangePartner(record.value()).toString());
                System.out.println("are equal? " + partner.equals(Partners.fromChangePartner(record.value())) );


                final Partner partnerFromRecord = Partners.fromChangePartner(record.value());
                if (partnerFromRecord.equals(partner)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean validateArrivedInStateTopic(Partner partner) {
        Consumer<String, readFullPartnerAllVersionsFromDateResponse> consumer = new KafkaConsumer<>(KafkaHelper.getConsumerConfig(true));
        consumer.assign(stateTopicStartingPoints.keySet());
        stateTopicStartingPoints.forEach((consumer::seek));

        Duration maxPollTime = Duration.ofSeconds(20L);
        final LocalDateTime startPollingTime = LocalDateTime.now();
        while (Duration.between(startPollingTime, LocalDateTime.now()).compareTo(maxPollTime) < 0) {
            final ConsumerRecords<String, readFullPartnerAllVersionsFromDateResponse> records = consumer.poll(Duration.ofSeconds(1L));
            for (ConsumerRecord<String, readFullPartnerAllVersionsFromDateResponse> record : records) {
                System.out.println(partner.toString());
                System.out.println(Partners.fromStatePartner(record.value()).toString());
                System.out.println("are equal? " + partner.equals(Partners.fromStatePartner(record.value())) );

                final Partner partnerFromRecord = Partners.fromStatePartner(record.value());
                if (partnerFromRecord.equals(partner)) {
                    return true;
                }
            }
        }
        return false;
    }


    private void setLaufnummerOnFullPerson(MdmFullPerson mdmFullPerson) {
        mdmFullPerson.getMdmCompositePerson().setMdmPersonenLaufnummer(personenLaufNummer.value);
        if (mdmFullPerson.getMdmCompositePerson().getMdmPrivatperson() != null) {
            mdmFullPerson.getMdmCompositePerson().getMdmPrivatperson().setLaufnummer(personenLaufNummer.value);
        }
        if (mdmFullPerson.getMdmCompositePerson().getMdmUnternehmung() != null) {
            mdmFullPerson.getMdmCompositePerson().getMdmUnternehmung().setLaufnummer(personenLaufNummer.value);
        }
        if (mdmFullPerson.getMdmCompositePerson().getMdmOrganisationseinheit() != null) {
            mdmFullPerson.getMdmCompositePerson().getMdmOrganisationseinheit().setLaufnummer(personenLaufNummer.value);
        }
        mdmFullPerson.getMdmAdressen().forEach(e -> e.setLaufnummer(personenLaufNummer.value));
        mdmFullPerson.getMdmKommunikationselemente().forEach(e -> e.setLaufnummer(personenLaufNummer.value));
        mdmFullPerson.getMdmCodesZurPerson().forEach(e -> e.setLaufnummer(personenLaufNummer.value));
    }

    private void initPort() {
        try {
            status = new Holder<>();
            port = service.getPort(PORT_NAME, MdmFullPersonV50.class);
            wsConfiguration.setup();
            HelperWsClient.prepareWs(port, wsConfiguration.getWsUrlMdmFullPerson(),
                    wsConfiguration);
            wsControl = new WsControl();
            wsControl.setUserId(wsConfiguration.getLoggedInUser());
            wsControl.setLanguage("de");
        } catch (Exception e) {
            throw new RuntimeException("ZEPAS connection failed", e);
        }
    }


    public Properties getConsumerConfig() {
        return KafkaHelper.getConsumerConfig(false);
    }



    private static String objectToXml(Object object) {
        try {
            StringWriter sw = new StringWriter();
            JAXB.marshal(object, sw);
            String result = sw.toString().replace("\n", "").replace("\r", "").replace("\t", "");
            while (result.contains(" <")) {
                result = result.replace(" <", "<");
            }
            while (result.contains("> ")) {
                result = result.replace("> ", ">");
            }
            return result;
        } catch (Exception ex) {
            return "xml not valid:" + ex.getMessage();
        }
    }

    private static String classToJson(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return "object not serializable";
        }
    }

}
