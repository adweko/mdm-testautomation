package com.helvetia.mdm.testautomation;

import com.helvetia.mdm.testautomation.model.Address;
import com.helvetia.mdm.testautomation.model.NaturalPerson;
import com.helvetia.mdm.testautomation.model.Partner;
import com.helvetia.mdm.testautomation.model.communicationelement.FaxNumber;
import com.helvetia.mdm.testautomation.model.communicationelement.TelephoneNumber;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class HelloTest {

    @Test
    public void hello() {


        Partner testPartner = NaturalPerson.builder()
                .employmentStatus("unemployed")
                .communicationElements(
                        Arrays.asList(
                                TelephoneNumber.builder().telephoneNumber("0160549846").build(),
                                FaxNumber.builder().faxNumber("848948614").build()
                        )
                )
                .mainAddress(
                        Address.builder()
                                .country("Russia")
                                .build()
                )
                .build();

        System.out.println(testPartner);


    }

}