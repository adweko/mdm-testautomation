package com.helvetia.mdm.testautomation;

import com.datamirror.ts.kafka.txconsistentconsumer.TxConsistentConsumerException;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.Bookmark;
import com.datamirror.ts.kafka.txconsistentconsumer.bookmark.PublicTxConsistentConsumerBookmarkV1;
import com.helvetia.mdm.testautomation.kafka.KafkaHelper;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.Deserializer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.*;
import java.time.temporal.TemporalAccessor;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public class connectKafkaIT {

    /**
     * create a file called src/main/resources/cert/passwords.properties with the content
     * SSL_KEYSTORE_PASSWORD=your-password
     * SSL_TRUSTSTORE_PASSWORD=your-password
     * The available keystores are those of the DEVL cvboost kafka agent, so if you know the passwords for them
     * you can use them here and do not have to replace the stores.
     * Else, you will have to apply for your own via https://wiki.helvetia.group/x/iBktEw
     */
    @BeforeAll
    public static void loadEnvvarsFromFile() throws IOException {
        Properties passwords = new Properties();
        passwords.load(Files.newInputStream(Paths.get("src/main/resources/cert/passwords.properties")));
        passwords.forEach((k, v) -> System.setProperty(k.toString(), v.toString()));
    }



    @Test
    public void testConnect() {
        Properties consumerConfig = KafkaHelper.getConsumerConfig(false);
        Consumer<String, GenericRecord> consumer = new KafkaConsumer<>(consumerConfig);
        consumer.listTopics().keySet().forEach(System.out::println);

    }

    @Test
    public void consumeSomeTopic() {
        Properties consumerConfig = KafkaHelper.getConsumerConfig(false);
        Consumer<byte[], byte[]> consumer = new KafkaConsumer<>(consumerConfig);

        final String topic = "CDC.zdkub000.z00.tkuko_kommelement";
        Map<TopicPartition, Long> endOffsets = KafkaHelper.getEndOffsets(topic);
        consumer.assign(endOffsets.keySet());

        final ZonedDateTime zonedDateTime = LocalDateTime.of(2020, 8, 6, 15, 0, 0, 0).atZone(ZoneId.of("Europe/Berlin"));

        long timestampToSearchFor = Instant.from(zonedDateTime).toEpochMilli();
        final Map<TopicPartition, Long> topicPartitionToDesiredTimestamp = endOffsets.keySet().stream()
                .collect(Collectors.toMap(topicPartition -> topicPartition, topicPartition -> timestampToSearchFor));
        final Map<TopicPartition, OffsetAndTimestamp> topicPartitionToDesiredOffsetAndTimestamp = consumer.offsetsForTimes(topicPartitionToDesiredTimestamp);

        topicPartitionToDesiredOffsetAndTimestamp.forEach((tp, oat) -> consumer.seek(tp, 381344));

        while (true) {
            consumer.poll(Duration.ofSeconds(1L)).forEach(record -> {
                System.out.println("timestamp: " + record.timestamp());
                System.out.println("offset: " + record.offset());
                System.out.println("key: " + record.key());
                System.out.println("value: " + record.value());
            });
        }

    }

    @Test
    public void consumeBookmarkTopic() {
        Properties consumerConfig = KafkaHelper.getConsumerConfigWithoutSerdes();
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        Consumer<byte[], byte[]> consumer = new KafkaConsumer<>(consumerConfig);

        final String topic = "CDC-KAFKATEST1-ZEQ2KAFK-commitstream"; //"CDC.zdkub000.z00.tkuko_kommelement"
        Map<TopicPartition, Long> endOffsets = KafkaHelper.getEndOffsets(topic);
        consumer.assign(endOffsets.keySet());

        final ZonedDateTime zonedDateTime = LocalDateTime.of(2020, 8, 6, 15, 0, 0, 0).atZone(ZoneId.of("Europe/Berlin"));

        long timestampToSearchFor = Instant.from(zonedDateTime).toEpochMilli();
        final Map<TopicPartition, Long> topicPartitionToDesiredTimestamp = endOffsets.keySet().stream()
                .collect(Collectors.toMap(topicPartition -> topicPartition, topicPartition -> timestampToSearchFor));
        final Map<TopicPartition, OffsetAndTimestamp> topicPartitionToDesiredOffsetAndTimestamp = consumer.offsetsForTimes(topicPartitionToDesiredTimestamp);

        topicPartitionToDesiredOffsetAndTimestamp.forEach((tp, oat) -> consumer.seek(tp, oat.offset()));

        final BookmarkDeserializer bookmarkDeserializer = new BookmarkDeserializer();

        while (true) {
            consumer.poll(Duration.ofSeconds(1L)).forEach(record -> {
                System.out.println("timestamp: " + record.timestamp());
                System.out.println("key: " + record.key());
                System.out.println("value: " + record.value());
                System.out.println("deserialized: " + bookmarkDeserializer.deserialize("topic", record.value()));
                System.out.println("bookmark: " + bookmarkDeserializer.deserialize("topic", record.value()).get());
            });
        }

    }
//com.com.datamirror.ts.kafka.txconsistentconsumer.bookmark
    class BookmarkDeserializer implements Deserializer<Bookmark> {

        @Override
        public void configure(Map<String, ?> configs, boolean isKey) {

        }

        @Override
        public Bookmark deserialize(String topic, byte[] data) {
            try {
                return new PublicTxConsistentConsumerBookmarkV1().from(ByteBuffer.wrap(data));
            } catch (TxConsistentConsumerException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Bookmark deserialize(String topic, Headers headers, byte[] data) {
            return deserialize(topic, data);
        }

        @Override
        public void close() {

        }
    }

}
