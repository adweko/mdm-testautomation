package com.helvetia.mdm.testautomation;

import com.helvetia.mdm.testautomation.kafka.KafkaHelper;
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

public class consumeTopicsINTG {

    /**
     * create a file called src/main/resources/cert/passwords.properties with the content
     * SSL_KEYSTORE_PASSWORD=your-password
     * SSL_TRUSTSTORE_PASSWORD=your-password
     * The available keystores are those of the DEVL cvboost kafka agent, so if you know the passwords for them
     * you can use them here and do not have to replace the stores.
     * Else, you will have to apply for your own via https://wiki.helvetia.group/x/iBktEw
     */
    @BeforeAll
    public static void loadEnvvarsFromFile() throws IOException {
        Properties passwords = new Properties();
        passwords.load(Files.newInputStream(Paths.get("src/main/resources/cert_INTG/passwords.properties")));
        passwords.forEach((k, v) -> System.setProperty(k.toString(), v.toString()));
    }


    @Test
    public void testConnect() {
        Properties consumerConfig = getConsumerConfigINTGWithoutSerdes(true);
        Consumer<String, GenericRecord> consumer = new KafkaConsumer<>(consumerConfig);
        consumer.listTopics().keySet().forEach(System.out::println);

    }


    @Test
    public void testINTGMissingPartner() {
        Properties consumerConfig = getConsumerConfigINTGWithoutSerdes(false);
        Consumer<String, GenericRecord> consumer = new KafkaConsumer<>(consumerConfig);
        final List<PartitionInfo> partitionInfos = consumer.partitionsFor("CDC.zdkub000.z00.tkupe_person");
        final List<TopicPartition> topicPartitions = partitionInfos.stream()
                .map(info -> new TopicPartition(info.topic(), info.partition()))
                .collect(Collectors.toList());

        consumer.assign(topicPartitions);
        //45875605
        System.out.println("beginning " + consumer.beginningOffsets(topicPartitions));
        System.out.println("end " + consumer.endOffsets(topicPartitions));
        topicPartitions.forEach(tp -> consumer.seek(tp, 45875605));
        final List<Integer> integers = Arrays.asList(23850913, 23850915, 23850916, 23850917, 23850928, 23850990, 23850918, 23850920, 23850940);

        while (true) {
            ConsumerRecords<String, GenericRecord> poll = consumer.poll(Duration.ofSeconds(3));
            if (poll.isEmpty()) {
                System.out.println("empty poll");
                break;
            }
            for (ConsumerRecord<String, GenericRecord> record : poll) {
                GenericRecord value = record.value();
                try {
                    int kupe = Integer.parseInt((record.value().get("KUPE_LAUFNUMMER").toString()));
                    if (integers.contains(kupe)) {
                        System.out.println("found a match");
                        System.out.println(value);
                    }
                } catch (Exception e) {
                    System.out.println("Error " + e);
                    System.out.println("value " + value);
                }
            }
            System.out.println("finished poll up to " + consumer.position(topicPartitions.get(0)));
        }
    }


    public static Properties getConsumerConfigINTGWithoutSerdes(boolean useSpecificAvro) {
        Properties consumerConfig = new Properties();
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "ch-mdm_test1234");
        consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "cluster-main-kafka-bootstrap-kafka-intg.apps.helvetia.io:443");
        consumerConfig.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "https://cp-schema-registry-kafka-intg.apps.helvetia.io:443");
        consumerConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        consumerConfig.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);

        consumerConfig.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL.name);
        consumerConfig.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, Paths.get("src/main/resources/cert_INTG/user-truststore.jks").toAbsolutePath().toString());
        consumerConfig.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, System.getProperty("SSL_TRUSTSTORE_PASSWORD"));
        consumerConfig.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, Paths.get("src/main/resources/cert_INTG/user-keystore.jks").toAbsolutePath().toString());
        consumerConfig.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, System.getProperty("SSL_KEYSTORE_PASSWORD"));
        consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        consumerConfig.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, useSpecificAvro);

        return consumerConfig;
    }

}
