# MDM-Testautomation

Dieses Projekt implementiert eine grundlegende Testautomatisierung für den MDM-Systemverbund,
bestehend aus MDM, ZEPAS und Syrius.

Die grundlegende Anforderung ist, dass integrative Testfälle datengetrieben sein sollen. 
Um die Definition solcher Testfälle zu vereinfachen wird ein generisches Objekt "Partner"
definiert, welches ausschließlich fachlich relevante Attribute beinhaltet (z.B. keine kupeLaufnummer aus ZEPAS).
 
 Von diesem aus werden Mappings in die verschiedenen Repräsentationen definiert, z.B. in das Format
 in ZEPAS oder im Kafka-Topic ch-mdm.change.avro.
 
 Hierauf basierend lassen sich Testfälle auf fachlicher Datenbasis definieren und validieren.
 Ein Beispiel wäre:
 Definiere einen Partner und ändere den Namen vom Defaultwert auf "A.P. Felstrudel".
 Lasse diesen im ZEPAS anlegen.
 Validiere, dass in den Kafka-topics Nachrichten ankommen, aus denen sich genau dieser Partner wieder
 rekonstruieren lässt. 
 
 Dieser grundlegende Testfall ist auch als erstes Beispiel in src/test/java/com/helvetia/mdm/testautomation/creation/CreatePartnerIT.java
 implementiert.
 
 ### Offene Punkte
 1. Anlegen von Partnern im Syrius
 2. Abfragen von Partnern im ZEPAS (Webservice "getFullPerson" benutzen)
 3. Abfragen von Partnern im Syrius
 4. Vereinfachung der API zur Definition von Testfällen
 5. Überprüfen der Implementierung der equals()-Funktionen und mappings
 6. Mappings aus ch-mdm-syrius-provider und ch-mdm-zepas-provider wiederverwenden
 7. Besser Typisierung der Felder von Partner.java und Subklassen